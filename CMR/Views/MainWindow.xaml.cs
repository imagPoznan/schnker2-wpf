﻿using System.Windows;
using Schenker2.Controllers;
using Schenker2.Models.Models;
using Microsoft.Win32;
using System;
using System.Windows.Controls;
using System.Collections.Generic;
using Schenker2.Models.Repository;
using System.Linq;
using Schenker2.DbModels;
using System.Collections.ObjectModel;

namespace Schenker2.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            pickupFromDate.SelectedDate = DateTime.Now;
            pickupToDate.SelectedDate = DateTime.Now;
            deliveryFromDate.SelectedDate = DateTime.Now.AddDays(1);
            deliveryToDate.SelectedDate = DateTime.Now.AddDays(1);
            var timeFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0);
            var timeTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20, 0, 0);
            pickupFromTime.SelectedTime = timeFrom;
            pickupToTime.SelectedTime = timeTo;
            pickupFromTime.Is24Hours = true;
            pickupToTime.Is24Hours = true;
            //
            deliveryFromTime.SelectedTime = timeFrom;
            deliveryToTime.SelectedTime = timeTo;
            deliveryFromTime.Is24Hours = true;
            deliveryToTime.Is24Hours = true;
            ChangeVisivilityOfComponent();

            //
            //System.Windows.Forms.MessageBox.Show("Test");
            var currentHour = DateTime.Now.Hour;
            if (currentHour >= 16)
            {
                pickupFromDate.SelectedDate = pickupFromDate.SelectedDate.Value.AddDays(1);
                //pickupToDate.SelectedDate = pickupToDate.SelectedDate.Value.AddDays(1);
            }

            if (deliveryFromDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday)
                deliveryFromDate.SelectedDate = deliveryFromDate.SelectedDate.Value.AddDays(2);
            if (deliveryFromDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
                deliveryFromDate.SelectedDate = deliveryFromDate.SelectedDate.Value.AddDays(1);

            if (deliveryToDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday)
                deliveryToDate.SelectedDate = deliveryToDate.SelectedDate.Value.AddDays(2);
            if (deliveryToDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
                deliveryToDate.SelectedDate = deliveryToDate.SelectedDate.Value.AddDays(1);

            if (pickupFromDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday)
                pickupFromDate.SelectedDate = pickupFromDate.SelectedDate.Value.AddDays(2);
            if (pickupFromDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
                pickupFromDate.SelectedDate = pickupFromDate.SelectedDate.Value.AddDays(1);

            if (pickupToDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday)
                pickupToDate.SelectedDate = pickupToDate.SelectedDate.Value.AddDays(2);
            if (pickupToDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
                pickupToDate.SelectedDate = pickupToDate.SelectedDate.Value.AddDays(1);


            generateFromDate.SelectedDate = DateTime.Now;
            generateToDate.SelectedDate = DateTime.Now;

       
        }

        private void tabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            
        }
        
        private void cbDatabaseAuth_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectBox = CbDatabaseAuth.SelectionBoxItem.ToString();
            if (selectBox != null && selectBox.Contains("SQL"))
            {
                TxtDatabaseLogin.IsEnabled = false;
                TxtDatabaseLogin.Text = string.Empty;
                TxtDatabasePassword.IsEnabled = false;
                TxtDatabasePassword.Text = string.Empty;
             
            }
            else
            {
                TxtDatabaseLogin.IsEnabled = true;
                TxtDatabasePassword.IsEnabled = true;
            }
        }





        #region AutoGeneratingColumn
        //private void dgFvList_AutoGeneratingColumn(object sender,
        //    System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        //{
        //    switch (e.PropertyName)
        //    {
        //        case "Id":
        //            e.Column.Header = "Id szablonu";
        //            break;
        //        case "Name":
        //            e.Column.Header = "Nazwa";
        //            break;
        //        case "Kraj":
        //        case "SzablonHtml":
        //        case "SzablonTxt":
        //        case "Monit":
        //            e.Column.Visibility = Visibility.Hidden;
        //            break;
        //        case "KrajeString":
        //            e.Column.Header = "Wybrane kraje";
        //            break;
        //        case "Wlaczony":
        //            e.Column.Header = "Włączony";
        //            break;
        //        case "MonitString":
        //            e.Column.Header = "Monit";
        //            break;
        //        case "TematWiadomosci":
        //            e.Column.Header = "Temat wiadomości";
        //            break;
        //    }
        //}


        #endregion

        #region LoadingRows


        #endregion

        private void ComboBox_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
           // DoAction();
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            //DoAction();
        }
        private void DoAction()
        {
            try
            {
                //var selectedItem = (DanePrzesylki)this.dgPaczki.CurrentItem;
                var selectedItem = (DanePrzesylki)dgPaczki.SelectedItem;
                if (selectedItem==null)
                {
                    return;
                }
                var row0 = (DanePrzesylki)this.dgPaczki.Items[0];

                var listaPaczek = GlobalSettingsSingletonController.Instance.DanePrzesylki;
                var definicje = new ImagSchenkerTypyOpakowan();
                using (var db = new DataBaseContext())
                {
                    definicje = db.ImagSchenkerTypyOpakowan.FirstOrDefault(x => x.NazwaOpakowania == selectedItem.WybranaNazwaOpakowania);
                }
                if (definicje == null)
                {
                    return;
                }
                selectedItem.Dlugosc = definicje.Dlugosc.ToString();
                selectedItem.Szerokosc = definicje.Szerokosc.ToString();
                selectedItem.Wysokosc = definicje.Wysokosc.ToString();
                //selectedItem.WybranyTypOpakowania = selectedItem.WybranaNazwaOpakowania;
                selectedItem.IloscOpakowan = 1;
                selectedItem.SymbolPaczki = definicje.SymbolPaczki;
                
                //var listaPaczek = new List<DanePrzesylki>();
                //for (int i = 0; i < dgPaczki.Items.Count; i++)
                //{
                //    var row = (DanePrzesylki)this.dgPaczki.Items[i];
                //    string selectedSymbol = string.Empty;
                //    if (row.TypyOpakowania != null && row.WybranyTypyOpakowania != null)
                //        selectedSymbol = row?.WybranyTypyOpakowania;

                //    if (row.TypyOpakowania == null && row.WybranyTypyOpakowania != null)
                //        selectedSymbol = row.WybranyTypyOpakowania;

                //    if (row.TypyOpakowania != null && row.WybranyTypyOpakowania == null)
                //        selectedSymbol = row.TypyOpakowania;

                //    var daneDoUzupelnienia = SchenkerTypyOpakowanRepository.CreateRepo().Odswiez().FirstOrDefault(x => x.SymbolPaczki == selectedSymbol);

                //    if (daneDoUzupelnienia == null) continue;

                //    var paczka = new DanePrzesylki()
                //    {
                //        Dlugosc = (daneDoUzupelnienia.Dlugosc).ToString(),
                //        Szerokosc = (daneDoUzupelnienia.Szerokosc).ToString(),
                //        Wysokosc = (daneDoUzupelnienia.Wysokosc).ToString(),
                //        TypyOpakowania = daneDoUzupelnienia.SymbolPaczki,
                //        IloscOpakowan = 1,

                //        //MiejscaPaletowe = GlobalSettingsSingletonController.Instance.IsPaczka ? "0.2" : "0",
                //        //Waga = GlobalSettingsSingletonController.Instance.PobierajWageZDokumentu ? ArtykulController.GetWholeWeightFromDocument() : 0
                //    };
                //    listaPaczek.Add(paczka);

                //}
                if (listaPaczek.Count > 0)
                {
                    GlobalSettingsSingletonController.Instance.DanePrzesylki = new System.Collections.ObjectModel.ObservableCollection<DanePrzesylki>(listaPaczek);

                }

            }
            catch (Exception)
            {

            }
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //DoAction();
        }

        private void DataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }



        private void IsSystem_Checked(object sender, RoutedEventArgs e)
        {
            ChangeVisivilityOfComponent();
        }

        private void ChangeVisivilityOfComponent()
        {
            if (gbPaczki is null) return;

            if (GlobalSettingsSingletonController.Instance.IsSystem)
            {
                dgPaczki.Visibility = Visibility.Visible;
                gbPaczki.Visibility = Visibility.Collapsed;
            }
            else
            {
                dgPaczki.Visibility = Visibility.Hidden;
                gbPaczki.Visibility = Visibility.Visible;
            }
        }

        private void IsParcel_Checked(object sender, RoutedEventArgs e)
        {
            ChangeVisivilityOfComponent();
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            DoAction();
        }

        private void TabItem_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void TabItem_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (GlobalSettingsSingletonController.Instance.PrzesylkiNadane == null)
            {
                GlobalSettingsSingletonController.Instance.PrzesylkiNadane = ImagSchenker.Instance().GetFromToday();
            }
            
        }

        //private void dgPaczki_AddingNewItem(object sender, AddingNewItemEventArgs e)
        //{
        //    try
        //    {
        //        var lastRow = GlobalSettingsSingletonController.Instance.DanePrzesylki[GlobalSettingsSingletonController.Instance.DanePrzesylki.Count - 1];
        //        GlobalSettingsSingletonController.Instance.DanePrzesylki.Add(lastRow);
        //        var listaPaczek = GlobalSettingsSingletonController.Instance.DanePrzesylki.ToList();

        //    GlobalSettingsSingletonController.Instance.DanePrzesylki = new System.Collections.ObjectModel.ObservableCollection<DanePrzesylki>(listaPaczek);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}
    }
}