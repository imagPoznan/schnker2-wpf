﻿using System;
using System.Windows;
using Schenker2.Controllers;
using Schenker2.Extensions;
using Schenker2.ModelViews;
using Schenker2.Views;

namespace Schenker2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }
        private void Test(object sender, StartupEventArgs e)
        {

        }
        private void Application_Startup(object sender, StartupEventArgs e) 
        {
            //MessageBox.Show("Test");
            try
            {
                if (e.Args.Length > 1)
                {

                    var param = e?.Args[1].Split(',');
                    var rodzajDokumentu = param[1];
                    string tryb = string.Empty;
                    if (e.Args.Length > 2)
                        tryb = e?.Args?[3] ?? "";
                    else tryb = "";


                    if (rodzajDokumentu == "202")
                    {
                        GlobalSettingsSingletonController.Instance.IdZam = Convert.ToInt32(param[0]);
                        if (string.Equals(tryb, "HiddenMode"))
                        {
                            GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.Hidden;
                            var hiddenWindow = new MainWindow();

                            hiddenWindow.Show();
                            hiddenWindow.Hide();
                            hiddenWindow.Visibility = Visibility.Hidden;
                            hiddenWindow.WindowState = WindowState.Minimized;

                        }
                        else
                        {
                            GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
                            var mainWindow = new MainWindow();
                            mainWindow.Show();
                        }
                    }
              
                    if (rodzajDokumentu == "1")
                    {
                        GlobalSettingsSingletonController.Instance.IdDokWz = Convert.ToInt32(param[0]);
                        GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
                        var mainWindow = new MainWindow();
                        mainWindow.Show();
                    }
                    if (rodzajDokumentu == "4")
                    {
                        GlobalSettingsSingletonController.Instance.IdDokHan = Convert.ToInt32(param[0]);
                        GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
                        var mainWindow = new MainWindow();
                        mainWindow.Show();
                    }
                }
                else
                {
#if DEBUG
                    GlobalSettingsSingletonController.Instance.IdDokWz = 40026;
                    GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
#endif
                    var mainWindow = new MainWindow();
                    mainWindow.Show();
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                Environment.Exit(1);
            }
         
        }
        public enum RodzajDokumentu
        {
            Zamowienie = 202,
            DokumentMagazynowy = 1,
            DokumentHandlowy = 4
        }
    }
}