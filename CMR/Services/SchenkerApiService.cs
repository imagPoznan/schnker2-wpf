﻿using Schenker2.Controllers;
using Schenker2.Extensions;
using Schenker2.schenkerProd;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Schenker2
{
    public class SchenkerApiService
    {
        public TransportOrdersService _service = new TransportOrdersService();
        public SchenkerApiService()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            _service.UserAgent = ".NET APP";
            _service.Credentials = new NetworkCredential(GlobalSettingsSingletonController.Instance.LoginSchenker, GlobalSettingsSingletonController.Instance.HasloSchenker);
        }
        public static SchenkerApiService Create()
        {
            return new SchenkerApiService();
        }
        public bool CheckLogin()
        {
            try
            {
                var resp = _service.getDistance("60128", "02220");
                if (resp > 0) return true;
                return false;
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"CheckLogin() błąd: {ex}");
                return false;
            }
          
        }
        public ColliDictionaryElement[] GetPackagesDictionary()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                return _service.getPackageDictionary();
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"GetPackagesDictionary() błąd: {ex}");
                return null;
            }
        }
        public void GetDocument(string orderId, ref string pathLabel,ref string pathLp)
        {
            try
            {
                //var ety = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.DWB, orderId, "LABEL");
                //var ety2 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.CGN, orderId, "LABEL");
                //var ety4 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.FF, orderId, "LABEL");
                //var ety5 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.PKG, orderId, "LABEL");
                //var ety6 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.SHP, orderId, "LABEL");
                //var ety7 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.COR, orderId, "LABEL");
                //var ety8 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType., orderId, "LABEL");
                ////chyba dla system
                ///
                //Dla przesyłek Parcel dostępna jest wyłącznie etykieta adresowa
                var lp = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.DWB, orderId, "LP");

                var label = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.DWB, orderId, "LABEL");

            
                pathLabel = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{orderId}_EA.pdf";
               
                if (!GlobalSettingsSingletonController.Instance.IsParcel)
                {
                    pathLp = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{orderId}_LP.pdf";
                    File.WriteAllBytes(pathLp, lp);
                }
                File.WriteAllBytes(pathLabel, label);
                
                //return pathLabel;

            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"GetDocument() błąd: {ex}");
                //return "";
            }
        }
        public bool IsServiceAvailableForDestination (string kodPocztowyOdbiorcy,string kodPocztowyNadawcy,int kodUslugi)
        {
            try
            {
                var availableServices = _service.getAvailableServices(kodPocztowyOdbiorcy, kodPocztowyNadawcy);
                return availableServices.FirstOrDefault(x => x.serviceCode == kodUslugi)?.available ?? false;
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"IsServiceAvailableForDestination() błąd: {ex}");
                return false;
            }
            
        }
        public Reference[] PrepareReferences()
        {
            if (GlobalSettingsSingletonController.Instance.SelectedReferencesIndex == 0)
                return null;
            var referencje = new List<Reference>()
            {
                new Reference(){refNo = GlobalSettingsSingletonController.Instance.ReferenceValue,refType = GlobalSettingsSingletonController.Instance.SelectedReferencesIndex }
            };
            
            if (referencje.Count == 0)
            {
                return null;
            }
            return referencje.ToArray();
        }
        public Service[] PrepareServices()
        {
            var uslugi = new List<Service>();
            if (GlobalSettingsSingletonController.Instance.Pobranie)
            {
                var serv = new Service()
                {
                    code = 9,
                    parameter1 = (GlobalSettingsSingletonController.Instance.KwotaPobrania.ConvertToDecimal()*100).ToString().Replace(",0000","").Replace(",00","")
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.IsDeklarowanaWartosc)
            {
                var serv = new Service()
                {
                    code = 8,
                    parameter1 = (GlobalSettingsSingletonController.Instance.KwotaDeklarowanejWartosci.ConvertToDecimal() * 100).ToString().Replace(",0000", "").Replace(",00", "")
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.Wniesienie/*Czynności ładunkowe na zlecenie klienta np.wniesienie*/)
            {
                var serv = new Service()
                {
                    code = 1,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.Wyladunek/*Czynności ładunkowe na zlecenie klienta uodbiorcy np. wyładunek*/)
            {
                var serv = new Service()
                {
                    code = 2,
                };
                uslugi.Add(serv);
            }
            if (false/*System Obrotu Paletami (SOP) - wymaga podania numerów paletowych nadawcy i
odbiorcy. Pola odpowiednio: "paletteId" nadawcy," paletteId" odbiorcy. Parametr oznacza ilość
palet zwrotnych. 
*/)
            {
                var serv = new Service()
                {
                    code = 7,
                    parameter1 ="" //TODO

                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.AwizacjaDosieci/*Awizacja do sieci */)
            {
                var serv = new Service()
                {
                    code = 16,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.PlaciOdbiorca/*Przeniesienie opłaty za transport na Odbiorcę*/)
            {
                var serv = new Service()
                {
                    code = 26,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.DostawaDo10/*Przeniesienie opłaty za transport na Odbiorcę*/)
            {
                var serv = new Service()
                {
                    code = 34,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.DostawaDo13/*Przeniesienie opłaty za transport na Odbiorcę*/)
            {
                var serv = new Service()
                {
                    code = 36,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.DostawaDo10Premium/*Przeniesienie opłaty za transport na Odbiorcę*/)
            {
                var serv = new Service()
                {
                    code = 55,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.DostawaDo13Premium/*Przeniesienie opłaty za transport na Odbiorcę*/)
            {
                var serv = new Service()
                {
                    code = 56,
                };
                uslugi.Add(serv);
            }
            if (false/*E-Dokumenty zwrotne - skan i archiwizacja
(dostęp do skanów dokumentów w systemie econnect i archiwizacja oryginałów przez DB
Schenker) - wymaga podania 3 parametrów*/)
            {
                var serv = new Service()
                {
                    code = 27,
                    parameter1="",
                   parameter2="",
                   parameter3=""
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.AwizacjaEmail/*Awizacja e-mail*/)
            {
                var serv = new Service()
                {
                    code = 81,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.AwizacjaSms/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 82,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.AwizacjaTelefoniczna/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 83,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.SamochodWindaDostawa/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 502,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.SamochodWindaOdbior/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 501,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.BrakOdbioruNaczepaLadownosc24tony/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 503,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.BrakDostawyNaczepaLadownosc24tony/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 504,
                };
                uslugi.Add(serv);
            }
            if (GlobalSettingsSingletonController.Instance.PolecenieOdbioru/*Awizacja sms*/)
            {
                var serv = new Service()
                {
                    code = 78,
                };
                uslugi.Add(serv);
            }

            if (uslugi.Count == 0)
            {
                return null;
            }
            return uslugi.ToArray();
        }

        internal void PobierzPonownieEtykiete(ImagSchenker selectedNadanie)
        {
            try
            {
                string pathLabel = "";
                string pathLp = "";
                //var lp = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.SHP, selectedNadanie.OrderId, "LP");
                var lp2 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.DWB, selectedNadanie.OrderId, "LP");

                //var label = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.SHP, selectedNadanie.OrderId, "LABEL");
                var label2 = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.DWB, selectedNadanie.OrderId, "LABEL");//system

                if (lp2.Length > 0)
                {
                    pathLp = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{selectedNadanie.OrderId}_LP.pdf";
                    File.WriteAllBytes(pathLp, lp2);
                    Process.Start(pathLp);
                }
                if (label2.Length > 0)
                {
                    //pathLabel = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{selectedNadanie.OrderId}_EA.pdf";
                    //File.WriteAllBytes(pathLabel, label);
                    //Process.Start(pathLabel);
                    pathLabel = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{selectedNadanie.OrderId}_EA.pdf";
                    File.WriteAllBytes(pathLabel, label2);
                    Process.Start(pathLabel);
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"PobierzPonownieEtykieteAction błąd: {ex}");
                throw ex;
            }
        }

        public bool GenerujNadanie(List<string> numeryPrzesylek)
        {
            try
            {
                var zdnList = new ZdnWaybNosType()
                {
                    //labelNo = numeryPrzesylek.ToArray(),
                    waybNo = numeryPrzesylek.ToArray()
                };

                var response = _service.createManifest(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, "", "", zdnList, out string message, out decimal zdnNo1);
                if (!string.IsNullOrWhiteSpace(message))
                {
                    throw new Exception(message);
                }
                if (zdnNo1 > 0)
                {
                    var pdfBytes = _service.getDocuments(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, ReferenceType.ZDNNO, zdnNo1.ToString(), "ZDN");
                    var pathLp = GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety + $@"\{zdnNo1}_ZDN.pdf";
                    File.WriteAllBytes(pathLp, pdfBytes);

                    var statusWydruku = PrintingController.PrintPdf(GlobalSettingsSingletonController.Instance.DrukarkaLp, "A4", pathLp, 1);
                    Logs.AddLogInfo($@"Status wydruku ZD na drukarce: {GlobalSettingsSingletonController.Instance.DrukarkaEtykieta} - {statusWydruku}");
                    return statusWydruku;
                }
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }

    }
}
