using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Schenker2.Controllers;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using MaterialDesignThemes.Wpf;
using System.Windows.Forms;
using Schenker2.Models.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Schenker2;
using Schenker2.Models.Repository;
using System.Linq;
using Schenker2.DbModels;

namespace Schenker2.ModelViews
{
    public class MainWindowViewModel : BindableBaseImpl
    {
        private DataBaseController _dataBaseConnection;
        private DaneOdbiorcyModel _daneOdbiorcyModel;
        private ImagSchenkerTypyOpakowan _selectedDanePrzesylkiSzablon;
        private IEnumerable<ImagSchenkerTypyOpakowan> _danePrzesylkiSzablon;
        private string _symbolPaczkiSzablon;
        private DanePrzesylki _selectedDanePrzesylki;
        private ImagSchenker _selectedNadanie;

        public GlobalSettingsSingletonController GlobalSettingsSingletonController => GlobalSettingsSingletonController.Instance;
        public static DataModel DataModel { get; set; } = new DataModel();

        public ImagSchenker SelectedNadanie
        {
            get { return _selectedNadanie; }
            set
            {
                _selectedNadanie = value;
                OnPropertyChanged();
            }
        }
        public ImagSchenkerTypyOpakowan SelectedDanePrzesylkiSzablon
        {
            get { return _selectedDanePrzesylkiSzablon; }
            set
            {
                _selectedDanePrzesylkiSzablon = value;
                OnPropertyChanged();
            }
        }

        public DanePrzesylki SelectedDanePrzesylki
        {
            get { return _selectedDanePrzesylki; }
            set
            {
                _selectedDanePrzesylki = value;
                OnPropertyChanged();
            }
        }
        public IEnumerable<ImagSchenkerTypyOpakowan> DanePrzesylkiSzablon
        {
            get { return _danePrzesylkiSzablon; }
            set
            {
                _danePrzesylkiSzablon = value;
                OnPropertyChanged();
            }
        }
        public string SymbolPaczkiSzablon
        {
            get { return _symbolPaczkiSzablon; }
            set
            {
                _symbolPaczkiSzablon = value;
                OnPropertyChanged();
            }
        }
        public DaneOdbiorcyModel DaneOdbiorcyModel
        {
            get { return _daneOdbiorcyModel; }
            set
            {
                _daneOdbiorcyModel = value;
                OnPropertyChanged();
            }
        }
        public DataBaseController DataBaseConnection
        {
            get { return _dataBaseConnection; }
            set
            {
                _dataBaseConnection = value;
                OnPropertyChanged();
            }
        }


        public decimal VersionApp => GlobalSettingsSingletonController.VersionApp;
        public ICommand SaveDatabaseData { get; set; }
        public ICommand ConnectDatabase { get; set; }
        public ICommand CheckLicenseButton { get; set; }
        public ICommand Loaded { get; set; }
        public ICommand GetDokumentMagazynowyById { get; set; }
        public ICommand GetZamowienieById { get; set; }
        public ICommand GetDokumentHandlowyById { get; set; }
        public ICommand CreateOperationWfMag { get; set; }
        public ICommand Generuj { get; set; }
        public ICommand OdswiezDokument { get; set; }
        public ICommand WybierzSciezke { get; set; }
        public ICommand DodajSymbolPaczki { get; set; }
        public ICommand UsunSymbolPaczki { get; set; }
        public ICommand ZapiszDefinicjePaczek { get; set; }
        public ICommand DodajWiersz { get; set; }
        public ICommand UsunWiersz { get; set; }
        public ICommand GenerujNadanie { get; set; }
        public ICommand OdswiezNadane { get; set; }
        public ICommand PobierzPonownieEtykiete { get; set; }

        public MainWindowViewModel()
        {
            InitObjects();
            InitBindings();
        }


        private void InitObjects()
        {
            try
            {
                DataBaseController.ConnectDatabase();
                DataBaseConnection = DataBaseController.Instance;
                DaneOdbiorcyModel = new DaneOdbiorcyModel()
                {
                    AdresOdbiorcy = new AdresModel()
                };
                //_danePrzesylkiSzablon = SchenkerTypyOpakowanRepository.CreateRepo().Odswiez().ToList();
                AktualizujTypyOpakowan();
                //DanePrzesylkiSzablon = SchenkerTypyOpakowanRepository.CreateRepo().Aktualizuj(DanePrzesylkiSzablon).ToList();
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"Błąd przy inicjalizacji: {ex}");
            }
           

        }

        private void InitBindings()
        {
            SaveDatabaseData = new LoaderAwaitableDelegateCommand(SaveDatabaseDataAction);
            ConnectDatabase = new LoaderAwaitableDelegateCommand(ConnectDatabaseAction);
            CheckLicenseButton = new LoaderAwaitableDelegateCommand(CheckLicenseButtonAction);
            Loaded = new LoaderAwaitableDelegateCommand(LoadedAction);
            GetDokumentMagazynowyById = new LoaderAwaitableDelegateCommand(GetDokumentMagazynowyByIdAction);
            GetDokumentHandlowyById = new LoaderAwaitableDelegateCommand(GetDokumentHandlowyByIdAction);
            CreateOperationWfMag = new LoaderAwaitableDelegateCommand(CreateOperationWfMagAction);
            OdswiezDokument = new AwaitableDelegateCommand(OdswiezDokumentAction);
            GetZamowienieById = new LoaderAwaitableDelegateCommand(GetZamowienieByIdAction);
            Generuj = new LoaderAwaitableDelegateCommand(GenerujAction);
            WybierzSciezke = new AwaitableDelegateCommand(WybierzSciezkeAction);
            DodajSymbolPaczki = new AwaitableDelegateCommand(DodajSymbolPaczkiAction);
            UsunSymbolPaczki = new AwaitableDelegateCommand(UsunSymbolPaczkiAction);
            ZapiszDefinicjePaczek = new AwaitableDelegateCommand(ZapiszDefinicjePaczekAction);
            DodajWiersz = new AwaitableDelegateCommand(DodajWierszAction);
            UsunWiersz = new AwaitableDelegateCommand(UsunWierszAction);
            GenerujNadanie = new AwaitableDelegateCommand(GenerujNadanieAction);
            OdswiezNadane = new AwaitableDelegateCommand(OdswiezNadaneAction);
            PobierzPonownieEtykiete = new LoaderAwaitableDelegateCommand(PobierzPonownieEtykieteAction);
        }

        private async Task PobierzPonownieEtykieteAction()
        {
            try
            {
                var resp = false;

                await Task.Factory.StartNew(
                    () => 
                    { SchenkerApiService.Create().PobierzPonownieEtykiete(SelectedNadanie); }
                    );
            }
            catch (Exception ex)
            {
                await ActionsController.ShowMessagebox(@"Wystąpił błąd");
            }
        }

        private async Task GenerujNadanieAction()
        {
            try
            {
                SchenkerApiService.Create().GenerujNadanie(GlobalSettingsSingletonController.PrzesylkiNadane.Select(x=>x.OrderId).ToList());
            }
            catch (Exception)
            {

               // throw;
            }
        }
        private async Task OdswiezNadaneAction()
        {
            try
            {
                GlobalSettingsSingletonController.PrzesylkiNadane = ImagSchenker.Instance().GetByFilters();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task DodajWierszAction()
        {
            try
            {
                var paczka =
                        new DanePrzesylki()
                        {
                            IloscOpakowan = 1,
                            NazwyTowarow = /*DataBaseController.PobierzSzablonyPaczek().Select(x => x.NazwaOpakowania).ToList(),*/
                            new List<string>(){ "narzędzia i elektronarzędzia", "drabina do 3m", "drabina pow. 3m" },
                            TypyOpakowania = DataBaseController.PobierzSzablonyPaczek()?.Select(x => x.SymbolPaczki)?.ToList(),
                            NazwyOpakowan = DataBaseController.PobierzSzablonyPaczek()?.Select(x => x.NazwaOpakowania)?.ToList(),
                            WybranyTypOpakowania = "",
                            TypZabezpieczenia = "karton + folia",
                            Waga = ""
                        };
                    
                GlobalSettingsSingletonController.DanePrzesylki.Add(paczka);
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"DodajWierszAction() błąd: {ex}");
            }
        }
        private async Task UsunWierszAction()
        {
            try
            {
                if (GlobalSettingsSingletonController.DanePrzesylki.Count == 1 || SelectedDanePrzesylki == null)
                {
                    return;
                }
                GlobalSettingsSingletonController.DanePrzesylki.Remove(SelectedDanePrzesylki);
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"UsunWierszAction() błąd: {ex}");
            }
        }

        private async Task ZapiszDefinicjePaczekAction()
        {
            await Task.Run(() => AktualizujTypyOpakowan());
        }
        private void AktualizujTypyOpakowan()
        {
            DanePrzesylkiSzablon = SchenkerTypyOpakowanRepository.CreateRepo().Aktualizuj(DanePrzesylkiSzablon)?.ToList();
        }
        private async Task UsunSymbolPaczkiAction()
        {
            try
            {
                if (SelectedDanePrzesylkiSzablon != null)
                {
                    SchenkerTypyOpakowanRepository.CreateRepo().UsunTyp(SelectedDanePrzesylkiSzablon);
                    AktualizujTypyOpakowan();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task DodajSymbolPaczkiAction()
        {
            try
            {
                await Task.Run(() =>
                {
                    if (string.IsNullOrWhiteSpace(SymbolPaczkiSzablon))
                        throw new Exception("Wprowadź poprawną nazwę.");

                    //if (SchenkerTypyOpakowanRepository.CreateRepo()?.Get().FirstOrDefault(x => x.SymbolPaczki == SymbolPaczkiSzablon) != null)
                    //    throw new Exception("Typ opakowania jest już zadeklarowany.");
                
                    SchenkerTypyOpakowanRepository.CreateRepo().DodajTyp(SymbolPaczkiSzablon);
                    //DanePrzesylkiSzablon = SchenkerTypyOpakowanRepository.CreateRepo()?.Get();
                    //GlobalSettingsSingletonController.Instance.SymbolePrzesylek = DanePrzesylkiSzablon.Select(x => x.SymbolPaczki).ToList();

                    AktualizujTypyOpakowan();
                }
                );

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private  async Task WybierzSciezkeAction()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety = dialog.ShowDialog() == DialogResult.OK ? dialog.SelectedPath : string.Empty;
        }

        private async Task OdswiezDokumentAction()
        {
            await Task.Factory.StartNew(
                () =>
                {
                    DaneOdbiorcyModel = DokumentMagazynowyController.LoadedActionAsync(DaneOdbiorcyModel);
                });
        }

        private async Task CreateOperationWfMagAction()
        {
            var resp = false;
            await Task.Factory.StartNew(() => { resp = ActionsController.CreateOperationWfMagActionAsync(); });
          
            await ActionsController.ShowMessagebox(resp
                    ? @"Zadanie zostało dodane do operacji dodatkowych pod nazwą Moduł Schenker IMAG"
                    : @"Wystąpił błąd");
        }

        private async Task GenerujAction()
        {
            try
            {
                bool status = false;
                await Task.Factory.StartNew(() =>
                {
                    status = GenerujController.Create().GenerujPrzesylke(DaneOdbiorcyModel);
                    GlobalSettingsSingletonController.PrzesylkiNadane = ImagSchenker.Instance().GetFromToday();
                });
                if (status)
                    //await ActionsController.ShowMessagebox("Wygenerowano pomyślnie!");
                    await MboxHiddenModeHelper.InfoLogs("Wygenerowano pomyślnie!");
            }
            catch (Exception ex)
            {
                await MboxHiddenModeHelper.ErrorLogs($@"Nie wygenerowano przesyłki: {ex}");
            }
        }


        private async Task GetDokumentMagazynowyByIdAction()
        {
            await Task.Factory.StartNew(
                () => { DokumentMagazynowyController.GetDokumentMagazynowyByIdAsync(DaneOdbiorcyModel); });
        }
        private async Task GetDokumentHandlowyByIdAction()
        {
            await Task.Factory.StartNew(
                () => { DokumentHandlowyController.GetDokHandlowyByIdAsync(DaneOdbiorcyModel); });
        }

        private async Task GetZamowienieByIdAction()
        {
            await Task.Factory.StartNew(
                () => { ZamowienieController.GetZamowienieByIdAsync(DaneOdbiorcyModel); });
        }

        private async Task LoadedAction()
        {
            //MessageBox.Show("Test");
            await Task.Factory.StartNew(() =>
            {
                GlobalSettingsSingletonController.IsSystem = true;
                var szablonyPaczek = DataBaseController.PobierzSzablonyPaczek();

                if (GlobalSettingsSingletonController.DanePrzesylki==null || GlobalSettingsSingletonController.DanePrzesylki.Count == 0)
                {
                    
                    GlobalSettingsSingletonController.DanePrzesylki = new ObservableCollection<DanePrzesylki>()
                    {
                        new DanePrzesylki()
                        {
                            IloscOpakowan = 1,
                            NazwyTowarow = new List<string>(){ "narzędzia i elektronarzędzia", "drabina do 3m", "drabina pow. 3m" },
                            TypyOpakowania = szablonyPaczek?.Select(x=>x.SymbolPaczki)?.ToList(),
                            NazwyOpakowan = szablonyPaczek?.Select(x=>x.NazwaOpakowania)?.ToList(),
                            WybranyTypOpakowania = "",
                            TypZabezpieczenia = "karton + folia",
                            Waga = "",
                        }
                    };
                }


                //ActionsController.SetLicenseInit();
                GlobalSettingsSingletonController.Instance.IsLicense = true;
                if (GlobalSettingsSingletonController.Instance.IdZam != 0)
                {
                    DaneOdbiorcyModel = ZamowienieController.LoadedActionAsync(DaneOdbiorcyModel);
                    if (DaneOdbiorcyModel.FormaPlatnosci.ToLower().Contains("pobranie"))
                        GlobalSettingsSingletonController.Pobranie = true;

                    //aw narzędzia
                    var listaPaczek = new List<DanePrzesylki>();

                    if (!string.IsNullOrWhiteSpace(DaneOdbiorcyModel.Zamowienie.Pole9))
                    {
                        //MessageBox.Show("Test");
                        //Obsługa ilości i wag
                       
                        int ilosc = 0;
                        int waga = 0;
                        var nazwaAsortymentu = DaneOdbiorcyModel.Zamowienie.Pole7;
                        if (nazwaAsortymentu == "NAR_I_ELE")
                            nazwaAsortymentu = "narzędzia i elektronarzędzia";
                        else if (nazwaAsortymentu == "DRA<3")
                            nazwaAsortymentu = "drabina do 3m";
                        else if (nazwaAsortymentu == "DRA>3")
                            nazwaAsortymentu = "drabina pow. 3m";

                        else nazwaAsortymentu = "narzędzia i elektronarzędzia";//"karton + folia";


                        //MessageBox.Show("Test");

                        int wagaPerPaczka = 0;
                        Logs.AddLogInfo($@"Pole9 dla zamówienia od id {GlobalSettingsSingletonController.Instance.IdZam} , wartość : {DaneOdbiorcyModel.Zamowienie.Pole9}");
                        var pole9Splited = DaneOdbiorcyModel.Zamowienie.Pole9?.Split(';')?? new string[0];
                        if (pole9Splited?.Count() >= 2)
                        {
                            ilosc = pole9Splited[0].ToString().ConvertToInt();
                            waga = pole9Splited[1].ToString().ConvertToInt();
                            wagaPerPaczka = waga / ilosc;

                            
                        }
                        //Obsługa nazwy opakowania
                        var typOpakowania = PrzygotujTypOpakowania(DaneOdbiorcyModel.Zamowienie.Pole8);
                        //if (typOpakowania?.ToLower().Contains("paczka") == true)
                        //{
                        //    GlobalSettingsSingletonController.IsParcel = true;
                        //    GlobalSettingsSingletonController.ParcelWagaLacznie = waga;
                        //    GlobalSettingsSingletonController.ParcelLiczbaPaczek = ilosc;
                        //}
                        //else
                        if (typOpakowania?.ToLower().Contains("paczka") == true)
                        {
                            typOpakowania = "KARTON";
                        }
                            GlobalSettingsSingletonController.IsSystem = true;

                        var definicje = new ImagSchenkerTypyOpakowan();
                        using (var db = new DataBaseContext())
                        {
                            definicje = db.ImagSchenkerTypyOpakowan.FirstOrDefault(x => x.NazwaOpakowania == typOpakowania);
                        }
                        if (definicje == null)
                        {
                            return;
                        }


                        for (int i = 0; i < ilosc; i++)
                        {
                            listaPaczek.Add(new DanePrzesylki()
                            {
                                IloscOpakowan = 1,
                                NazwyTowarow = new List<string>() { "narzędzia i elektronarzędzia", "drabina do 3m", "drabina pow. 3m" },//new List<string>(){"NAR_I_ELE","DRA<3","DRA>3"},
                                TypyOpakowania = szablonyPaczek?.Select(x => x.SymbolPaczki)?.ToList(),
                                NazwyOpakowan = szablonyPaczek?.Select(x => x.NazwaOpakowania)?.ToList(),
                                WybranyTypOpakowania = nazwaAsortymentu,
                                TypZabezpieczenia = "karton+folia",
                                Waga = wagaPerPaczka.ToString(),
                                WybranaNazwaTowaru = nazwaAsortymentu,
                            });
                        }
                        foreach (var item in listaPaczek)
                        {
                            item.Dlugosc = definicje.Dlugosc.ToString();
                            item.Szerokosc = definicje.Szerokosc.ToString();
                            item.Wysokosc = definicje.Wysokosc.ToString();
                            item.SymbolPaczki = definicje.SymbolPaczki;
                            item.WybranaNazwaOpakowania = definicje.NazwaOpakowania;
                            item.IloscOpakowan = 1;
                        }
                        GlobalSettingsSingletonController.Instance.DanePrzesylki = new System.Collections.ObjectModel.ObservableCollection<DanePrzesylki>(listaPaczek);

                    }

                }          
                if (GlobalSettingsSingletonController.Instance.IdDokWz != 0)
                     DaneOdbiorcyModel = DokumentMagazynowyController.LoadedActionAsync(DaneOdbiorcyModel);
                if (GlobalSettingsSingletonController.Instance.IdDokHan != 0)
                    DaneOdbiorcyModel = DokumentHandlowyController.LoadedActionAsync(DaneOdbiorcyModel);
            });

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.Hidden)
            {
                try
                {
                    var status = GenerujController.Create().GenerujPrzesylke(DaneOdbiorcyModel);
                    if (status)
                    {
                        await MboxHiddenModeHelper.InfoLogs("Wygenerowano pomyślnie przesyłke");
                        Environment.Exit(0);
                    }

                    else
                    {
                        await MboxHiddenModeHelper.ErrorLogs("Nie wygenerowano przesyłki, id_zamowienia: " + GlobalSettingsSingletonController.Instance.IdZam);
                        Environment.Exit(1002);
                    }
                }
                catch (Exception ex)
                {
                    await MboxHiddenModeHelper.ErrorLogs($@"Nie wygenerowano przesyłki, błąd:{ex}, id_zamowienia: " + GlobalSettingsSingletonController.Instance.IdZam);
                }
              
            }
        }

        private string PrzygotujTypOpakowania(string pole8)
        {
            try
            {
                switch (pole8)
                {
                    case "PALETA_EURO":
                        return "PALETA EUR lub EPAL";
                    case "PALETA_JEDNORAZOWA":
                        return "PALETA JEDNORAZOWA";
                    case "POLPALETA":
                        return "1/2PALETA";
                    case "PALETA_PRZEMYSLOWA":
                        return "PALETA PRZEMYSŁOWA";
                    case "PACZKA 30kg":
                        return "PACZKA 30kg";
                    case "PACZKA 50kg":
                        return "PACZKA 50kg";
                    case "1/4 PALETA":
                        return "1/4PALETA";
                    default:
                        Logs.AddLogError($"Wrong format of pole8: '{pole8}', must be: 'rodzaj paczki/palety'");
                        return "";
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError($"PrzygotujTypOpakowania() błąd: {ex}");
                return "";
            }
        }

        private static async Task CheckLicenseButtonAction()
        {
            var resp = String.Empty;
            await Task.Factory.StartNew(() => { resp = LicenseManagerController.CheckLicenseButtonAction(); });
            await ActionsController.ShowMessagebox(GlobalSettingsSingletonController.Instance.IsLicense
                ? $@"{resp}{Environment.NewLine}{GlobalSettingsSingletonController.Instance.DataWygasnieciaLicencji}"
                : $"Brak licencji: {resp}");
        }


        private async Task SaveDatabaseDataAction()
        {
            var resp = false;
            await Task.Factory.StartNew(() => { resp = DataBaseController.SaveDatabaseDataAction(DataBaseConnection); });
            await ActionsController.ShowMessagebox(resp ? @"Baza danych została zapisana" : @"Wystąpił błąd");
        }
        

        private async Task ConnectDatabaseAction()
        {
            var resp = false;
          
            await Task.Factory.StartNew(() => { resp = DataBaseController.ConnectDatabase(); });
            await ActionsController.ShowMessagebox(resp ? @"Połączenie zostało nawiązane" : @"Wystąpił błąd");
            await Task.Factory.StartNew(() => { DataBaseController.UzupelniSzablonyPaczek(); });
        }

        public static DialogClosingEventHandler ClosingEventHandler { get; set; }

        public static void OpenTask(object taskContent)
        {
            GlobalSettingsSingletonController.Instance.TaskContent = taskContent;
            GlobalSettingsSingletonController.Instance.IsTaskOpen = true;
        }

        public static void ClosingTask()
        {
            GlobalSettingsSingletonController.Instance.IsTaskOpen = false;
        }
    }
    public enum StartAppType
    {
        Normal,
        WfMag,
        Hidden
    }
}