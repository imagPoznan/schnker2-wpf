﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schenker2.Extensions
{
	public static class ValueConverter
	{
		public static decimal ConvertToDecimal(this string input)
		{
			try
			{
				if (decimal.TryParse(input, out decimal result))
					return result;

				if (decimal.TryParse(input.Replace(",", "."), out decimal result2))
					return result2;

				if (decimal.TryParse(input.Replace(".", ","), out decimal result3))
					return result3;

				return 0;
			}
			catch (Exception ex)
			{
				Logs.AddLogError($@"Błąd ConvertToDecimal() - inpu:{input} + ex: {ex}");
				return 0;
				throw;
			}
		}
		public static int ConvertToInt(this string input)
		{
			try
			{
				if (int.TryParse(input, out int result))
					return result;

				if (int.TryParse(input.Replace(",", "."), out int result2))
					return result2;

				if (int.TryParse(input.Replace(".", ","), out int result3))
					return result3;

				return 0;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
	}
}
