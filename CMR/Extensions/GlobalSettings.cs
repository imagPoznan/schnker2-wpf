﻿namespace Schenker2.Extensions
{
    //public class GlobalSettings : BindableBaseImpl
    //{
    //    /// <summary>
    //    /// VERSION APP
    //    /// </summary>
    //    public const decimal VersionApp = (decimal)1.0;

    //    public static GlobalSettings Instance { get; set; } = new GlobalSettings();

    //    private List<string> _allTypy = new List<string>() { "Windows Authentication", "SQL Server Authentication" };
        
    //    private int _infoMailBefore = 3;
    //    private int _infoMailAfter = 0;
    //    private int _infoMailAfterRepeat = 1;
    //    private decimal _sumaNiezaplaconychFaktur = 0;
    //    private int _liczbaKontrahentow = 0;
    //    private int _liczbaDokumentow = 0;
    //    private int _rowPerPage;
    //    private string _numerLicencji = Settings.Default.vNumerLicencji;
    //    private bool _isLicense = false;
    //    private LicenseManagerController.LicencjaType _licenseType = LicenseManagerController.LicencjaType.Brak;
    //    private string _dataWygasnieciaLicencji;
    //    private List<string> _drukarki;
    //    private string _drukarkaRaportow = Settings.Default.vDrukarkaRaportow;
    //    private string _cmrRaportPath = Settings.Default.vCmrRaportPath;
    //    private bool _printAfterGenerateReport = Settings.Default.vPrintAfterGenerateReport;
    //    private bool _isTaskOpen;
    //    private object _taskContent;
    //    private bool _isActiveTaskScheduler;

    //    public readonly string NameTaskScheduler = "Empty project IMAG";

    //    private GlobalSettings() { 
    //        //Singleton 
    //    }

    //    public string TextBrakLicencji = "Brak licencji - nie można wykonać zadania!";

    //    public StartAppType StartAppType { get; set; }

    //    public int IdDokWz { get; set; }

    //    public List<string> AllTypy
    //    {
    //        get { return _allTypy; }
    //        set
    //        {
    //            _allTypy = value;
    //            OnPropertyChanged(() => AllTypy);
    //        }
    //    }

    //    public int InfoMailBefore
    //    {
    //        get { return _infoMailBefore; }
    //        set
    //        {
    //            _infoMailBefore = value;
    //            OnPropertyChanged(() => InfoMailBefore);
    //        }
    //    }

    //    public int InfoMailAfter
    //    {
    //        get { return _infoMailAfter; }
    //        set
    //        {
    //            _infoMailAfter = value;
    //            OnPropertyChanged(() => InfoMailAfter);
    //        }
    //    }

    //    public int InfoMailAfterRepeat
    //    {
    //        get { return _infoMailAfterRepeat; }
    //        set
    //        {
    //            _infoMailAfterRepeat = value;
    //            OnPropertyChanged(() => InfoMailAfterRepeat);
    //        }
    //    }

    //    public decimal SumaNiezaplaconychFaktur
    //    {
    //        get { return _sumaNiezaplaconychFaktur; }
    //        set
    //        {
    //            _sumaNiezaplaconychFaktur = value;
    //            OnPropertyChanged(() => SumaNiezaplaconychFaktur);
    //        }
    //    }
        
    //    public int LiczbaKontrahentow
    //    {
    //        get { return _liczbaKontrahentow; }
    //        set
    //        {
    //            _liczbaKontrahentow = value;
    //            OnPropertyChanged(() => LiczbaKontrahentow);
    //        }
    //    }
        
    //    public int LiczbaDokumentow
    //    {
    //        get { return _liczbaDokumentow; }
    //        set
    //        {
    //            _liczbaDokumentow = value;
    //            OnPropertyChanged(() => LiczbaDokumentow);
    //        }
    //    }

    //    public int RowPerPage
    //    {
    //        get { return _rowPerPage; }
    //        set
    //        {
    //            _rowPerPage = value;
    //            OnPropertyChanged(() => RowPerPage);
    //        }
    //    }

    //    public string NumerLicencji
    //    {
    //        get { return _numerLicencji; }
    //        set
    //        {
    //            _numerLicencji = value;
    //            OnPropertyChanged(() => NumerLicencji);
    //            Settings.Default.vNumerLicencji = NumerLicencji;
    //            Settings.Default.Save();
    //        }
    //    }

    //    public string DataWygasnieciaLicencji
    //    {
    //        get { return _dataWygasnieciaLicencji; }
    //        set
    //        {
    //            _dataWygasnieciaLicencji = value;
    //            OnPropertyChanged(() => DataWygasnieciaLicencji);
    //        }
    //    }

    //    public List<string> Drukarki
    //    {
    //        get { return PrinterSettings.InstalledPrinters.Cast<string>().ToList(); }
    //        set
    //        {
    //            _drukarki = value;
    //            OnPropertyChanged(() => Drukarki);
    //        }
    //    }

    //    public string DrukarkaRaportow
    //    {
    //        get { return _drukarkaRaportow; }
    //        set
    //        {
    //            _drukarkaRaportow = value;
    //            OnPropertyChanged(() => DrukarkaRaportow);
    //            Settings.Default.vDrukarkaRaportow = DrukarkaRaportow;
    //            Settings.Default.Save();
    //        }
    //    }

    //    public string CmrRaportPath
    //    {
    //        get { return _cmrRaportPath; }
    //        set
    //        {
    //            _cmrRaportPath = value;
    //            OnPropertyChanged(() => CmrRaportPath);
    //            Settings.Default.vCmrRaportPath = CmrRaportPath;
    //            Settings.Default.Save();
    //        }
    //    }

    //    public bool PrintAfterGenerateReport
    //    {
    //        get { return _printAfterGenerateReport; }
    //        set
    //        {
    //            _printAfterGenerateReport = value;
    //            OnPropertyChanged(() => PrintAfterGenerateReport);
    //            Settings.Default.vPrintAfterGenerateReport = PrintAfterGenerateReport;
    //            Settings.Default.Save();
    //        }
    //    }

    //    public bool IsLicense
    //    {
    //        get { return _isLicense; }
    //        set
    //        {
    //            if (_isLicense == value) return;
    //            _isLicense = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public LicenseManagerController.LicencjaType LicenseType
    //    {
    //        get { return _licenseType; }
    //        set
    //        {
    //            if (_licenseType == value) return;
    //            _licenseType = value;
    //            OnPropertyChanged();
    //        }
    //    }
        
    //    public bool IsTaskOpen
    //    {
    //        get { return _isTaskOpen; }
    //        set
    //        {
    //            if (_isTaskOpen == value) return;
    //            _isTaskOpen = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public object TaskContent
    //    {
    //        get { return _taskContent; }
    //        set
    //        {
    //            if (_taskContent == value) return;
    //            _taskContent = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public bool IsActiveTaskScheduler
    //    {
    //        get { return _isActiveTaskScheduler; }
    //        set
    //        {
    //            _isActiveTaskScheduler = value;
    //            OnPropertyChanged(() => IsActiveTaskScheduler);
    //        }
    //    }

    //    public decimal SelectedIdAdresuFirmy { get; set; } = Settings.Default.vSelectedIdAdresuFirmy;

    //    public FieldsFromEnum SelectedDaneOdbiorcy = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedDaneOdbiorcy);

    //    public FieldsFromEnum SelectedMiejscePrzeznaczenia = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedMiejscePrzeznaczenia);

    //    public FieldsFromEnum SelectedMiejsceZaladowania = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedMiejsceZaladowania);

    //    public FieldsFromEnum SelectedDataZaladowania = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedDataZaladowania);

    //    public FieldsFromEnum SelectedPrzewoznik = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedPrzewoznik);
        
    //    public FieldsFromEnum SelectedKolejnyPrzewoznik = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedKolejnyPrzewoznik);

    //    public FieldsFromEnum SelectedTowar = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedTowar);

    //    public FieldsFromEnum SelectedNrRejPrzewoznik = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedNrRejPrzewoznik);

    //    public FieldsFromEnum SelectedNrRejKolejnyPrzewoznik = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedNrRejKolejnyPrzewoznik);

    //    public FieldsFromEnum SelectedOpakowanie = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedOpakowanie);

    //    public FieldsFromEnum SelectedRodzajTowar = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedRodzajTowar);

    //    public FieldsFromEnum SelectedNrStatystyczny = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedNrStatystyczny);

    //    public FieldsFromEnum SelectedInstrukcjeNadawcy = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedInstrukcjeNadawcy);

    //    public FieldsFromEnum SelectedPostanowieniePrzewozny = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedPostanowieniePrzewozny);

    //    public FieldsFromEnum SelectedZaplata = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedZaplata);

    //    public FieldsFromEnum SelectedZastrzezeniaIUwagiPrzewoznika = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedZastrzezeniaIUwagiPrzewoznika);

    //    public FieldsFromEnum SelectedPostanowieniaSpecjalne = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedPostanowieniaSpecjalne);

    //    public FieldsFromEnum SelectedWystawionoW = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedWystawionoW);

    //    public FieldsFromEnum SelectedPodpisNadawcy = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedPodpisNadawcy);

    //    public FieldsFromEnum SelectedPodpisPrzewoznika = EnumHelper.ParseEnum<FieldsFromEnum>(Settings.Default.vSelectedPodpisPrzewoznika);


    //    public bool SelectedZalaczFv = Settings.Default.vSelectedZalaczFv;
    //    public bool SelectedZalaczPole1 = Settings.Default.vSelectedZalaczPole1;
    //    public bool SelectedZalaczPole2 = Settings.Default.vSelectedZalaczPole2;
    //    public bool SelectedZalaczPole3 = Settings.Default.vSelectedZalaczPole3;
    //    public bool SelectedZalaczPole4 = Settings.Default.vSelectedZalaczPole4;
    //    public bool SelectedZalaczPole5 = Settings.Default.vSelectedZalaczPole5;
    //    public bool SelectedZalaczPole6 = Settings.Default.vSelectedZalaczPole6;
    //    public bool SelectedZalaczPole7 = Settings.Default.vSelectedZalaczPole7;
    //    public bool SelectedZalaczPole8 = Settings.Default.vSelectedZalaczPole8;
    //    public bool SelectedZalaczPole9 = Settings.Default.vSelectedZalaczPole9;
    //    public bool SelectedZalaczPole10 = Settings.Default.vSelectedZalaczPole10;



    //}
}
