﻿using Schenker2.Controllers;
using Schenker2.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Schenker2.Extensions
{
    public static class MboxHiddenModeHelper
    {
        public static async Task ErrorLogs(string error)
        {

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.Hidden)
            {
                Logs.AddLogError(error);
            }
            else
            {
                Logs.AddLogError(error);
                //ActionsController.ShowMessagebox($@"Wystąpił błąd: {error}");
                //MessageBox.Show($@"Wystąpił błąd: {error}");
                await ActionsController.ShowMessagebox($@"Wystąpił błąd: {error}");
            }
        }
        public static async Task InfoLogs(string info)
        {
            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.Hidden)
            {
                Logs.AddLogInfo(info);
            }
            else
            {
                Logs.AddLogInfo(info);
                //MessageBox.Show(info);
                await ActionsController.ShowMessagebox(info);
            }
        }
    }
}
