﻿using System;
using NLog;

namespace Schenker2.Extensions
{
    public static class Logs
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        public static void AddLogError(Exception ex)
        {
            try
            {
#if DEBUG
                Logger.Error(ex.ToString());
#else
                Logger.Error(ex.Message);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }
        public static void AddLogError(string ex)
        {
            try
            {
#if DEBUG
                Logger.Error(ex.ToString());
#else
                Logger.Error(ex.Message);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void AddLogInfo(Exception ex)
        {
            try
            {
#if DEBUG
                Logger.Info(ex.ToString());
#else
                Logger.Info(ex.Message);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }
        public static void AddLogInfo(string ex)
        {
            try
            {
#if DEBUG
                Logger.Info(ex.ToString());
#else
                Logger.Info(ex.Message);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void AddLogWarrning(Exception ex)
        {
            try
            {
#if DEBUG
                Logger.Warn(ex.ToString());
#else
                Logger.Warn(ex.Message);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

    }
}
