﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Schenker2.Controllers;
using Schenker2.ModelViews;
using Schenker2.Views;
using Prism.Commands;

namespace Schenker2.Extensions
{
    public class LoaderAwaitableDelegateCommand : LoaderAwaitableDelegateCommand<object>, IAsyncCommand
    {
        public LoaderAwaitableDelegateCommand(Func<Task> executeMethod)
            : base(o => executeMethod())
        {
        }

        public LoaderAwaitableDelegateCommand(Func<Task> executeMethod, Func<bool> canExecuteMethod)
            : base(o => executeMethod(), o => canExecuteMethod())
        {
        }
    }

    public class LoaderAwaitableDelegateCommand<T> : IAsyncCommand<T>, ICommand
    {
        private readonly Func<T, Task> _executeMethod;
        private readonly DelegateCommand<T> _underlyingCommand;
        private bool _isExecuting;

        public LoaderAwaitableDelegateCommand(Func<T, Task> executeMethod)
            : this(executeMethod, _ => true)
        {
        }

        public LoaderAwaitableDelegateCommand(Func<T, Task> executeMethod, Func<T, bool> canExecuteMethod)
        {
            _executeMethod = executeMethod;
            _underlyingCommand = new DelegateCommand<T>(x => { }, canExecuteMethod);
        }

        public async Task ExecuteAsync(T obj)
        {
            try
            {
                MainWindowViewModel.OpenTask(new DialogLoading());

                _isExecuting = true;
                RaiseCanExecuteChanged();
                await _executeMethod(obj);
                
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                MainWindowViewModel.ClosingTask();
                await ActionsController.ShowMessagebox(ex.Message);
            }
            finally
            {
                MainWindowViewModel.ClosingTask();
                _isExecuting = false;
                RaiseCanExecuteChanged();
            }
        }

        public ICommand Command => this;

        public bool CanExecute(object parameter)
        {
            return !_isExecuting && _underlyingCommand.CanExecute((T)parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { _underlyingCommand.CanExecuteChanged += value; }
            remove { _underlyingCommand.CanExecuteChanged -= value; }
        }

        public async void Execute(object parameter)
        {
            await ExecuteAsync((T)parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            _underlyingCommand.RaiseCanExecuteChanged();
        }
    }
}
