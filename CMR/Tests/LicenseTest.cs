﻿using System;
using Schenker2.Controllers;
using NUnit.Framework;

namespace Schenker2.Tests
{
    [TestFixture]
    internal class LicenseTest
    {
        [Test]
        public void GetLicenseTest()
        {
            LicenseManagerController.LicencjaType licencjaType;
            DateTime? dataWygasniecia;

            Assert.AreEqual(LicenseManagerController.GetLicense(GlobalSettingsSingletonController.Instance.NumerLicencji, out licencjaType, out dataWygasniecia), true);

            Assert.AreEqual(LicenseManagerController.GetLicense("", out licencjaType, out dataWygasniecia), false);

        }
    }
}
