﻿using System;
using Schenker2.Extensions;
using NUnit.Framework;

namespace Schenker2.Tests
{
    [TestFixture]
    class DateHelperTest
    {
        [Test]
        public void InTtoDateTest()
        {
            Assert.AreEqual(DateHelper.InTtoDate(0), new DateTime());
            Assert.AreEqual(DateHelper.InTtoDate(36162), new DateTime());
            Assert.AreEqual(DateHelper.InTtoDate(78857), new DateTime(2016,11,22,00,00,00));
        }
        [Test]
        public void DateToIntTest()
        {
            Assert.AreEqual(DateHelper.DateToInt(new DateTime(2016, 11, 22, 00, 00, 00)), 78857);
            Assert.AreEqual(DateHelper.DateToInt(new DateTime(2011, 11, 22, 00, 00, 00)), 77030);
            Assert.AreEqual(DateHelper.DateToInt(new DateTime(1990, 01, 01, 10, 59, 59)), 69035);
        }
    }
}
