﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Schenker2.Controllers;
using NUnit.Framework;

namespace Schenker2.Tests
{
    [TestFixture]
    internal class SerializacjaTest
    {

        [Test]
        public void Test()
        {
            MemoryStream mem = new MemoryStream();
            BinaryFormatter b = new BinaryFormatter();
            try
            {
              b.Serialize(mem, GlobalSettingsFileSingletonController.Instance);
              
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
       
        public static Stream Serialize(object source)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            formatter.Serialize(stream, GlobalSettingsFileSingletonController.Instance);
            return stream;
        }

        public static T Deserialize<T>(Stream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Position = 0;
            return (T)formatter.Deserialize(stream);
        }

        public static T Clone<T>(object source)
        {
            return Deserialize<T>(Serialize(GlobalSettingsFileSingletonController.Instance));
        }



    }
}
