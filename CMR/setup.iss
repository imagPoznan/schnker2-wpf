; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Windykator"
#define MyAppVersion "1.5.0"
#define MyAppPublisher "Imag"
#define MyAppURL "http://imag.pl"
#define MyAppExeName "Windykator.exe"   
#define MyAppCopyright "IMAG DARIUSZ BILI�SKI WWW.IMAG.PL"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{7B32B9B6-67EB-44CD-AAD8-0768B5E3F5C2}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName=C:/Imag\{#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename=setup
SetupIconFile=C:\PROJEKTY\c#\Windykator\Windykator\imag.ico
Compression=lzma
SolidCompression=yes
AppCopyright={#MyAppCopyright}
AppContact={#MyAppURL}
[Languages]
Name: "polish"; MessagesFile: "compiler:Languages\Polish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Windykator.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\EntityFramework.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\EntityFramework.SqlServer.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\EntityFramework.SqlServer.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\EntityFramework.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MahApps.Metro.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MahApps.Metro.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MahApps.Metro.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MaterialDesignColors.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MaterialDesignThemes.Wpf.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MaterialDesignThemes.Wpf.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\MaterialDesignThemes.Wpf.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Microsoft.Win32.TaskScheduler.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\NLog.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\NLog.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\NLog.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Prism.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Prism.Wpf.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\System.Windows.Interactivity.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Windykator.application"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Windykator.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Windykator.exe.manifest"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Windykator.vshost.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\WpfRichText.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\WpfRichText.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.AvalonDock.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.AvalonDock.Themes.Aero.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.AvalonDock.Themes.Metro.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.AvalonDock.Themes.VS2010.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.DataGrid.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\PROJEKTY\c#\Windykator\Windykator\bin\Release\Xceed.Wpf.Toolkit.dll"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

