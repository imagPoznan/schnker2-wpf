﻿using System;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Repository;

namespace Schenker2.Controllers
{
    class OperacjeDodatkoweController
    {
        public static bool DodajDoDokumentuMagazynowego()
        {
            try
            {
                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MagOperacjeDodatkowe()
                {
                    Nazwa = "Moduł Schenker IMAG",
                    KodGrupy = "DOKMAG_BRW",
                    KodOperacji = "1111_000_SCHENKERIMAG",
                    Grupowanie = 1,
                    Parametry = "0001000010000",
                    Sciezka = path,
                    SkrotKlawiszowy = String.Empty,
                    SkrotKlawiszowyKod = 0,
                    FunkcjaZewnetrzna = 1,
                    OdswiezListe = 0,
                    KasujZaznaczenie = 0
                };

                var repo =
                    new OperacjeDodatkoweRepository(new DataBaseContext());

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }

        public static bool DodajDoZamowienia()
        {
            try
            {
                
                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MagOperacjeDodatkowe()
                {
                    Nazwa = "Moduł Schenker IMAG",
                    KodGrupy = "ZAM_BRW",
                    KodOperacji = "1111_000_SchenkerV2",
                    Grupowanie = 1,
                    Parametry = "0001000010000",
                    Sciezka = path,
                    SkrotKlawiszowy = String.Empty,
                    SkrotKlawiszowyKod = 0,
                    FunkcjaZewnetrzna = 1,
                    OdswiezListe = 0,
                    KasujZaznaczenie = 0
                };

                var repo = new OperacjeDodatkoweRepository(new DataBaseContext());

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }
        public static bool DodajDoDokumentuHandlowego() //TODO
        {
            try
            {

                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MagOperacjeDodatkowe()
                {
                    Nazwa = "Moduł Schenker IMAG",
                    KodGrupy = "DOKHAN_BRW",
                    KodOperacji = "1111_000_Moduł Schenker IMAG",
                    Grupowanie = 1,
                    Parametry = "0001000010000",
                    Sciezka = path,
                    SkrotKlawiszowy = String.Empty,
                    SkrotKlawiszowyKod = 0,
                    FunkcjaZewnetrzna = 1,
                    OdswiezListe = 0,
                    KasujZaznaczenie = 0
                };

                var repo =
                    new OperacjeDodatkoweRepository(new DataBaseContext());

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }
    }
}
