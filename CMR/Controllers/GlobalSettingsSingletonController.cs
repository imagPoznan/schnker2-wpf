﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Printing;
using System.Linq;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.ModelViews;
using Schenker2.Models.Models;
using System;

namespace Schenker2.Controllers
{
    public class GlobalSettingsSingletonController : BindableBaseImpl
    {
        /// <summary>
        /// VERSION APP
        /// </summary>
        public const decimal VersionApp = (decimal)1.0;

        public static GlobalSettingsSingletonController Instance { get; set; } = new GlobalSettingsSingletonController();

        private static GlobalSettingsFileSingletonController FileSingletonGlobalSettings { get;} = GlobalSettingsFileSingletonController.Instance;
        

        private List<string> _allTypy = new List<string>() { "Windows Authentication", "SQL Server Authentication" };

        private int _infoMailBefore = 3;
        private int _infoMailAfter;
        private int _infoMailAfterRepeat = 1;
        private decimal _sumaNiezaplaconychFaktur;
        private int _liczbaKontrahentow;
        private int _liczbaDokumentow;
        private int _rowPerPage;
        private bool _isLicense;
        private LicenseManagerController.LicencjaType _licenseType = LicenseManagerController.LicencjaType.Brak;
        private string _dataWygasnieciaLicencji;

        public List<string> SymbolePrzesylek
        {
            get { return _symbolePRzesylek; }
            set
            {
                _symbolePRzesylek = value;
                OnPropertyChanged();
            }
        }

        private List<string> _drukarki;
        private bool _isTaskOpen;
        private object _taskContent;
        private bool _isActive;


       // public decimal SelectedIdAdresuFirmy { get; set; } = 0;

        public bool SelectedZalaczFv { get; set; }

        public readonly string NameTaskScheduler = "Empty project IMAG";
        public ObservableCollection<DanePrzesylki> DanePrzesylki
        {
            get { return _danePrzesylki; }
            set
            {
                _danePrzesylki = value;
                OnPropertyChanged();
            }
        }
        private GlobalSettingsSingletonController()
        {
            //Singleton 
        }
        public string LoginSchenker
        {
            get { return FileSingletonGlobalSettings.LoginSchenker; }

            set
            {
                FileSingletonGlobalSettings.LoginSchenker = value;
                OnPropertyChanged();
            }
        }
        public string HasloSchenker
        {
            get { return FileSingletonGlobalSettings.HasloSchenker; }

            set
            {
                FileSingletonGlobalSettings.HasloSchenker = value;
                OnPropertyChanged();
            }
        }
        public string NrKlientaSchenker
        {
            get { return FileSingletonGlobalSettings.NrKlientaSchenker; }

            set
            {
                FileSingletonGlobalSettings.NrKlientaSchenker = value;
                OnPropertyChanged();
            }
        }
        public bool IsSystem
        {
            get { return FileSingletonGlobalSettings.IsSystem; }

            set
            {
                FileSingletonGlobalSettings.IsSystem = value;
                OnPropertyChanged();
            }
        }
        public bool IsParcel
        {
            get { return FileSingletonGlobalSettings.IsParcel; }

            set
            {
                FileSingletonGlobalSettings.IsParcel = value;
                OnPropertyChanged();
            }
        }
        //public bool IsSystemPremium
        //{
        //    get { return FileSingletonGlobalSettings.IsSystemPremium; }

        //    set
        //    {
        //        FileSingletonGlobalSettings.IsSystemPremium = value;
        //        OnPropertyChanged();
        //    }
        //}
        //public bool IsParcelPremium
        //{
        //    get { return FileSingletonGlobalSettings.IsParcelPremium; }

        //    set
        //    {
        //        FileSingletonGlobalSettings.IsParcelPremium = value;
        //        OnPropertyChanged();
        //    }
        //}
        public DateTime PickUpFromTime
        {
            get { return _pickUpFromTime; }
            set
            {
                _pickUpFromTime = value;
                OnPropertyChanged();

            }
        }
        public DateTime PickUpToTime
        {
            get { return _pickUpToTime; }
            set
            {
                _pickUpToTime = value;
                OnPropertyChanged();

            }
        }
        public DateTime DeliveryFromTime
        {
            get { return _deliveryFromTime; }
            set
            {
                _deliveryFromTime = value;
                OnPropertyChanged();

            }
        }
        public DateTime DeliveryToTime
        {
            get { return _deliveryToTime; }
            set
            {
                _deliveryToTime = value;
                OnPropertyChanged();

            }
        }
        public decimal ParcelWagaLacznie
        {
            get { return _parcelWagaLacznie; }
            set
            {
                _parcelWagaLacznie = value;
                OnPropertyChanged();

            }
        }
        public int ParcelLiczbaPaczek
        {
            get { return _parcelLiczbaPaczek; }
            set
            {
                _parcelLiczbaPaczek = value;
                OnPropertyChanged();

            }
        }
        public FieldsFromEnum SelectedMiejsceZapisuNumerListu
        {
            get { return FileSingletonGlobalSettings.SelectedMiejsceZapisuNumerListu; }
            set
            {
                FileSingletonGlobalSettings.SelectedMiejsceZapisuNumerListu = value;
                OnPropertyChanged();

            }
        }
        public DateTime PickupFromDate
        {
            get { return FileSingletonGlobalSettings.PickupFromDate; }

            set
            {
                FileSingletonGlobalSettings.PickupFromDate = value;
                
                if (FileSingletonGlobalSettings.PickupFromDate.DayOfYear != PickupToDate.DayOfYear)
                {
                    PickupToDate = value;
                    //
                    var newData = value.AddDays(1);

                    if (newData.DayOfWeek == DayOfWeek.Saturday)
                        newData = newData.AddDays(2);

                    if (newData.DayOfWeek == DayOfWeek.Sunday)
                        newData = newData.AddDays(1);
                    DeliveryFromDate = newData;
                    DeliveryToDate = newData;
                    //

                }
                OnPropertyChanged();
            }
        }
        public DateTime PickupToDate
        {
            get { return FileSingletonGlobalSettings.PickupToDate; }

            set
            {
                FileSingletonGlobalSettings.PickupToDate = value;
                if (FileSingletonGlobalSettings.PickupToDate.DayOfYear != PickupFromDate.DayOfYear)
                {
                    PickupFromDate = value;
                    //
                    var newData = value.AddDays(1);

                    if (newData.DayOfWeek == DayOfWeek.Saturday)
                        newData = newData.AddDays(2);

                    if (newData.DayOfWeek == DayOfWeek.Sunday)
                        newData = newData.AddDays(1);
                    DeliveryFromDate = newData;
                    DeliveryToDate = newData;
                    //
                }
                OnPropertyChanged();
            }
        }
        public DateTime DeliveryFromDate
        {
            get { return _deliveryFromDate; }

            set
            {
                _deliveryFromDate = value;

                if (_deliveryFromDate.DayOfYear != DeliveryToDate.DayOfYear)
                {
                    DeliveryToDate = value;
                }
                OnPropertyChanged();
            }
        }
        public DateTime DeliveryToDate
        {
            get { return _deliveryToDate; }

            set
            {
                _deliveryToDate = value;
                if (_deliveryToDate.DayOfYear != DeliveryFromDate.DayOfYear)
                {
                    DeliveryFromDate = value;
                }
                OnPropertyChanged();
            }
        }
        public DateTime GenerateFromDate
        {
            get { return FileSingletonGlobalSettings.GenerateFromDate; }

            set
            {
                FileSingletonGlobalSettings.GenerateFromDate = value;
                OnPropertyChanged();
            }
        }
        public DateTime GenerateToDate
        {
            get { return FileSingletonGlobalSettings.GenerateToDate; }

            set
            {
                FileSingletonGlobalSettings.GenerateToDate = value;
                OnPropertyChanged();
            }
        }
        public string Uwagi
        {
            get { return FileSingletonGlobalSettings.Uwagi; }

            set
            {
                FileSingletonGlobalSettings.Uwagi = value;
                OnPropertyChanged();
            }
        }

        public string InstrukcjaDostarczenia
        {
            get { return _instrukcjaDostarczenia; }

            set
            {
                _instrukcjaDostarczenia = value;
                OnPropertyChanged();
            }
        }
        public string SzukaniePoNazwieKontrahentaFraza
        {
            get { return _szukaniePoNazwieKontrahentaFraza; }

            set
            {
                _szukaniePoNazwieKontrahentaFraza = value;
               // if (value.Length> 1)
                //{
                    PrzesylkiNadane= ImagSchenker.Instance().GetByFilters();
                //}
                OnPropertyChanged();
            }
        }
        public ObservableCollection<ImagSchenker> PrzesylkiNadane
        {
            get { return _przesylkiNadane; }
            set
            {
                _przesylkiNadane = value;
                OnPropertyChanged();
            }
        }
        public string SzukaniePoNrListu
        {
            get { return _szukaniePoNrLiostu; }

            set
            {
                _szukaniePoNrLiostu = value;
               // if (value.Length > 1)
               // {
                    PrzesylkiNadane = ImagSchenker.Instance().GetByFilters();
               // }
                OnPropertyChanged();
            }
        }
        public string KwotaPobrania   {
            get { return _kwotaPobrania; }

            set
            {
                _kwotaPobrania = value;
                OnPropertyChanged();
}
        }
        public string KwotaDeklarowanejWartosci
        {
            get { return _kwotaDeklarowanejWartosci; }

            set
            {
                _kwotaDeklarowanejWartosci = value;
                OnPropertyChanged();
            }
        }
        public bool IsDeklarowanaWartosc
        {
            get { return _isDeklarowanaWartosc; }

            set
            {
                _isDeklarowanaWartosc = value;
                OnPropertyChanged();
            }
        }
        public bool Wniesienie
        {
            get { return _wniesienie; }

            set
            {
                _wniesienie = value;
                OnPropertyChanged();
            }
        }
        public bool Wyladunek
        {
            get { return _wyladunek; }

            set
            {
                _wyladunek = value;
                OnPropertyChanged();
            }
        }
        public bool AwizacjaDosieci
        {
            get { return _awizacjaDosieci; }

            set
            {
                _awizacjaDosieci = value;
                OnPropertyChanged();
            }
        }
        public bool PlaciOdbiorca
        {
            get { return _placiOdbiorca; }

            set
            {
                _placiOdbiorca = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaDo10
        {
            get { return _dostawaDo10; }

            set
            {
                _dostawaDo10 = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaNaDanyDzien
        {
            get { return _dostawaNaDanyDzien; }

            set
            {
                _dostawaNaDanyDzien = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaDo13
        {
            get { return _dostawaDo13; }

            set
            {
                _dostawaDo13 = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaPo16
        {
            get { return _dostawaPo16; }

            set
            {
                _dostawaPo16 = value;
                OnPropertyChanged();
            }
        }
        public int SelectedReferencesIndex
        {
            get { return _selectedReferencesIndex; }

            set
            {
                _selectedReferencesIndex = value;
                OnPropertyChanged();
            }
        }
        public string ReferenceValue
        {
            get { return _referenceValue; }

            set
            {
                _referenceValue = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaDo10Premium
        {
            get { return _dostawaDo10Premium; }

            set
            {
                _dostawaDo10Premium = value;
                OnPropertyChanged();
            }
        }
        public bool PolecenieOdbioru
        {
            get { return _polecenieOdbioru; }

            set
            {
                _polecenieOdbioru = value;
                OnPropertyChanged();
            }
        }
        public bool AwizacjaEmail
        {
            get { return _awizoEmail; }

            set
            {
                _awizoEmail = value;
                OnPropertyChanged();
            }
        }
        public bool AwizacjaTelefoniczna
        {
            get { return _awizoTelefoniczna; }

            set
            {
                _awizoTelefoniczna = value;
                OnPropertyChanged();
            }
        }
        public bool AwizacjaSms
        {
            get { return _awizoSms; }

            set
            {
                _awizoSms = value;
                OnPropertyChanged();
            }
        }
      
        public bool SamochodWindaOdbior
        {
            get { return _samochodWindaOdbior; }

            set
            {
                _samochodWindaOdbior = value;
                OnPropertyChanged();
            }
        }
        public bool SamochodWindaDostawa
        {
            get { return _samochodWindaDostawa; }

            set
            {
                _samochodWindaDostawa = value;
                OnPropertyChanged();
            }
        }
        public bool BrakOdbioruNaczepaLadownosc24tony
        {
            get { return _brakOdbioruNaczepaLadownosc24tony; }

            set
            {
                _brakOdbioruNaczepaLadownosc24tony = value;
                OnPropertyChanged();
            }
        }
        public bool BrakDostawyNaczepaLadownosc24tony
        {
            get { return _brakDostawyNaczepaLadownosc24tony; }

            set
            {
                _brakDostawyNaczepaLadownosc24tony = value;
                OnPropertyChanged();
            }
        }
        public bool DostawaDo13Premium
        {
            get { return _dostawaDo13Premium; }

            set
            {
                _dostawaDo13Premium = value;
                OnPropertyChanged();
            }
        }
        public bool IsPremium
        {
            get { return _isPremium; }

            set
            {
                _isPremium = value;
                OnPropertyChanged();
            }
        }
        //public bool IsPobranie
        //{
        //    get { return FileSingletonGlobalSettings.IsPobranie; }

        //    set
        //    {
        //        FileSingletonGlobalSettings.IsPobranie = value;
        //        OnPropertyChanged();
        //    }
        //}
        public bool IsZamowienie
        {
            get { return FileSingletonGlobalSettings.IsZamowienie; }

            set
            {
                FileSingletonGlobalSettings.IsZamowienie = value;
                OnPropertyChanged();
            }
        }

        public bool IsDokumentHandlowy
        {
            get { return FileSingletonGlobalSettings.IsDokumentHandlowy; }

            set
            {
                FileSingletonGlobalSettings.IsDokumentHandlowy = value;
                OnPropertyChanged();
            }
        }
        public bool IsDoumentMagazynowy
        {
            get { return FileSingletonGlobalSettings.IsDoumentMagazynowy; }

            set
            {
                FileSingletonGlobalSettings.IsDoumentMagazynowy = value;
                OnPropertyChanged();
            }
        }

        public string NadawcaFirma
        {
            get { return FileSingletonGlobalSettings.FirmaNadawca; }
            set
            {
                FileSingletonGlobalSettings.FirmaNadawca = value;
                OnPropertyChanged();

            }
        }
        public string ImieNazwiskoNadawca
        {
            get { return FileSingletonGlobalSettings.ImieNazwiskoNadawca; }
            set
            {
                FileSingletonGlobalSettings.ImieNazwiskoNadawca = value;
                OnPropertyChanged();

            }
        }
        public string TelefonNadawca
        {
            get { return FileSingletonGlobalSettings.TelefonNadawca; }
            set
            {
                FileSingletonGlobalSettings.TelefonNadawca = value;
                OnPropertyChanged();

            }
        }
        public string MailNadawca
        {
            get { return FileSingletonGlobalSettings.MailNadawca; }
            set
            {
                FileSingletonGlobalSettings.MailNadawca = value;
                OnPropertyChanged();

            }
        }
        public string NipNadawca
        {
            get { return FileSingletonGlobalSettings.NipNadawca; }
            set
            {
                FileSingletonGlobalSettings.NipNadawca = value;
                OnPropertyChanged();

            }
        }
        public string UlicaNadawca
        {
            get { return FileSingletonGlobalSettings.UlicaNadawca; }
            set
            {
                FileSingletonGlobalSettings.UlicaNadawca = value;
                OnPropertyChanged();

            }
        }
        public string KodPocztowyNadawca
        {
            get { return FileSingletonGlobalSettings.KodPocztowyNadawca; }
            set
            {
                FileSingletonGlobalSettings.KodPocztowyNadawca = value;
                OnPropertyChanged();

            }
        }
        public string MiejscowoscNadawca
        {
            get { return FileSingletonGlobalSettings.MiejscowoscNadawca; }
            set
            {
                FileSingletonGlobalSettings.MiejscowoscNadawca = value;
                OnPropertyChanged();

            }
        }
        public string KrajNadawca
        {
            get { return FileSingletonGlobalSettings.KrajNadawca; }
            set
            {
                FileSingletonGlobalSettings.KrajNadawca = value;
                OnPropertyChanged();

            }
        }
       

        public bool DrukujEtykiete
        {
            get { return FileSingletonGlobalSettings.PrintActiveEtykieta; }
            set
            {
                FileSingletonGlobalSettings.PrintActiveEtykieta = value;
                OnPropertyChanged();
             
            }
        }
        public bool DrukujLp
        {
            get { return FileSingletonGlobalSettings.PrintActiveLp; }
            set
            {
                FileSingletonGlobalSettings.PrintActiveLp = value;
                OnPropertyChanged();

            }
        }
        public bool ZapiszNumerListuPrzewozowego
        {
            get { return FileSingletonGlobalSettings.ZapiszNumerListuPrzewozowego; }
            set
            {
                FileSingletonGlobalSettings.ZapiszNumerListuPrzewozowego = value;
                OnPropertyChanged();

            }
        }
        public int IdDokWz { get; set; }
        public int IdZam { get; set; }
        public int IdDokHan { get; set; }
  
        public string DrukarkaEtykieta
        {
            get { return FileSingletonGlobalSettings.DrukarkaEtykieta; }
            set
            {
                FileSingletonGlobalSettings.DrukarkaEtykieta = value;
                OnPropertyChanged();
        
            }
        }
        public string DrukarkaLp
        {
            get { return FileSingletonGlobalSettings.DrukarkaLp; }
            set
            {
                FileSingletonGlobalSettings.DrukarkaLp = value;
                OnPropertyChanged();

            }
        }

        public string SciezkaZapisuEtykiety
        {
            get { return FileSingletonGlobalSettings.SciezkaZapisuEtykiet; }
            set
            {
                FileSingletonGlobalSettings.SciezkaZapisuEtykiet = value;
                OnPropertyChanged(() => SciezkaZapisuEtykiety);

            }
        }

        public string CmrRaportPath
        {
            get { return FileSingletonGlobalSettings.CmrRaportPath; }
            set
            {
                FileSingletonGlobalSettings.CmrRaportPath = value;
                OnPropertyChanged();
             
            }
        }
        public decimal SelectedDaneNadawcy
        {
            get { return FileSingletonGlobalSettings.SelectedDaneNadawcy; }
            set
            {
               FileSingletonGlobalSettings.SelectedDaneNadawcy = value;
                OnPropertyChanged();
               
            }
        }

        public FieldsFromEnum SelectedDaneOdbiorcy
        {
            get { return FileSingletonGlobalSettings.SelectedDaneOdbiorcy; }
            set
            {
                FileSingletonGlobalSettings.SelectedDaneOdbiorcy = value;
                OnPropertyChanged();
            
            }
        }

        //
        public string TextBrakLicencji = "Brak licencji - nie można wykonać zadania!";
        private bool _pobranie;
        private ObservableCollection<DanePrzesylki> _danePrzesylki;
        private List<string> _symbolePRzesylek;
        private string _kwotaPobrania;
        private string _kwotaDeklarowanejWartosci;
        private bool _isDeklarowanaWartosc;
        private string _instrukcjaDostarczenia;
        private string _numerPrzesylki;
        private decimal _parcelWagaLacznie;
        private int _parcelLiczbaPaczek;
        private bool _isPremium;
        private bool _wniesienie;
        private bool _wyladunek;
        private bool _awizacjaDosieci;
        private bool _placiOdbiorca;
        private bool _dostawaDo10;
        private bool _dostawaDo13;
        private bool _dostawaPo16;
        private bool _dostawaDo13Premium;
        private bool _dostawaDo10Premium;
        private bool _polecenieOdbioru;
        private bool _awizoEmail;
        private bool _awizoSms;
        private bool _samochodWindaOdbior;
        private bool _samochodWindaDostawa;
        private bool _brakOdbioruNaczepaLadownosc24tony;
        private bool _brakDostawyNaczepaLadownosc24tony;
        private bool _dostawaNaDanyDzien;
        private int _selectedReferencesIndex;
        private string _referenceValue;
        private string _szukaniePoNazwieKontrahentaFraza;
        private string _szukaniePoNrLiostu;
        private ObservableCollection<ImagSchenker> _przesylkiNadane;
        private bool _awizoTelefoniczna;
        private DateTime _deliveryFromDate;
        private DateTime _deliveryToDate;
        private DateTime _pickUpFromTime;
        private DateTime _pickUpToTime;
        private DateTime _deliveryFromTime;
        private DateTime _deliveryToTime;

        public StartAppType StartAppType { get; set; }

      
        public string DataWygasnieciaLicencji
        {
            get { return _dataWygasnieciaLicencji; }
            set
            {
                _dataWygasnieciaLicencji = value;
                OnPropertyChanged(() => DataWygasnieciaLicencji);
            }
        }

        public List<string> Drukarki
        {
            get { return PrinterSettings.InstalledPrinters.Cast<string>().ToList(); }
            set
            {
                _drukarki = value;
                OnPropertyChanged(() => Drukarki);
            }
        }

        public bool Pobranie 
        {
            get { return _pobranie; }
            set
            {
                if (_pobranie == value) return;
                _pobranie = value;
                OnPropertyChanged();
            }
        }
        public bool IsLicense
        {
            get { return _isLicense; }
            set
            {
                if (_isLicense == value) return;
                _isLicense = value;
                OnPropertyChanged();
            }
        }

        public LicenseManagerController.LicencjaType LicenseType
        {
            get { return _licenseType; }
            set
            {
                if (_licenseType == value) return;
                _licenseType = value;
                OnPropertyChanged();
            }
        }

        public bool IsTaskOpen
        {
            get { return _isTaskOpen; }
            set
            {
                if (_isTaskOpen == value) return;
                _isTaskOpen = value;
                OnPropertyChanged();
            }
        }

        public object TaskContent
        {
            get { return _taskContent; }
            set
            {
                if (_taskContent == value) return;
                _taskContent = value;
                OnPropertyChanged();
            }
        }

        public bool BreakAutoTask { get; set; }

      
        public string NumerPrzesylki
        {
            get { return _numerPrzesylki; }
            set
            {
                _numerPrzesylki = value;
                OnPropertyChanged();
            }
        }
        public string NumerLicencji
        {
            get { return FileSingletonGlobalSettings.NumerLicencji; }
            set
            {
                FileSingletonGlobalSettings.NumerLicencji = value;
                OnPropertyChanged();
            }
        }
        public string DbName
        {
            get { return FileSingletonGlobalSettings.DbName; }
            set
            {
                FileSingletonGlobalSettings.DbName = value;
                OnPropertyChanged();
            }
        }
        public string ServerName
        {
            get { return FileSingletonGlobalSettings.ServerName; }
            set
            {
                FileSingletonGlobalSettings.ServerName = value;
                OnPropertyChanged();
            }
        }
        public string Login
        {
            get { return FileSingletonGlobalSettings.Login; }
            set
            {
                FileSingletonGlobalSettings.Login = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get { return FileSingletonGlobalSettings.Password; }
            set
            {
                FileSingletonGlobalSettings.Password = value;
                OnPropertyChanged();
            }
        }
        public string Auth
        {
            get { return FileSingletonGlobalSettings.Auth; }
            set
            {
                FileSingletonGlobalSettings.Auth = value;
                OnPropertyChanged();
            }
        }
        public List<string> AllTypy
        {
            get { return _allTypy; }
            set
            {
                _allTypy = value;
                OnPropertyChanged(() => AllTypy);
            }
        }



    }
}