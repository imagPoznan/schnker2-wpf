﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;

namespace Schenker2.Controllers
{
    public sealed class DataBaseController
    {
        public static DataBaseController Instance { get; set; } = new DataBaseController();

        public string Auth
        {
            get { return GlobalSettingsFileSingletonController.Instance.Auth; }
            set { GlobalSettingsFileSingletonController.Instance.Auth = value; }
        }

        public string Login
        {
            get { return GlobalSettingsFileSingletonController.Instance.Login; }
            set { GlobalSettingsFileSingletonController.Instance.Login = value; }
        }

        public string Password
        {
            get { return GlobalSettingsFileSingletonController.Instance.Password; }
            set { GlobalSettingsFileSingletonController.Instance.Password = value; }
        }

        public string DbServerName
        {
            get { return GlobalSettingsFileSingletonController.Instance.ServerName; }
            set { GlobalSettingsFileSingletonController.Instance.ServerName = value; }
        }

        public string DbName
        {
            get { return GlobalSettingsSingletonController.Instance.DbName; }
            set { GlobalSettingsSingletonController.Instance.DbName = value; }
        }

        internal string PobierzNumerFakturyPoIdZamowienia()
        {
            var query = $@"select distinct dh.NUMER from ZAMOWIENIE z
                join POZYCJA_ZAMOWIENIA pz on pz.ID_ZAMOWIENIA = z.ID_ZAMOWIENIA
                join POZYCJA_DOKUMENTU_MAGAZYNOWEGO pzm on pzm.ID_POZ_ZAM = pz.ID_POZYCJI_ZAMOWIENIA
                join DOKUMENT_HANDLOWY dh on dh.ID_DOKUMENTU_HANDLOWEGO = pzm.ID_DOK_HANDLOWEGO
                where z.ID_ZAMOWIENIA = {GlobalSettingsSingletonController.Instance.IdZam}";
            return RunQueryWithoutEntityString(query);
        }
        public string RunQueryWithoutEntityString(string query)
        {
            try
            {
                using (var conn = new SqlConnection(CreateConnStrWithoutEntity()))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            return cmd.ExecuteScalar()?.ToString() ?? "";
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                Logs.AddLogError(new Exception("QUERY:" + query + ";" + ex));
                return "";
            }
        }
        private DataBaseController()
        {
        }

        public string CreateConnStrWithoutEntity()
        {
            var windowsAuthString = "Windows Authentication";
            var uwierzytelnienie = !Auth.Contains("SQL") ? windowsAuthString : $"User Id={Login};Password={Password};";

            var auth = uwierzytelnienie == windowsAuthString ? "Integrated Security=true;" : $"User Id={Login};Password={Password};";


            return $"Data Source={DbServerName};Initial Catalog={DbName};{auth}";
        }

        public static bool SaveDatabaseData(DataBaseController dbObject)
        {
            try
            {
                GlobalSettingsFileSingletonController.Instance.ServerName = dbObject.DbServerName;
                GlobalSettingsFileSingletonController.Instance.DbName = dbObject.DbName;
                GlobalSettingsFileSingletonController.Instance.Auth = dbObject.Auth;
                GlobalSettingsFileSingletonController.Instance.Login = dbObject.Login;
                GlobalSettingsFileSingletonController.Instance.Password = dbObject.Password;

                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }

        }

        public static void Reset()
        {
            Instance = new DataBaseController();
        }

        public bool RunQueryWithoutEntity(string query)
        {
            try
            {
                using (var conn = new SqlConnection(CreateConnStrWithoutEntity()))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(new Exception("QUERY:" + query + ";" + ex));
                return false;
            }
        }

        public bool CreateTables()
        {
            var boolReq = RunQueryWithoutEntity(QueryModel.Sprawdz());
            boolReq = RunQueryWithoutEntity(QueryModel.TypyOpakowan());
            boolReq = RunQueryWithoutEntity(QueryModel.Create());

            return boolReq;
        }

        public static bool SaveDatabaseDataAction(DataBaseController dataBaseSingletonController)
        {
            var resp = SaveDatabaseData(dataBaseSingletonController);
            Reset();

            return resp;
        }

        public static bool ConnectDatabase()
        {
            if (Instance == null || !Instance.CreateTables())
                throw new Exception("Wystąpił błąd podczas tworzenia tabel w bazie danych - sprawdź połaczenie z bazą danych");

            return true;

            //     if (isConnected)
            //   await ShowMessagebox(@"Baza danych jest gotowa do pracy");
        }
        public static void UzupelniSzablonyPaczek()
        {
            var getPackagesDictionary = SchenkerApiService.Create().GetPackagesDictionary();
            using (var db = new DataBaseContext())
            {
                foreach (var item in getPackagesDictionary)
                {
                    if (item.packCode == "PC" || item.packCode == "EC" || item.packCode == "TU")
                    {
                        continue;
                    }
                    var resp = db.ImagSchenkerTypyOpakowan.FirstOrDefault(x => x.SymbolPaczki == item.packCode);
                    if (resp == null)
                    {
                        var typOpakowania = new ImagSchenkerTypyOpakowan()
                        {
                            Dlugosc = item.length * 100,
                            SymbolPaczki = item.packCode,
                            NazwaOpakowania = item.packName,
                            Szerokosc = item.width * 100,
                            Wysokosc = item.height*100

                        };
                        db.ImagSchenkerTypyOpakowan.Add(typOpakowania);
                    }
                    
                }
                db.SaveChanges();
            }
        }
        public static System.Collections.Generic.List<ImagSchenkerTypyOpakowan> PobierzSzablonyPaczek()
        {
            try
            {
                using (var db = new DataBaseContext())
                {
                    return db.ImagSchenkerTypyOpakowan.ToList();
                }
            }
            catch (Exception ex )
            {
                Logs.AddLogError($@"PobierzSzablonyPaczek() błąd: {ex}");
                return new System.Collections.Generic.List<ImagSchenkerTypyOpakowan>();
            }
            
        }
    }
}
