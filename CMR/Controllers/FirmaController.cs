﻿using System;
using System.Collections.Generic;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.Models.Repository;

namespace Schenker2.Controllers
{
    class FirmaController
    {

        public static AdresModel GetById(decimal idAdresuFirma)
        {
            try
            {
                var repo = new FirmaRepository(new DataBaseContext());
                return repo.GetAdresFirmyByIdFirma(idAdresuFirma);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new AdresModel();
            }
        }

        public static Dictionary<decimal, string> GetAllAdresyFirmyAdd0()
        {
            try
            {
                var repo = new FirmaRepository(new DataBaseContext());
                var req = repo.GetAllAdresyFirmy();
                req.Add(0, "Nie pobieraj");
                return req;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new Dictionary<decimal, string>();
            }
        }
    }
}
