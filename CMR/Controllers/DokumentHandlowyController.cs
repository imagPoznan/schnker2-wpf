﻿using System;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.Models.Repository;
using Schenker2.ModelViews;

namespace Schenker2.Controllers
{
    internal class DokumentHandlowyController
    {
        public static DaneOdbiorcyModel GetComplexById(decimal id)
        {
            try
            {
                var repo = new DokumentHandlowyRepository(new DataBaseContext());
                return repo.GetComplexById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                if (ex.Message.Equals("The underlying provider failed on Open."))
                    throw new Exception("Nie można połaczyć się z bazą danych");
                throw new Exception(ex.Message);

            }
        }


        public static DaneOdbiorcyModel LoadedActionAsync(DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null)
                throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.WfMag)
                zamowienieModel = GetComplexById(GlobalSettingsSingletonController.Instance.IdDokHan);

            return zamowienieModel;
        }

        public static DaneOdbiorcyModel GetDokHandlowyByIdAsync(
             DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null) throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            return GetComplexById(GlobalSettingsSingletonController.Instance.IdDokHan);// TODO id zam
        }
    }
}
