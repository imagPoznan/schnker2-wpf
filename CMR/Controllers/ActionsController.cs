﻿using System;
using System.Threading.Tasks;
using Schenker2.ModelViews;
using Schenker2.Views;
using MaterialDesignThemes.Wpf;

namespace Schenker2.Controllers
{
    /// <summary>
    /// Klasa wykonująca tylko statyczne akcje. Pośrednik między VievModel a Controller
    /// </summary>
    public class ActionsController
    {
        public static bool CreateOperationWfMagActionAsync()
        {
            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            if (GlobalSettingsSingletonController.Instance.IsZamowienie)
                return OperacjeDodatkoweController.DodajDoZamowienia();

            if (GlobalSettingsSingletonController.Instance.IsDokumentHandlowy)
                return OperacjeDodatkoweController.DodajDoDokumentuHandlowego();

            else return OperacjeDodatkoweController.DodajDoDokumentuMagazynowego();

        }
        
        public static async Task CheckLicenseButtonActionWithoutDialogAsync()
        {
           
                await Task.Factory.StartNew(() =>
                {
                    LicenseManagerController.LicencjaType licencjaType;
                    DateTime? dataWygasniecia;
                    GlobalSettingsSingletonController.Instance.IsLicense = LicenseManagerController.GetLicense(GlobalSettingsSingletonController.Instance.NumerLicencji, out licencjaType, out dataWygasniecia);
                    GlobalSettingsSingletonController.Instance.LicenseType = licencjaType;
                    GlobalSettingsSingletonController.Instance.DataWygasnieciaLicencji = dataWygasniecia?.ToLongDateString() ?? "-";
                });
           
        }
        
        public static async Task ShowMessagebox(string message)
        {
            if (GlobalSettingsSingletonController.Instance.IsTaskOpen)
                MainWindowViewModel.ClosingTask();

            var sampleMessageDialog = new MessageBoxDialog
            {
                Message =
                {
                    Text = message,
                    MinWidth = 150
                }
            };

            await DialogHost.Show(sampleMessageDialog, "RootDialog");
        }

        public static void SetLicenseInit()
        {
            DateTime? dataWygasnieciaLicencji;
            LicenseManagerController.LicencjaType tempLicenseType;

            GlobalSettingsSingletonController.Instance.IsLicense =  LicenseManagerController.GetLicense(GlobalSettingsSingletonController.Instance.NumerLicencji, out tempLicenseType, out dataWygasnieciaLicencji);
            GlobalSettingsSingletonController.Instance.LicenseType = tempLicenseType;
            GlobalSettingsSingletonController.Instance.DataWygasnieciaLicencji = dataWygasnieciaLicencji?.ToLongDateString() ?? "-";
        }

       
    }
}
