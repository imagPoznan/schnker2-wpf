﻿using System;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.Models.Repository;

namespace Schenker2.Controllers
{
    public class MiejsceDostawyController
    {
        public static AdresModel GetAdresByIdDok(decimal idDokumentu,decimal idKontrahenta)
        {
            try
            {
                switch (GlobalSettingsSingletonController.Instance.SelectedDaneOdbiorcy)
                {
                    case FieldsFromEnum.WzMiejsceDostawy:
                    {
                        var repo = new MiejsceDostawyRepository(new DataBaseContext());
                        return repo.GetAdresByIdDokMag(idDokumentu);
                    }
                    case FieldsFromEnum.Kontrahent:
                    {
                        var repo = new KontrahentRepository(new DataBaseContext());
                        return repo.GetAdresById(idKontrahenta);
                        }
                        case FieldsFromEnum.MiejsceDostawyZamowienie:
                    {
                        var repo = new MiejsceDostawyRepository(new DataBaseContext());
                        return repo.GetAdresByIdZam(idDokumentu);
                    }
                    case FieldsFromEnum.None:
                        return new AdresModel();
                    default:
                        throw new Exception("Nie wybrano adresu odbiorcy");
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                throw new Exception(ex.Message);
            }
        }

     
    }
}
