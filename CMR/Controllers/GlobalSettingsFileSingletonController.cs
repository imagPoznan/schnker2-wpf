﻿using System;
using Schenker2.Extensions;
using Schenker2.Models.Models;

namespace Schenker2.Controllers
{
    [Serializable]
    public class GlobalSettingsFileSingletonController
    {
        public static GlobalSettingsFileSingletonController Instance { get; set; } = GetSetting();

        private string _auth = "SQL";
        private string _login = "sa";
        private string _password = "Wapro3000";
        private string _dbServerName = @"LAPDB1\SQLEXPRESS2014";
        private string _dbName = "WAPRO_DEMO";
        private string _drukarkaEtykieta;
        private bool _isActiveTaskScheduler;
        private string _numerLicencji = "";

        private bool _printActiveEtykieta;
        private int _interval = 1;

        private FieldsFromEnum _selectedDaneOdbiorcy;
        private decimal _selectedDaneNadawcy = 0;

        private string _cmrRaportPath;
        private string _firmaNadawca;
        private string _imieNazwiskoNadawca;
        private string _ulicaNadawca;
        private string _kodPocztowyNadawca;
        private string _miejscowoscNadawca;
        private string _krajNadawca;
        private string _sciezkaZapisuEtykiet;
        private bool _isZamowienie;
        private bool _isDokumentHandlowy;
        private bool _isdokumentMagazynowy;
        private string _nipNadawca;
        private string _mailNadawca;
        private string _telefonNadawca;
        private bool _isDeklarowanaWartosc;
        private bool _isPobranie;
        private bool _isParcel;
        private bool _isSystem;
        private string _loginSchenker;
        private string _hasloSchenker;
        private string _nrKlientaSchenker;
        private bool _isParcelPremium;
        private bool _isSystemPremium;
        private DateTime _pickupFromDate;
        private DateTime _pickupToDate;
        private string _uwagi;
        private FieldsFromEnum _selectedMiejsceZapisuNumerListu;
        private bool _zapiszNumerListuPrzewozowego;
        private string _drukarkaLp;
        private bool _printActiveLp;
        private DateTime _generateFromDate;
        private DateTime _generateToDate;

        public string LoginSchenker
        {
            get { return _loginSchenker; }

            set
            {
                _loginSchenker = value;
                Serialize();
            }
        }
        public string HasloSchenker
        {
            get { return _hasloSchenker; }

            set
            {
                _hasloSchenker = value;
                Serialize();
            }
        }
        public string NrKlientaSchenker
        {
            get { return _nrKlientaSchenker; }

            set
            {
                _nrKlientaSchenker = value;
                Serialize();
            }
        }
        public bool IsParcel
        {
            get { return _isParcel; }

            set
            {
                _isParcel = value;
                //Serialize();
            }
        }
        public bool IsSystem
        {
            get { return _isSystem; }

            set
            {
                _isSystem = value;
                Serialize();
            }
        }
        public bool IsDeklarowanaWartosc
        {
            get { return _isDeklarowanaWartosc; }

            set
            {
                _isDeklarowanaWartosc = value;
                Serialize();
            }
        }
        public bool IsPobranie
        {
            get { return _isPobranie; }

            set
            {
                _isPobranie = value;
                Serialize();
            }
        }
        public bool IsZamowienie
        {
            get { return _isZamowienie; }

            set
            {
                _isZamowienie = value;
                Serialize();
            }
        }

        public bool IsDokumentHandlowy
        {
            get { return _isDokumentHandlowy; }

            set
            {
                _isDokumentHandlowy = value;
                Serialize();
            }
        }
        public bool IsDoumentMagazynowy
        {
            get { return _isdokumentMagazynowy; }

            set
            {
                _isdokumentMagazynowy = value;
                Serialize();
            }
        }

        public string FirmaNadawca
        {
            get { return _firmaNadawca; }
            set
            {
                _firmaNadawca = value;
                Serialize();
            }
        }

        public string SciezkaZapisuEtykiet
        {
            get { return _sciezkaZapisuEtykiet; }
            set
            {
                _sciezkaZapisuEtykiet = value;
                Serialize();
                
            }
        }

        public string ImieNazwiskoNadawca
        {
            get { return _imieNazwiskoNadawca; }
            set
            {
                _imieNazwiskoNadawca = value;
                Serialize();
            }
        }
        public string UlicaNadawca
        {
            get { return _ulicaNadawca; }
            set
            {
                _ulicaNadawca = value;
                Serialize();
            }
        }
        public string TelefonNadawca
        {
            get { return _telefonNadawca; }
            set
            {
                _telefonNadawca = value;
                Serialize();
            }
        }
        public string MailNadawca
        {
            get { return _mailNadawca; }
            set
            {
                _mailNadawca = value;
                Serialize();
            }
        }
        public string NipNadawca
        {
            get { return _nipNadawca; }
            set
            {
                _nipNadawca = value;
                Serialize();
            }
        }
        public string KodPocztowyNadawca
        {
            get { return _kodPocztowyNadawca; }
            set
            {
                _kodPocztowyNadawca = value;
                Serialize();
            }
        }
        public string MiejscowoscNadawca
        {
            get { return _miejscowoscNadawca; }
            set
            {
                _miejscowoscNadawca = value;
                Serialize();
            }
        }
        public string KrajNadawca
        {
            get { return _krajNadawca; }
            set
            {
                _krajNadawca = value;
                Serialize();
            }
        }


        public string Auth
        {
            get { return _auth; }
            set
            {
                _auth = value;
                Serialize();
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                Serialize();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                Serialize();
            }
        }
        public string CmrRaportPath
        {
            get { return _cmrRaportPath; }
            set
            {
                _cmrRaportPath = value;
                Serialize();

            }
        }
        public FieldsFromEnum SelectedDaneOdbiorcy
        {
            get { return _selectedDaneOdbiorcy;}
            set
            {
                _selectedDaneOdbiorcy = value;
                Serialize();
            }
        }

        public decimal SelectedDaneNadawcy 
        {
            get { return _selectedDaneNadawcy; }
            set
            {
                _selectedDaneNadawcy = value;
                Serialize();
            }
        }
       
   

        public string ServerName
        {
            get { return _dbServerName; }
            set
            {
                _dbServerName = value;
                Serialize();
            }
        }

        public string DbName
        {
            get { return _dbName; }
            set
            {
                _dbName = value;
                Serialize();
            }
        }
      


        public string DrukarkaEtykieta
        {
            get { return _drukarkaEtykieta; }
            set
            {
                _drukarkaEtykieta = value;
                Serialize();
            }
        }
        public string DrukarkaLp
        {
            get { return _drukarkaLp; }
            set
            {
                _drukarkaLp = value;
                Serialize();
            }
        }

        public bool IsActiveTaskScheduler
        {
            get { return _isActiveTaskScheduler; }
            set
            {
                _isActiveTaskScheduler = value;
                Serialize();
            }
        }

      

        public bool PrintActiveEtykieta
        {
            get { return _printActiveEtykieta; }
            set
            {
                _printActiveEtykieta = value;
                Serialize();
            }
        }
        public bool PrintActiveLp
        {
            get { return _printActiveLp; }
            set
            {
                _printActiveLp = value;
                Serialize();
            }
        }
        public bool ZapiszNumerListuPrzewozowego
        {
            get { return _zapiszNumerListuPrzewozowego; }
            set
            {
                _zapiszNumerListuPrzewozowego = value;
                Serialize();
            }
        }

        public int CrystalReportPriority { get; set; } = 1; //TODO

        public int SpedytorPriority { get; set; } = 2; //TODO

        public int Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                Serialize();
            }
        }

        public string NumerLicencji
        {
            get { return _numerLicencji; }
            set
            {
                _numerLicencji = value;
                Serialize();
            }
        }
        public DateTime PickupFromDate
        {
            get { return _pickupFromDate; }
            set
            {
                _pickupFromDate = value;
                Serialize();
            }
        }
        public DateTime PickupToDate
        {
            get { return _pickupToDate; }
            set
            {
                _pickupToDate = value;
                Serialize();
            }
        }
        public DateTime GenerateFromDate
        {
            get { return _generateFromDate; }
            set
            {
                _generateFromDate = value;
                Serialize();
            }
        }
        public DateTime GenerateToDate
        {
            get { return _generateToDate; }
            set
            {
                _generateToDate = value;
                Serialize();
            }
        }
        public string Uwagi
        {
            get { return _uwagi; }
            set
            {
                _uwagi = value;
                Serialize();
            }
        }

        public bool IsParcelPremium
        {
            get { return _isParcelPremium; }
            set
            {
                _isParcelPremium = value;
                Serialize();
            }
        }
        public bool IsSystemPremium
        {
            get { return _isSystemPremium; }
            set
            {
                _isSystemPremium = value;
                Serialize();
            }
        }
        public FieldsFromEnum SelectedMiejsceZapisuNumerListu
        {
            get { return _selectedMiejsceZapisuNumerListu; }
            set
            {
                _selectedMiejsceZapisuNumerListu = value;
                Serialize();
            }
        }


        private GlobalSettingsFileSingletonController() { } //Singleton

        public static GlobalSettingsFileSingletonController GetSetting()
        {

            try
            {
                var currentSettings = Deserialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin");

                if (currentSettings != null)
                    return currentSettings;
                else
                {
                    throw new Exception("Nie wczytana ustawień");
                }
                Serialize();
                return new GlobalSettingsFileSingletonController();
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new GlobalSettingsFileSingletonController();
            }
        }

        public static bool SetDatabaseData(GlobalSettingsFileSingletonController obj)
        {
            Serialize(obj);
            return true;
        }

        private static void Serialize(GlobalSettingsFileSingletonController obj)
        {
            BinarySerializer<GlobalSettingsFileSingletonController>.Serialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin", obj);
        }

        private static void Serialize()
        {
            BinarySerializer<GlobalSettingsFileSingletonController>.Serialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin", Instance);
        }

        private static GlobalSettingsFileSingletonController Deserialize(string path)
        {
            return BinarySerializer<GlobalSettingsFileSingletonController>.Deserialize(path);
        }
    }
}
