﻿using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.schenkerProd;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;

namespace Schenker2.Controllers
{
    public class GenerujController
    {
        List<Adr> adrList = new List<Adr>();
        //static TransportOrdersService service = new TransportOrdersService();
        List<Service> edokumenty = new List<Service>();
        private SchenkerApiService _schenkerApiService;

        public GenerujController()
        {
            _schenkerApiService = new SchenkerApiService();
        }
        public static GenerujController Create()
        {
            return new GenerujController();
        }
        public bool GenerujPrzesylke(DaneOdbiorcyModel odbiorca)
        {
            if (odbiorca?.FormaPlatnosci == "gotówka")
            {
                throw new Exception("Forma płatności gotówka zmień na pobranie!");

            }
            if (GlobalSettingsSingletonController.Instance.KwotaPobrania.ConvertToInt() > 10000)
            {
                throw new Exception("Kwota pobrania za wysoka!");
            }
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            _schenkerApiService._service.UserAgent = ".NET APP";
            _schenkerApiService._service.Credentials = new NetworkCredential(GlobalSettingsSingletonController.Instance.LoginSchenker, GlobalSettingsSingletonController.Instance.HasloSchenker);


            var product = "";
            if (GlobalSettingsSingletonController.Instance.IsSystem && !GlobalSettingsSingletonController.Instance.IsPremium)
                product = "SYSTEM";
            if (GlobalSettingsSingletonController.Instance.IsSystem && GlobalSettingsSingletonController.Instance.IsPremium)
                product = "PREMIUM_SYSTEM";
            if (GlobalSettingsSingletonController.Instance.IsParcel && !GlobalSettingsSingletonController.Instance.IsPremium)
                product = "PARCEL";
            if (GlobalSettingsSingletonController.Instance.IsParcel && GlobalSettingsSingletonController.Instance.IsPremium)
                product = "PREMIUM_PARCEL";


            var sender = new Party()
            {
                clientId = GlobalSettingsSingletonController.Instance.NrKlientaSchenker,
                city = GlobalSettingsSingletonController.Instance.MiejscowoscNadawca,
                contactPerson = GlobalSettingsSingletonController.Instance?.ImieNazwiskoNadawca?.Trim(),
                country = GlobalSettingsSingletonController.Instance.KrajNadawca?.ToUpper(),
                email = GlobalSettingsSingletonController.Instance.MailNadawca,
                name1 = GlobalSettingsSingletonController.Instance.NadawcaFirma,
                postCode = GlobalSettingsSingletonController.Instance.KodPocztowyNadawca.Replace("-", "").Trim(),
                nip = GlobalSettingsSingletonController.Instance.NipNadawca,
                street = GlobalSettingsSingletonController.Instance.UlicaNadawca,
                phone = GlobalSettingsSingletonController.Instance.TelefonNadawca
            };
            var receiver = new Party()
            {
                city = odbiorca.AdresOdbiorcy.Miejscowosc?.Trim(), // max 35
                contactPerson = odbiorca.AdresOdbiorcy.ImieINazwisko?.Trim(), //max60
                country = odbiorca.AdresOdbiorcy.Kraj?.Trim()?.ToUpper(),
                email = odbiorca.AdresOdbiorcy.Email?.Trim(), //max 60
                name1 = odbiorca.AdresOdbiorcy.Nazwa?.Trim(),//max 60
                postCode = odbiorca.AdresOdbiorcy?.KodPocztowy.Replace("-", "").Trim(),
                street = odbiorca.AdresOdbiorcy?.Ulica?.Trim(), // max 60
                phone = odbiorca.AdresOdbiorcy?.Telefon?.Trim() //max 35

            };
            if (string.IsNullOrWhiteSpace(receiver.contactPerson))
            {
                receiver.contactPerson = receiver.name1;
            }
            if (string.IsNullOrWhiteSpace(receiver.name1))
            {
                receiver.name1 = receiver.contactPerson;
            }

            var pickupFrom = new DateTime(GlobalSettingsSingletonController.Instance.PickupFromDate.Year, GlobalSettingsSingletonController.Instance.PickupFromDate.Month, GlobalSettingsSingletonController.Instance.PickupFromDate.Day, GlobalSettingsSingletonController.Instance.PickUpFromTime.Hour, GlobalSettingsSingletonController.Instance.PickUpFromTime.Minute, 0);
            var pickupTo = new DateTime(GlobalSettingsSingletonController.Instance.PickupToDate.Year, GlobalSettingsSingletonController.Instance.PickupToDate.Month, GlobalSettingsSingletonController.Instance.PickupToDate.Day, GlobalSettingsSingletonController.Instance.PickUpToTime.Hour, GlobalSettingsSingletonController.Instance.PickUpToTime.Minute, 0);

            var deliveryFrom = new DateTime(GlobalSettingsSingletonController.Instance.DeliveryFromDate.Year, GlobalSettingsSingletonController.Instance.DeliveryFromDate.Month, GlobalSettingsSingletonController.Instance.DeliveryFromDate.Day, GlobalSettingsSingletonController.Instance.DeliveryFromTime.Hour, GlobalSettingsSingletonController.Instance.DeliveryFromTime.Minute, 0);
            var deliveryTo = new DateTime(GlobalSettingsSingletonController.Instance.DeliveryToDate.Year, GlobalSettingsSingletonController.Instance.DeliveryToDate.Month, GlobalSettingsSingletonController.Instance.DeliveryToDate.Day, GlobalSettingsSingletonController.Instance.DeliveryToTime.Hour, GlobalSettingsSingletonController.Instance.DeliveryToTime.Minute, 0);


            var packages = new List<Colli>();
            //var coli = new Colli[GlobalSettingsSingletonController.Instance.DanePrzesylki.Count];

            // paczki element pkg powinien powtarzać się dla każdego opakowania
            if (GlobalSettingsSingletonController.Instance.IsParcel)
            {
                var pack = new Colli()
                {
                    name = "art",
                    packCode = "PC",
                    quantity = GlobalSettingsSingletonController.Instance.ParcelLiczbaPaczek,
                    protection = "Folia bąbelkowa",
                    weight = GlobalSettingsSingletonController.Instance.ParcelWagaLacznie * 100,


                };
                packages.Add(pack);
            }
            // palety element; pkg powinien powtarzać
            //się dla każdego opakowania homogenicznego(taka sama wysokość, szerokość, długość, waga
            //oraz typ nośnika).W przypadku jednego typu opakowania homogenicznego należy podać
            //jeden element colli, jego wymiary oraz liczbę opakowań danego typu, sumę ich wag i objętości.
            //W przypadku przesyłek mieszanych(palety i np.kartony – niespaletyzowane) należy podać
            //jeden element Colli dla każdego typu opakowania – jeden grupujący palety i zawierający sumy
            //wag, i objętości pale t, drugi odpowiednie sumy dla kartonów i przykładowo trzeci – dla
            //hoboków
            if (GlobalSettingsSingletonController.Instance.IsSystem)
            {
                foreach (var paczka in GlobalSettingsSingletonController.Instance.DanePrzesylki)
                {
                    Logs.AddLogInfo(paczka.WybranaNazwaTowaru +"/////"+ paczka.WybranyTypOpakowania+"///////"+ paczka.WybranaNazwaOpakowania);
                    var pack = new Colli()
                    {
                        name = paczka.WybranyTypOpakowania,//paczka.WybranaNazwaTowaru,//paczka.WybranaNazwaOpakowania,//
                        packCode = paczka.SymbolPaczki,
                        quantity = paczka.IloscOpakowan,
                        protection = paczka.TypZabezpieczenia,
                        weight = paczka.Waga.ConvertToDecimal() * 100,
                        height = paczka.Wysokosc.ConvertToDecimal(),
                        length = paczka.Dlugosc.ConvertToDecimal(),
                        width = paczka.Szerokosc.ConvertToDecimal(),

                    };
                    if (pack.weight < 1)
                    {
                        throw new Exception($@"Wprowadź poprawną wagę paczki");
                    }
                    packages.Add(pack);
                }
            }

            var sscc = new Sscc[0];
            var adrs = new Adr[0];
            var services = _schenkerApiService.PrepareServices();
            var references = _schenkerApiService.PrepareReferences();

            bool deliveryToSpecified = false;
            bool deliveryFromSpecified = false;
            //Dodatkowe parametry

            //if (GlobalSettingsSingletonController.Instance.DostawaDo10 || GlobalSettingsSingletonController.Instance.DostawaDo13 || GlobalSettingsSingletonController.Instance.DostawaNaDanyDzien)
            //{
                deliveryToSpecified = true;
                deliveryFromSpecified = true;

            // }


            


            string orderId = "";
            var keyValuePair = new KeyValuePair[0];

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //var serxml = new System.Xml.Serialization.XmlSerializer(request.GetType());
                //var ms = new MemoryStream();
                //serxml.Serialize(ms, request);
                //string xml = Encoding.UTF8.GetString(ms.ToArray());

                //var req = _schenkerApiService._service.crea

              

                var response = _schenkerApiService._service.createOrder(GlobalSettingsSingletonController.Instance.NrKlientaSchenker, installId:"",
                DataOrigin.WS21, dataOriginSpecified:false, waybillNo:null, product, pickupFrom, pickupTo,
                deliveryFrom, deliveryFromSpecified, deliveryTo, deliveryToSpecified, GlobalSettingsSingletonController.Instance.Uwagi, GlobalSettingsSingletonController.Instance.InstrukcjaDostarczenia, sender, receiver, sender, packages.ToArray(),
                false, false, ref sscc, null, services, references, out orderId, out keyValuePair);
                Logs.AddLogInfo($@"Status odpowiedzi: {response}");
                if (response != orderResponseStatusCode.OK)
                {
                    throw new Exception($@"Nie wygenerowano przesyłki - wystąpił błąd");
                }
                GlobalSettingsSingletonController.Instance.NumerPrzesylki = orderId;

                string sciezkaEtykieta = "", sciezkaLp = "";
                _schenkerApiService.GetDocument(orderId, ref sciezkaEtykieta, ref sciezkaLp);

                if (!string.IsNullOrWhiteSpace(sciezkaEtykieta) && GlobalSettingsSingletonController.Instance.DrukujEtykiete)
                {
                    string drukarkaTemp = "";
                    if (GlobalSettingsSingletonController.Instance.StartAppType == ModelViews.StartAppType.Hidden)
                    {
                        drukarkaTemp = UstawDrukarkeNaPodstawieWms();
                    }
                    
                    var drukarka = string.IsNullOrWhiteSpace(drukarkaTemp) ? GlobalSettingsSingletonController.Instance.DrukarkaEtykieta : drukarkaTemp;
                    var statusWydruku = PrintingController.PrintPdf(drukarka, "A4", sciezkaEtykieta, 1);
                    Logs.AddLogInfo($@"Status wydruku etykiety na drukarce: {GlobalSettingsSingletonController.Instance.DrukarkaEtykieta} - {statusWydruku}");
                }

                if (!string.IsNullOrWhiteSpace(sciezkaLp) && GlobalSettingsSingletonController.Instance.DrukujLp)
                {
                    var statusWydruku = PrintingController.PrintPdf(GlobalSettingsSingletonController.Instance.DrukarkaLp, "A4", sciezkaLp, 1);
                    Logs.AddLogInfo($@"Status wydruku LP na drukarce: {GlobalSettingsSingletonController.Instance.DrukarkaEtykieta} - {statusWydruku}");
                }

                //AW narzędzia
                ZapiszNazweSpedytora();

                if (GlobalSettingsSingletonController.Instance.ZapiszNumerListuPrzewozowego)
                    ZapiszNumerListu();


                var lp = GlobalSettingsSingletonController.Instance.IsSystem ? true : false;
                var iloscPaczekSystem = 0;
                foreach (var item in GlobalSettingsSingletonController.Instance.DanePrzesylki)
                {
                    iloscPaczekSystem += item.IloscOpakowan;
                }
                var iloscPaczek = GlobalSettingsSingletonController.Instance.IsParcel ? GlobalSettingsSingletonController.Instance.ParcelLiczbaPaczek : iloscPaczekSystem;
                var imieNazwisko = !string.IsNullOrWhiteSpace(odbiorca.AdresOdbiorcy.ImieINazwisko) ? odbiorca.AdresOdbiorcy.ImieINazwisko : odbiorca.AdresOdbiorcy.Nazwa;
                var wpisDoBazy = new ImagSchenker()
                {
                    //Id = GlobalSettingsSingletonController.Instance.IdZam,
                    IdDok = GlobalSettingsSingletonController.Instance.IdZam.ToString(),
                    OrderId = orderId,
                    Statusf = "",
                    LiczbaPaczek = iloscPaczek,
                    NrProtokolu = "",
                    Kontrahent = receiver.name1,
                    Adres = receiver.street + "; " + receiver.postCode + "; " + receiver.city,
                    DataListu = DateTime.Now,
                    Import = false,
                    Telefon = receiver?.phone??"",
                    Lp = lp,
                    V2 = true,
                    Typ = "202",
                    Miejscowosc = odbiorca.AdresOdbiorcy.Miejscowosc,
                    ImieNazwisko = imieNazwisko
                };
                ImagSchenker.Instance().Insert(wpisDoBazy);
                
                return true;
            }
            catch (Exception ex)
            {
                if (ex is SoapException)
                {
                    SoapException se = ex as SoapException;
                    if (se.Detail != null)
                    {
                        Logs.AddLogError($@"Nie wygenerowano przesyłki - błąd {se.Detail.InnerText}");
                        throw new Exception($@"Nie wygenerowano przesyłki - błąd {se.Detail.InnerText}");
                    }
                }
                Logs.AddLogError($@"Nie wygenerowano przesyłki - błąd {ex}");
                throw new Exception($@"Nie wygenerowano przesyłki - błąd {ex}");
            }
        }
        private string UstawDrukarkeNaPodstawieWms()
        {
            var zap = $@"SELECT * FROM [StudioSystem2017_custom].[dbo].[aw_sf_print_lp] ( '{GlobalSettingsSingletonController.Instance.IdZam}')";
            var resp = DataBaseController.Instance.RunQueryWithoutEntityString(zap);
            if (resp.ToLower() == "null")
                resp = "";
            Logs.AddLogInfo($@"Drukarka WMS: {resp} dla id_zamowienia: {GlobalSettingsSingletonController.Instance.IdZam}");
           return resp;
        }
        private void ZapiszNazweSpedytora()
        {
            var zap = $@"if ((select pole6 from zamowienie where ID_ZAMOWIENIA = {GlobalSettingsSingletonController.Instance.IdZam}) not like '%SCH%')
update zamowienie set pole6 = 'SCH' where ID_ZAMOWIENIA = {GlobalSettingsSingletonController.Instance.IdZam}";
            DataBaseController.Instance.RunQueryWithoutEntity(zap);
        }

        private void ZapiszNumerListu()
        {
            if (GlobalSettingsSingletonController.Instance.IdZam > 0)
                ZapiszNumerListuZamowienie();
            if (GlobalSettingsSingletonController.Instance.IdDokWz > 0)
                ZapiszNumerListuDokumentMagazynowy();
            if (GlobalSettingsSingletonController.Instance.IdDokHan > 0)
                ZapiszNumerListuDokumentHandlowy();
        }

        private void ZapiszNumerListuDokumentHandlowy()
        {
            throw new NotImplementedException();
        }

        private void ZapiszNumerListuDokumentMagazynowy()
        {
            throw new NotImplementedException();
        }

        private void ZapiszNumerListuZamowienie()
        {
            try
            {
                using (var db = new DataBaseContext())
                {
                    var zamowienie = db.Zamowienie.FirstOrDefault(o => o.IdZamowienia == GlobalSettingsSingletonController.Instance.IdZam);
                    if (zamowienie == null)
                    {
                        Logs.AddLogError($@"Nie zapisano numeru listu na zamówieniu - nie pobrano zamówienia od id = {GlobalSettingsSingletonController.Instance.IdZam}");
                        return;
                    }
                    zamowienie.NumerPrzesylki = GlobalSettingsSingletonController.Instance.NumerPrzesylki;

                    switch (GlobalSettingsSingletonController.Instance.SelectedMiejsceZapisuNumerListu)
                    {

                        case FieldsFromEnum.Pole1:
                            zamowienie.Pole1 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole2:
                            zamowienie.Pole2 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole3:
                            zamowienie.Pole3 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole4:
                            zamowienie.Pole4 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole5:
                            zamowienie.Pole5 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole6:
                            zamowienie.Pole6 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole7:
                            zamowienie.Pole7 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole8:
                            zamowienie.Pole8 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole9:
                            zamowienie.Pole9 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        case FieldsFromEnum.Pole10:
                            zamowienie.Pole10 = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                            break;
                        //case FieldsFromEnum.KolumnaNumerListuZamowienie:
                        //    zamowienie.NumerPrzesylki = GlobalSettingsSingletonController.Instance.NumerPrzesylki;
                        //    break;

                        default: break;
                    }
                    db.SaveChanges();
                }
              
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"ZapiszNumerListuZamowienie() błąd: {ex}");
                return;
            }
        }
    }
}