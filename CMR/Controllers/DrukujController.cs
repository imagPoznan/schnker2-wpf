﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schenker2.Extensions;
using PdfiumViewer;

namespace Schenker2.Controllers
{
    static class PrintingController
    {
        public static bool PrintPdf(string printer,string paperName,string filename,int copies)
        {
            try
            {
                var printerSettings = new PrinterSettings
                {
                    PrinterName = printer,
                    Copies = (short) copies,
                    Duplex = Duplex.Simplex
                };
                
                var pageSettings = new PageSettings(printerSettings)
                {
                    Margins = new Margins(0, 0, 0, 0)
                };

                foreach (PaperSize paperSize in printerSettings.PaperSizes)
                {
                    if (paperSize.PaperName != paperName)
                        continue;

                    pageSettings.PaperSize = paperSize;
                    break;
                }
                
                using (var document = PdfDocument.Load(filename))
                {
                    using (var printDocument = document.CreatePrintDocument())
                    {
                        printDocument.PrinterSettings = printerSettings;
                        printDocument.DefaultPageSettings = pageSettings;
                        printDocument.PrintController = new StandardPrintController();
                        printDocument.Print();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }
    }
}
