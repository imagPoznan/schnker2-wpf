﻿using System;
using Schenker2.DbModels;
using Schenker2.Extensions;
using Schenker2.Models.Models;
using Schenker2.Models.Repository;

namespace Schenker2.Controllers
{
    public class KontrahentController
    {
        public static AdresModel GetById(decimal id)
        {
            try
            {
                var repo = new KontrahentRepository(new DataBaseContext());
                return repo.GetAdresById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new AdresModel();
            }
        }
    }
}
