﻿namespace Schenker2.Models.Models
{
    public class AdresModel
    {
        public decimal IdAdresuFirmy { get; set; }
        public decimal IdFirmy { get; set; }
        public string Nazwa { get; set; }
        public string ImieINazwisko { get; set; }
        public string Ulica { get; set; }
        public string KodPocztowy { get; set; }
        public string Miejscowosc { get; set; }
        public string Kraj { get; set; }
        public string KrajEn { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string NrRejestracyjny { get; set; }
        //public string Firma { get; set; }
    }
}