﻿namespace Schenker2.Models.Models
{
    public class QueryModel
    {

        private static string CreateStringAlterTable(string tableName, string field, string properties)
        {
            return
                $@" --dodajemy {field}
                           IF NOT EXISTS (
                             SELECT *
                             FROM   sys.columns
                             WHERE  object_id = OBJECT_ID(N'{tableName}') AND name = '{field}'
                            )
                            BEGIN
                                    ALTER TABLE {tableName}
                                    ADD {field} {properties}
                            END ";
        }

        public static string Sprawdz()
        {
            var query = @"Select top 1 * from zamowienie";
            return query;
        }
        internal static string TypyOpakowan()
        {
            var query = $@"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_IMAG_SCHENKER_TYPY_OPAKOWAN'))
                BEGIN
CREATE TABLE [dbo].[_IMAG_SCHENKER_TYPY_OPAKOWAN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SymbolPaczki] [varchar](50) NOT NULL,
	[Wysokosc] [decimal](18, 0) NOT NULL,
	[Szerokosc] [decimal](18, 0) NOT NULL,
	[Dlugosc] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK__IMAG_SCHENKER_TYPY_OPAKOWAN_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[_IMAG_SCHENKER_TYPY_OPAKOWAN] ADD  CONSTRAINT [DF__IMAG_SCHENKER_TYPY_OPAKOWAN_SymbolPaczki]  DEFAULT ((0)) FOR [SymbolPaczki]


ALTER TABLE [dbo].[_IMAG_SCHENKER_TYPY_OPAKOWAN] ADD  CONSTRAINT [DF__IMAG_SCHENKER_TYPY_OPAKOWAN_Wysokosc]  DEFAULT ((0)) FOR [Wysokosc]


ALTER TABLE [dbo].[_IMAG_SCHENKER_TYPY_OPAKOWAN] ADD  CONSTRAINT [DF__IMAG_SCHENKER_TYPY_OPAKOWAN_Szerokosc]  DEFAULT ((0)) FOR [Szerokosc]


ALTER TABLE [dbo].[_IMAG_SCHENKER_TYPY_OPAKOWAN] ADD  CONSTRAINT [DF__IMAG_SCHENKER_TYPY_OPAKOWAN_Dlugosc]  DEFAULT ((0)) FOR [Dlugosc]

end";
            query += CreateStringAlterTable("_IMAG_SCHENKER_TYPY_OPAKOWAN", "NazwaOpakowania", "[varchar](200) NOT NULL");
            return query;
        }
        internal static string Create()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_IMAG_SCHENKER'))
                BEGIN
	                create table _IMAG_SCHENKER
	                (
		                id int identity primary key,
		                id_dok varchar(max) not null,
		                typ varchar(max) not null,
		                orderId varchar(max) not null,
		                statusf varchar(max) null,
		                liczba_paczek int not null,
		                nr_protokolu varchar(max) null,
		                kontrahent varchar(max) not null,
		                adres varchar(max) not null,
		                telefon varchar (max) not null,
		                data_listu datetime not null,
		                import bit,
		                lp bit, 
	                )

                END
  IF NOT EXISTS (
                             SELECT *
                             FROM   sys.columns
                             WHERE  object_id = OBJECT_ID(N'_IMAG_SCHENKER') AND name = 'v2'
                            )
                            BEGIN
                                    ALTER TABLE _IMAG_SCHENKER
                                    ADD v2 bit null
                            END 
";
            query += CreateStringAlterTable("_IMAG_SCHENKER", "miejscowosc", "[varchar](200) NULL");
            query += CreateStringAlterTable("_IMAG_SCHENKER", "imie_nazwisko", "[varchar](200) NULL");
            return query;
        }
    }
}
