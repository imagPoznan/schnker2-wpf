﻿namespace Schenker2.Models.Models
{
    public class BoolStringClass
    {
        public string Value { get; set; }
        public bool IsChecked { get; set; }
    }
}
