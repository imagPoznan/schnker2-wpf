﻿namespace Schenker2.Models.Models
{
    public class ListaTowarowModel
    {
        public string CechyINumery { get; set; }
        public decimal IloscSztuk { get; set; }
        public string SposobOpakowania { get; set; }
        public string RodzajTowaru { get; set; }
        public string NrStatystyczny { get; set; }
        public string WagaBrutto { get; set; }
        public decimal? Objetosc { get; set; }
    }
}
