﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schenker2.Models.Models
{
    public class DanePrzesylki
    {
        public List<string> NazwyTowarow { get; set; }
        public string WybranaNazwaTowaru { get; set; }
        public List<string> TypyOpakowania { get; set; }
        public string WybranyTypOpakowania { get; set; }
        public List<string> NazwyOpakowan { get; set; }
        public string WybranaNazwaOpakowania { get; set; }
        public int IloscOpakowan { get; set; }
        public string TypZabezpieczenia { get; set; }
        public string Objetosc { get; set; }
        public string MiejscaPaletowe { get; set; }
        public string Waga { get; set; }
        public string Wysokosc { get; set; }
        public string Szerokosc { get; set; }
        public string Dlugosc { get; set; }
        public string SymbolPaczki { get; set; }

    }
    public class TypOpakowania
    {
        public string NazwaOpakowania { get; set; }
       
    }
}
