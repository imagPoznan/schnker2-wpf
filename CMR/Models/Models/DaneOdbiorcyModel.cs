﻿using Schenker2.DbModels;
using System;
using System.Collections.Generic;

namespace Schenker2.Models.Models
{
    public class DaneOdbiorcyModel
    {
        public decimal IdDokumentuMagazynowego { get; set; }
        public decimal IdZamowienia { get; set; }
        public decimal IdKontrahenta { get; set; }
        public decimal WartoscBrutto { get; set; }
        public string NumerDokumentu { get; set; }
        public AdresModel AdresNadawcy { get; set; }
        public AdresModel AdresOdbiorcy { get; set; }
        public string MiejscePrzeznaczenia { get; set; }
        public string MiejsceZaladowania { get; set; }
        //public string NumerPrzesylki { get; set; }
        //public bool Pobranie { get; set; }
        public string FormaPlatnosci { get; set; }
        public Zamowienie Zamowienie { get; set; }
        public string NumerFaktury { get; set; }
        public Kontrahent Kontrahent { get; set; }
    }
}
