﻿

using Schenker2.DbModels;
using Schenker2.Models.Models;
using System.Linq;

namespace Schenker2.Models.Repository
{
    class KontrahentRepository 
    {
        private DataBaseContext _context;
        public KontrahentRepository(DataBaseContext context)
        {
            _context = context;
        }

        public AdresModel GetAdresById(decimal id)
        {
            return _context.Kontrahent.Where(x => x.IdKontrahenta == id).Select(x => new AdresModel
            {
               Nazwa = x.NazwaPelna,
               Kraj = x.SymKraju,
               KodPocztowy = x.KodPocztowy,
               Miejscowosc = x.Miejscowosc,
               Ulica = x.UlicaLokal,
               KrajEn = x.SymKrajuKor,
               Email = x.AdresEmail,
               Telefon = x.TelefonFirmowy,
               IdFirmy = x.IdFirmy ?? 0
               
            }).FirstOrDefault();
        }
        public Kontrahent GetKontrahentById(decimal idKontrahenta)
        {
            return _context.Kontrahent.FirstOrDefault(x => x.IdKontrahenta == idKontrahenta);
        }
    }
}
