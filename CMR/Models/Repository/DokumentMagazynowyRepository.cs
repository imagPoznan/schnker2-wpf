﻿using System.Linq;
using System.Windows;
using Schenker2.Controllers;
using Schenker2.DbModels;
using Schenker2.Models.Models;

namespace Schenker2.Models.Repository
{
    class DokumentMagazynowyRepository 
    {
        private DataBaseContext _context;

        public DokumentMagazynowyRepository(DataBaseContext context)
        {
            _context = context;
        }

        public DaneOdbiorcyModel GetComplexById(decimal id)
        {
         
            var currentDokMag = _context.DokumentMagazynowy.FirstOrDefault(x => x.IdDokMagazynowego == id);

            if (currentDokMag == null)
                return new DaneOdbiorcyModel();

            var complex = new DaneOdbiorcyModel
            {
                NumerDokumentu = currentDokMag.Numer,
                IdDokumentuMagazynowego = currentDokMag.IdDokMagazynowego,
                AdresOdbiorcy = MiejsceDostawyController.GetAdresByIdDok(currentDokMag.IdDokMagazynowego,currentDokMag.IdKontrahenta ?? 0),
                AdresNadawcy = FirmaController.GetById(GlobalSettingsSingletonController.Instance.SelectedDaneNadawcy),
                WartoscBrutto = currentDokMag.WartoscBrutto ?? 0
              
            };
          
            return complex;
        }
    }
}