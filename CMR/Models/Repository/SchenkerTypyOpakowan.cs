﻿using Schenker2.DbModels;
using Schenker2.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schenker2.Models.Repository
{
    public class SchenkerTypyOpakowanRepository 
    {
        private DataBaseContext _context;

        public SchenkerTypyOpakowanRepository()
        {
            _context = new DataBaseContext();
        }
        public static SchenkerTypyOpakowanRepository CreateRepo()
        {
            return new SchenkerTypyOpakowanRepository();
        }
        public void UsunTyp(ImagSchenkerTypyOpakowan wybranyTyp)
        {
            _context.ImagSchenkerTypyOpakowan.RemoveRange(wybranyTyp);
            _context.SaveChanges();
        }
        public void DodajTyp(string typ)
        {
            _context.ImagSchenkerTypyOpakowan.Add(new ImagSchenkerTypyOpakowan()
            {
                SymbolPaczki = typ,
                Dlugosc = 100,
                Szerokosc = 100,
                Wysokosc = 100
            });
            _context.SaveChanges();
        }
        public IEnumerable<ImagSchenkerTypyOpakowan> Odswiez()
        {
            try
            {
                return _context.ImagSchenkerTypyOpakowan;
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"");
                return null;
            }
            
        }
        public IEnumerable<ImagSchenkerTypyOpakowan> Aktualizuj(IEnumerable<ImagSchenkerTypyOpakowan> typuOpakowan)
        {
            try
            {
                if (typuOpakowan == null)
                {
                    return Odswiez();
                }
                _context.ImagSchenkerTypyOpakowan.UpdateRange(typuOpakowan);
                _context.SaveChanges();
                return Odswiez();
            }
            catch (Exception ex)
            {
                Logs.AddLogError($@"");
                return null;
            }
           
        }
    }
}
