﻿using System.Collections.Generic;
using System.Linq;
using Schenker2.DbModels;
using Schenker2.Models.Models;

namespace Schenker2.Models.Repository
{
    internal class PozycjeDokumentowMagazynowychRepository
    {
        private DataBaseContext _context;
        public PozycjeDokumentowMagazynowychRepository(DataBaseContext context)
        {
            _context = context;
        }

        public List<ListaTowarowModel> GetListaTowarowById(decimal id)
        {
            
            return (from pozycjaDokumentuMagazynowego in _context.PozycjaDokumentuMagazynowego
                join artykul in _context.Artykul on pozycjaDokumentuMagazynowego.IdArtykulu equals artykul.IdArtykulu
                    where pozycjaDokumentuMagazynowego.IdDokMagazynowego == id
                    select new ListaTowarowModel
                {
                    CechyINumery = artykul.Nazwa,
                    IloscSztuk = pozycjaDokumentuMagazynowego.Ilosc,
                    NrStatystyczny = pozycjaDokumentuMagazynowego.NrSerii,
                    Objetosc = artykul.WymiarG*artykul.WymiarS*artykul.WymiarW,
                    RodzajTowaru = pozycjaDokumentuMagazynowego.RodzajArtykulu,
                    SposobOpakowania ="",
                    WagaBrutto = artykul.Waga + "" + artykul.JedWagi,
                }).ToList();
        }

        public List<Artykul> GetArtykulyByIdDokMag(decimal id)
        {

            return (from pozycjaDokumentuMagazynowego in _context.PozycjaDokumentuMagazynowego
                    join artykul in _context.Artykul on pozycjaDokumentuMagazynowego.IdArtykulu equals artykul.IdArtykulu
                    where pozycjaDokumentuMagazynowego.IdDokMagazynowego == id
                    select artykul).ToList();
        }

    }
}