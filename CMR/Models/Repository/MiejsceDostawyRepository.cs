﻿using System.Linq;
using Schenker2.DbModels;
using Schenker2.Models.Models;

namespace Schenker2.Models.Repository
{
    class MiejsceDostawyRepository 
    {
        private DataBaseContext _context;
        public MiejsceDostawyRepository(DataBaseContext context)
        {
            _context = context;
        }

        public AdresModel GetAdresByIdDokMag(decimal idDokMag)
        {
            return (from dostawa in _context.Dostawa
                    join miejsceDostawy in _context.MiejsceDostawy on dostawa.IdMiejscaDostawy equals
                    miejsceDostawy.IdMiejscaDostawy
                    where dostawa.IdDokMagazynowego.Value == idDokMag
                    select new AdresModel
                    {
                        Nazwa = miejsceDostawy.Firma,
                        ImieINazwisko = miejsceDostawy.Odbiorca.Replace(" ",""),
                        Kraj = miejsceDostawy.SymKraju,
                        KodPocztowy = miejsceDostawy.KodPocztowy,
                        Ulica = miejsceDostawy.UlicaLokal,
                        Miejscowosc = miejsceDostawy.Miejscowosc,
                        Telefon = miejsceDostawy.Tel
                       
                    }).FirstOrDefault();
        }

        public AdresModel GetAdresByIdZam(decimal idZam)
        {
            return (from dostawa in _context.Dostawa
                join miejsceDostawy in _context.MiejsceDostawy on dostawa.IdMiejscaDostawy equals miejsceDostawy.IdMiejscaDostawy
                join kontrahent in _context.Kontrahent on miejsceDostawy.IdKontrahenta equals kontrahent.IdKontrahenta
                where dostawa.IdZamowienia.Value == idZam
                select new AdresModel
                {
                    Nazwa = miejsceDostawy.Firma,
                    ImieINazwisko = miejsceDostawy.Odbiorca.Replace(" ", ""),
                    Kraj = miejsceDostawy.SymKraju,
                    KodPocztowy = miejsceDostawy.KodPocztowy,
                    Ulica = miejsceDostawy.UlicaLokal,
                    Miejscowosc = miejsceDostawy.Miejscowosc,
                    Telefon = miejsceDostawy.Tel,
                    //Firma = miejsceDostawy.Firma
                    
                    
                }

            ).FirstOrDefault();
        }


        public string GetMiejscePrzeznaczeniaByIdDok(decimal idDokMag)
        {
            return (from dostawa in _context.Dostawa
                    join miejsceDostawy in _context.MiejsceDostawy on dostawa.IdMiejscaDostawy equals miejsceDostawy.IdMiejscaDostawy
                    where dostawa.IdDokMagazynowego.Value == idDokMag
                    select miejsceDostawy.Miejscowosc
                    ).FirstOrDefault();
        }
    }
}
