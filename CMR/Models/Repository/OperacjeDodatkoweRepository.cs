﻿using Schenker2.DbModels;
using System.Linq;

namespace Schenker2.Models.Repository
{
    class OperacjeDodatkoweRepository 
    {
        private DataBaseContext _context;
        public OperacjeDodatkoweRepository(DataBaseContext context)
        {
            _context = context;
        }

        public MagOperacjeDodatkowe GetByName(string name)
        {
            return _context.MagOperacjeDodatkowe.FirstOrDefault(x=>x.Nazwa == name);
        }

        public bool Add(MagOperacjeDodatkowe objectOperacja)
        {
            var operation = _context.MagOperacjeDodatkowe.FirstOrDefault(x => x.Nazwa == objectOperacja.Nazwa);
            if (operation != null)
            {
                operation.Nazwa = objectOperacja.Nazwa;
                operation.KodGrupy = objectOperacja.KodGrupy;
                operation.KodOperacji = objectOperacja.KodOperacji;
                operation.Grupowanie = objectOperacja.Grupowanie;
                operation.Parametry = objectOperacja.Parametry;
                operation.Sciezka = objectOperacja.Sciezka;
                operation.SkrotKlawiszowy = objectOperacja.SkrotKlawiszowy;
                operation.SkrotKlawiszowyKod = objectOperacja.SkrotKlawiszowyKod;
                operation.FunkcjaZewnetrzna = objectOperacja.FunkcjaZewnetrzna;
                operation.OdswiezListe = objectOperacja.OdswiezListe;
                operation.KasujZaznaczenie = objectOperacja.KasujZaznaczenie;

                _context.SaveChanges();
            }
            else
            {
                _context.MagOperacjeDodatkowe.Add(objectOperacja);
                _context.SaveChanges();
            }
            
            return true;
        }
    }
}
