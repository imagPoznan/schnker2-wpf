﻿using System;
using System.Linq;
using Schenker2.Controllers;
using Schenker2.Models.Models;
using System.Windows;
using Schenker2.DbModels;
using Schenker2.Extensions;

namespace Schenker2.Models.Repository
{
    class ZamowienieRepository 
    {
        private DataBaseContext _context;
        public ZamowienieRepository(DataBaseContext context)
        {
            _context = context;
        }

        public DaneOdbiorcyModel GetComplexById(decimal id)
        {

            var zamowienie = _context.Zamowienie.FirstOrDefault(x => x.IdZamowienia == id);
            var repoKontrahent = new KontrahentRepository(new DataBaseContext());

            if (zamowienie == null)
                return new DaneOdbiorcyModel();
            var complex = new DaneOdbiorcyModel
            {
                IdZamowienia = zamowienie.IdZamowienia,
                IdKontrahenta = zamowienie.IdKontrahenta ?? 0,
                WartoscBrutto = zamowienie.WartoscBrutto ?? 0,
                AdresOdbiorcy = MiejsceDostawyController.GetAdresByIdDok(zamowienie.IdZamowienia, zamowienie.IdKontrahenta ?? 0),
                NumerDokumentu = zamowienie.Numer,
                FormaPlatnosci = zamowienie?.FormaPlatnosci??"",
                Zamowienie = zamowienie,
                NumerFaktury = DataBaseController.Instance.PobierzNumerFakturyPoIdZamowienia(),
                Kontrahent = repoKontrahent.GetKontrahentById(zamowienie?.IdKontrahenta??0)

            };
            //if (complex.FormaPlatnosci == "gotówka")
            //{
            //    throw new Exception("Forma płatności gotówka zmień na pobranie!");

            //}
            if (string.IsNullOrWhiteSpace(complex.AdresOdbiorcy.Nazwa) && string.IsNullOrWhiteSpace(complex.AdresOdbiorcy.ImieINazwisko))
            {
                complex.AdresOdbiorcy.Nazwa = complex.Kontrahent.Nazwa;
                complex.AdresOdbiorcy.ImieINazwisko = complex.Kontrahent.Nazwa;
            }
            if (!string.IsNullOrWhiteSpace(complex.NumerFaktury))
            {
                GlobalSettingsSingletonController.Instance.SelectedReferencesIndex = 3;
                GlobalSettingsSingletonController.Instance.ReferenceValue = complex.NumerFaktury;
            }
            GlobalSettingsSingletonController.Instance.KwotaDeklarowanejWartosci = complex.WartoscBrutto.ToString();
            GlobalSettingsSingletonController.Instance.KwotaPobrania = complex.WartoscBrutto.ToString();
            GlobalSettingsSingletonController.Instance.Uwagi = complex.NumerDokumentu;
            //AW
            if (!string.IsNullOrWhiteSpace(complex.Zamowienie.Pole2))
            {
                var pole2 = _context.Zamowienie.Where(x => x.Pole2 == complex.Zamowienie.Pole2).Select(x => x.Numer).ToList();
                if (pole2.Count > 0)
                {
                    GlobalSettingsSingletonController.Instance.Uwagi = "";
                    foreach (var item in pole2)
                    {
                        GlobalSettingsSingletonController.Instance.Uwagi += item + " ; ";
                    }
                }
            }


            //System.Windows.Forms.MessageBox.Show("Test");
            if (complex.Kontrahent.Nazwa.Contains("DROPSHIPPING") && !string.IsNullOrWhiteSpace(complex.Zamowienie.Pole3) && complex.Zamowienie.Pole3 != "0")
            {
                GlobalSettingsSingletonController.Instance.KwotaDeklarowanejWartosci = complex.Zamowienie.Pole3.ConvertToDecimal().ToString();
                GlobalSettingsSingletonController.Instance.KwotaPobrania = complex.Zamowienie.Pole3.ConvertToDecimal().ToString();
                GlobalSettingsSingletonController.Instance.Pobranie = true;
                GlobalSettingsSingletonController.Instance.IsDeklarowanaWartosc = true;
            }
            //if (GlobalSettingsSingletonController.Instance.KwotaPobrania > 10000)
            //{
            //    throw new Exception("Kwota pobrania za wysoka!");
            //}
            return complex;
        }
    }
    }