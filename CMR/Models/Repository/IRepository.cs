﻿using System.Collections.Generic;

namespace Schenker2.Models.Repository
{
    public interface IRepository<out TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();

        TEntity GetById(object id);

    }
}
