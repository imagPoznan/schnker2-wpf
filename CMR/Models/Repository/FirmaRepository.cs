﻿using System.Collections.Generic;

using System.Linq;
using System.Text;
using Schenker2.DbModels;
using Schenker2.Models.Models;

namespace Schenker2.Models.Repository
{
    class FirmaRepository 
    {
        private DataBaseContext _context;
        public FirmaRepository(DataBaseContext context)
        {
            _context = context;
        }

        public AdresModel GetAdresFirmyByIdFirma(decimal idAdresFirma)
        {
             return(from firma in _context.Firma
                    join adresyFirmy in _context.AdresyFirmy on firma.IdFirmy equals adresyFirmy.IdFirmy
                    where adresyFirmy.IdAdresyFirmy == idAdresFirma
                    select new AdresModel
                    {
                        Nazwa = firma.NazwaPelna,
                        Kraj = adresyFirmy.SymKraju,
                        KodPocztowy = adresyFirmy.KodPocztowy,
                        Ulica = adresyFirmy.Ulica + " " +adresyFirmy.NrDomu + "/"+ adresyFirmy.NrLokalu,
                        Miejscowosc = adresyFirmy.Miejscowosc,
                        Telefon = adresyFirmy.Telefon,
                        Email = adresyFirmy.EMail,
                    }).FirstOrDefault();
        }

        public Dictionary<decimal, string> GetAllAdresyFirmy()
        {
             var listFirmy=(from firma in _context.Firma
                    join adresyFirmy in _context.AdresyFirmy on firma.IdFirmy equals adresyFirmy.IdFirmy
                            select new AdresModel
                    {
                        IdAdresuFirmy = adresyFirmy.IdAdresyFirmy,
                        IdFirmy = adresyFirmy.IdFirmy ?? 0,
                        Nazwa = firma.NazwaPelna,
                        Kraj = adresyFirmy.SymKraju,
                        KodPocztowy = adresyFirmy.KodPocztowy,
                        Ulica = adresyFirmy.Ulica + " " + adresyFirmy.NrDomu + "/" + adresyFirmy.NrLokalu,
                        Miejscowosc = adresyFirmy.Miejscowosc,
                        Telefon = adresyFirmy.Telefon,
                        Email = adresyFirmy.EMail,
                    }).ToList();

            var dict = new Dictionary<decimal, string>();
            foreach (var row in listFirmy)
            {
                var stringB = new StringBuilder();
                stringB.Append(row.IdFirmy + " ");
                stringB.Append(row.Nazwa + " ");
                stringB.Append(row.Ulica + " ");
                stringB.Append(row.KodPocztowy + " ");
                stringB.Append(row.Miejscowosc + " ");
                stringB.Append(row.Kraj + " ");
                stringB.Append(row.Ulica + " ");
                stringB.Append(row.Telefon + " ");
                stringB.Append(row.Email + " ");
                dict.Add(row.IdAdresuFirmy, stringB.ToString());
            }
            return dict;
        }
    }
}
