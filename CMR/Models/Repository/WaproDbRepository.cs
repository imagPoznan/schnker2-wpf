﻿using Schenker2.DbModels;
using System.Linq;

namespace Schenker2.Models.Repository
{
    internal class WaproDbRepository
    {
        private DataBaseContext _context;
        public WaproDbRepository(DataBaseContext context)
        {
            _context = context;
        }

        public string GetDbId()
        {
            try
            {
                var a = _context.Waprodb.FirstOrDefault()?.WaprodbId.ToString();
                return a;
            }
            catch (System.Exception ex)
            {

                throw;
            }
         
        
        }
    }
}
