﻿using System.Linq;
using Schenker2.Controllers;
using Schenker2.Models.Models;
using System.Windows;
using Schenker2.DbModels;

namespace Schenker2.Models.Repository
{
    class DokumentHandlowyRepository
    {
        private DataBaseContext _context;
        public DokumentHandlowyRepository(DataBaseContext context)
        {
            _context = context;
        }
        public DaneOdbiorcyModel GetComplexById(decimal id)
        {
         
            var handlowy = _context.DokumentHandlowy.FirstOrDefault(x => x.IdDokumentuHandlowego == id);

            if (handlowy == null)
                return new DaneOdbiorcyModel();
            var complex = new DaneOdbiorcyModel
            {
                IdZamowienia = handlowy.IdDokumentuHandlowego,
                IdKontrahenta = handlowy.IdKontrahenta ?? 0,
                WartoscBrutto = handlowy.WartoscBrutto ?? 0,
                AdresOdbiorcy = MiejsceDostawyController.GetAdresByIdDok(handlowy.IdDokumentuHandlowego, handlowy.IdKontrahenta ?? 0),
                NumerDokumentu = handlowy.Numer,

            };

            return complex;
        }

        public string GetMiejsceZaladowaniaByIdDok(decimal idFv)
        {
            return (from fv in _context.DokumentHandlowy
                where fv.IdDokumentuHandlowego == idFv
                select fv.Miejsce
            ).FirstOrDefault();
        }

        public DokumentHandlowy GetByIdWz(decimal idWz)
        {
            return (from wz in _context.DokumentMagazynowy
                join handlowy in _context.DokumentHandlowy on wz.IdDokumentuHandlowego equals
                handlowy.IdDokumentuHandlowego
                where wz.IdDokMagazynowego == idWz
                select handlowy
            ).FirstOrDefault();
        }
    }
}