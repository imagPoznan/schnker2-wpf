﻿using System;
using System.Linq;
using System.Windows;
using Schenker2.Models.Models;
using Schenker2.Controllers;
using Schenker2.DbModels;

namespace Schenker2.Models.Repository
{
    class GenerujRepository
    {
        private DataBaseContext _context;
        public GenerujRepository(DataBaseContext context)
        {
            _context = context;
        }
        public void ZapiszNumerListu(decimal idZam, string numerPrzesylki)
        {

            //MessageBox.Show("Test");
            try
            {
 var nrPrzes = (from zam in _context.Zamowienie
                    where zam.IdZamowienia == idZam
                    select zam).First();
                nrPrzes.NumerPrzesylki = numerPrzesylki;
            }
            catch 
            {
                var nrPrzes = (from zam in _context.Zamowienie
                               where zam.IdZamowienia == 1
                               select zam).First();
                nrPrzes.NumerPrzesylki = numerPrzesylki;

            }
            _context.SaveChanges();
        }
    }
}
