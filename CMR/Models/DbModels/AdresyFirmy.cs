﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class AdresyFirmy
    {
        public decimal IdAdresyFirmy { get; set; }
        public decimal? IdFirmy { get; set; }
        public string SymKraju { get; set; }
        public int? DataOd { get; set; }
        public string Wojewodztwo { get; set; }
        public string Powiat { get; set; }
        public string Gmina { get; set; }
        public string Miejscowosc { get; set; }
        public string KodPocztowy { get; set; }
        public string Poczta { get; set; }
        public string Ulica { get; set; }
        public string NrDomu { get; set; }
        public string NrLokalu { get; set; }
        public string Telefon { get; set; }
        public string Faks { get; set; }
        public string EMail { get; set; }
        public string Skrytka { get; set; }
        public byte? Aktywny { get; set; }
        public string TypAdresu { get; set; }
        public DateTime? KpDataOd { get; set; }
        public decimal? IdUrzeduSkarbowego { get; set; }
        public string Telex { get; set; }
        public string Kraj { get; set; }
        public byte? Zakonczenie { get; set; }
    }
}
