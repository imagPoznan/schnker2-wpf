﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class DokumentHandlowy
    {
        public decimal IdDokumentuHandlowego { get; set; }
        public decimal? IdMagazynu { get; set; }
        public string TypDokumentu { get; set; }
        public byte? DokHandlowy { get; set; }
        public decimal? IdOstKorekty { get; set; }
        public decimal? IdDokOryginalnego { get; set; }
        public decimal? IdDokKorygowanego { get; set; }
        public byte? DokKorekty { get; set; }
        public decimal? IdTypu { get; set; }
        public string Numer { get; set; }
        public int? Autonumer { get; set; }
        public string FormaPlatnosci { get; set; }
        public string Miejsce { get; set; }
        public int? DataWplywu { get; set; }
        public int? DataWystawienia { get; set; }
        public int? DataSprzedazy { get; set; }
        public decimal? IdKontrahenta { get; set; }
        public decimal? IdPlatnika { get; set; }
        public string TypPlatnika { get; set; }
        public string Odebral { get; set; }
        public string Uwagi { get; set; }
        public decimal? NarzutRabat { get; set; }
        public decimal? RodzajPlat { get; set; }
        public int? TerminPlat { get; set; }
        public string ObliczanieWgCen { get; set; }
        public decimal? Zaliczka { get; set; }
        public byte? CzyZaliczkaDlaFin { get; set; }
        public decimal? WartoscNetto { get; set; }
        public decimal? WartoscBrutto { get; set; }
        public decimal? RazemZaplacono { get; set; }
        public decimal? Pozostalo { get; set; }
        public decimal? WartoscNettoWal { get; set; }
        public decimal? WartoscBruttoWal { get; set; }
        public decimal? RazemZaplaconoWal { get; set; }
        public decimal? PozostaloWal { get; set; }
        public decimal? PrzelicznikWal { get; set; }
        public int? DataKursWal { get; set; }
        public string SymWal { get; set; }
        public int? DataOstRozliczenia { get; set; }
        public decimal IdFirmy { get; set; }
        public decimal? Semafor { get; set; }
        public byte? FlagaStanu { get; set; }
        public byte? FiskalnyOk { get; set; }
        public string NumerParagonu { get; set; }
        public decimal? SumaOdsetek { get; set; }
        public decimal? IdPracownika { get; set; }
        public byte Trybrejestracji { get; set; }
        public byte? Rozliczony { get; set; }
        public byte DokWal { get; set; }
        public decimal? IdUzytkownika { get; set; }
        public byte? DokZaksiegowany { get; set; }
        public byte? DokZablokowany { get; set; }
        public decimal? IdRachunku { get; set; }
        public byte? Mp { get; set; }
        public string Pole1 { get; set; }
        public string Pole2 { get; set; }
        public string Pole3 { get; set; }
        public string Pole4 { get; set; }
        public string Pole5 { get; set; }
        public string Pole6 { get; set; }
        public string Pole7 { get; set; }
        public string Pole8 { get; set; }
        public string Pole9 { get; set; }
        public string Pole10 { get; set; }
        public byte? GrupujPozycje { get; set; }
        public byte? TrojstronnyUe { get; set; }
        public byte? PotwierdzonyUe { get; set; }
        public decimal? IdFzalOrg { get; set; }
        public decimal? IdFzalOst { get; set; }
        public byte? PozKosztoweBazowe { get; set; }
        public decimal? IdSzabdokser { get; set; }
        public decimal PrzelicznikWalPz { get; set; }
        public int? DataKursWalPz { get; set; }
        public string Wyroznik { get; set; }
        public byte? DokWalKrajowy { get; set; }
        public byte? Wewnetrzny { get; set; }
        public byte? Marzowy { get; set; }
        public decimal? IdEtykiety { get; set; }
        public byte? Uproszczony { get; set; }
        public byte? MetodaKasowa { get; set; }
        public byte? Odwrotny { get; set; }
        public string MarzowyOpis { get; set; }
        public decimal? IdKorektyZbiorczej { get; set; }
        public int? IdBanku { get; set; }
        public string KodKreskowy { get; set; }
        public byte? DoKsiegowania { get; set; }
        public byte? ZaliczkaOdroczona { get; set; }
        public int? DataOtrzymania { get; set; }
        public string KontrahentNazwa { get; set; }
        public string PlatnikNazwa { get; set; }
        public byte? ProporcjaZalAutoNettoBrutto { get; set; }
        public decimal? IdKontrahentaJst { get; set; }
    }
}
