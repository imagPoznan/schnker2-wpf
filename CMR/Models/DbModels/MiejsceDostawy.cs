﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class MiejsceDostawy
    {
        public MiejsceDostawy()
        {
            Dostawa = new HashSet<Dostawa>();
        }

        public decimal IdMiejscaDostawy { get; set; }
        public decimal? IdKontrahenta { get; set; }
        public string SymKraju { get; set; }
        public string KodPocztowy { get; set; }
        public string Miejscowosc { get; set; }
        public string UlicaLokal { get; set; }
        public string Firma { get; set; }
        public string Odbiorca { get; set; }
        public string Tel { get; set; }
        public string Opis { get; set; }
        public int TypAdresu { get; set; }
        public decimal? IdFirmyMiejsca { get; set; }

        public virtual ICollection<Dostawa> Dostawa { get; set; }
    }
}
