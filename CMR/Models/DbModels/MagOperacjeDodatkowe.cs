﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class MagOperacjeDodatkowe
    {
        public decimal IdOperacji { get; set; }
        public string KodGrupy { get; set; }
        public string Nazwa { get; set; }
        public string SkrotKlawiszowy { get; set; }
        public int? SkrotKlawiszowyKod { get; set; }
        public string KodOperacji { get; set; }
        public int Grupowanie { get; set; }
        public byte? FunkcjaZewnetrzna { get; set; }
        public string Parametry { get; set; }
        public string Sciezka { get; set; }
        public string Info { get; set; }
        public byte? OdswiezListe { get; set; }
        public byte? KasujZaznaczenie { get; set; }
    }
}
