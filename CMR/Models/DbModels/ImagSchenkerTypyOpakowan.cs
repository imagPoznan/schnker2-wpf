﻿using System;
using System.Collections.Generic;

namespace Schenker2
{
    public partial class ImagSchenkerTypyOpakowan
    {
        public int Id { get; set; }
        public string SymbolPaczki { get;  set; }
        public string NazwaOpakowania { get; set; }
        public decimal Wysokosc { get; set; }
        public decimal Szerokosc { get; set; }
        public decimal Dlugosc { get; set; }
    }
}
