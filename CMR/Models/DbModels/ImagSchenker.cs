﻿using Microsoft.EntityFrameworkCore;
using Schenker2.Controllers;
using Schenker2.DbModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Schenker2
{
    public partial class ImagSchenker
    {
        public int Id { get; set; }
        public string IdDok { get; set; }
        public string Typ { get; set; }
        public string OrderId { get; set; }
        public string Statusf { get; set; }
        public int LiczbaPaczek { get; set; }
        public string NrProtokolu { get; set; }
        public string Kontrahent { get; set; }
        public string Adres { get; set; }
        public string Telefon { get; set; }
        public DateTime DataListu { get; set; }
        public bool? Import { get; set; }
        public bool? Lp { get; set; }
        public bool? V2 { get; set; }
        public string Miejscowosc { get; set; }
        public string ImieNazwisko { get; set; }

        public static ImagSchenker Instance()
        {
            return new ImagSchenker();
        }
        public List<ImagSchenker> GetAll()
        {
            try
            {
                using (var db = new DataBaseContext())
                {
                    return db.ImagSchenker.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ObservableCollection<ImagSchenker> GetFromToday()
        {
            try
            {
                using (var db = new DataBaseContext())
                {
                    var a = db.ImagSchenker.Where(x => x.DataListu >= DateTime.Today);
                    return ToObservableCollection(a);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ObservableCollection<ImagSchenker> GetByFilters()
        {
            try
            {
                var lista = new ObservableCollection<ImagSchenker>();
                using (var db = new DataBaseContext())
                {
                    
                    var recordsByTime = db.ImagSchenker.Where(x => x.DataListu >= GlobalSettingsSingletonController.Instance.GenerateFromDate && x.DataListu <= GlobalSettingsSingletonController.Instance.GenerateToDate.AddHours(24)).ToList();
                    if (!string.IsNullOrWhiteSpace(GlobalSettingsSingletonController.Instance.SzukaniePoNazwieKontrahentaFraza))
                    {
                        recordsByTime = recordsByTime.Where(x => x.Kontrahent.ToLower().Contains(GlobalSettingsSingletonController.Instance.SzukaniePoNazwieKontrahentaFraza.ToLower())).ToList();
                    }
                    lista =  ToObservableCollection(recordsByTime);

                    if (!string.IsNullOrWhiteSpace(GlobalSettingsSingletonController.Instance.SzukaniePoNrListu))
                    {
                        var recordByLp = db.ImagSchenker.Where(x => x.OrderId.ToLower().Contains(GlobalSettingsSingletonController.Instance.SzukaniePoNrListu.ToLower()));
                        lista = ToObservableCollection(recordByLp);
                    }
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ObservableCollection<T> ToObservableCollection<T>(IEnumerable<T> enumeration)
        {
            return new ObservableCollection<T>(enumeration);
        }
        public void Insert(ImagSchenker row)
        {
            try
            {
                using (var db = new DataBaseContext())
                {
                    db.ImagSchenker.Add(row);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
