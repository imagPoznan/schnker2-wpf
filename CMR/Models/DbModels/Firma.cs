﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class Firma
    {
        public decimal IdFirmy { get; set; }
        public string Nazwa { get; set; }
        public string NazwaPelna { get; set; }
        public byte? PlatnikVat { get; set; }
        public string Nkp { get; set; }
        public string Ekd { get; set; }
        public string StruktWlFirmy { get; set; }
        public string RodzajPlatnika { get; set; }
        public string Nip { get; set; }
        public string Regon { get; set; }
        public byte? RodzajDzialU { get; set; }
        public byte? RodzajDzialP { get; set; }
        public byte? RodzajDzialH { get; set; }
        public decimal? IdAdresuDomyslnego { get; set; }
        public decimal? IdZus { get; set; }
        public decimal? IdUs { get; set; }
        public string OrganZal { get; set; }
        public string NazwaRej { get; set; }
        public int? DataRej { get; set; }
        public string NrWRej { get; set; }
        public int? DataRozpDzial { get; set; }
        public byte? Aktywna { get; set; }
        public string Naglowek { get; set; }
        public string Uwagi { get; set; }
        public DateTime? KpDataRej { get; set; }
        public byte? KpTrybNumeracji { get; set; }
        public DateTime? KpMiesiacAktualny { get; set; }
        public DateTime? KpDataRozpDzial { get; set; }
        public byte? Fakir { get; set; }
        public byte? Kaper { get; set; }
        public byte? Mag { get; set; }
        public byte? Est2000 { get; set; }
        public byte? KpOsobaFizyczna { get; set; }
        public int? KpNumerStartowy { get; set; }
        public string LogoFirmy { get; set; }
        public byte? KpIdRodzaju { get; set; }
        public decimal? Semafor { get; set; }
        public byte? KpRca { get; set; }
        public byte? KwartalnyVat { get; set; }
        public byte? Mp { get; set; }
        public decimal? IdRachunku { get; set; }
        public bool? KpWlascicielPlatnikiemZus { get; set; }
        public bool? KpOdliczenieZusDw { get; set; }
        public int? KpAktywna { get; set; }
        public int? NrFkPulaOd { get; set; }
        public int? NrFkPulaDo { get; set; }
        public int? NrFkPulaAktualny { get; set; }
        public byte? FlagaStanu { get; set; }
        public byte? Gang { get; set; }
        public string Katalog { get; set; }
        public string Pkd { get; set; }
        public string KpKduPath { get; set; }
        public string Naglowek2 { get; set; }
        public byte? MetodaKasowa { get; set; }
        public byte[] LogoFirmyBlob { get; set; }
        public decimal? IdKontrahentaJst { get; set; }
    }
}
