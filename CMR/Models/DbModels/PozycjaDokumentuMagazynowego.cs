﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class PozycjaDokumentuMagazynowego
    {
        public decimal IdPozDokMag { get; set; }
        public decimal? IdDokMagazynowego { get; set; }
        public decimal? IdDokHandlowego { get; set; }
        public decimal? IdArtykulu { get; set; }
        public decimal? IdPozOryginalnej { get; set; }
        public decimal? IdPozKorygowanej { get; set; }
        public decimal? IdOstKorekty { get; set; }
        public decimal? IdPowKorekty { get; set; }
        public byte? PozKorekty { get; set; }
        public string KodVat { get; set; }
        public decimal Ilosc { get; set; }
        public decimal Wydano { get; set; }
        public decimal Zarezerwowano { get; set; }
        public string RodzajPozycji { get; set; }
        public byte? PozPrzychoduOk { get; set; }
        public int? Data { get; set; }
        public decimal? CenaNetto { get; set; }
        public decimal? CenaBrutto { get; set; }
        public decimal? CenaNettoWal { get; set; }
        public decimal? CenaBruttoWal { get; set; }
        public string ZnacznikCeny { get; set; }
        public string OpisCeny { get; set; }
        public string Jednostka { get; set; }
        public decimal? Przelicznik { get; set; }
        public decimal? Rabat { get; set; }
        public decimal? Rabat2 { get; set; }
        public byte? FlagaStanu { get; set; }
        public string RodzajArtykulu { get; set; }
        public decimal? OpakowaniaWydano { get; set; }
        public decimal? OpakowaniaPrzyjeto { get; set; }
        public string Opis { get; set; }
        public decimal? IdPozZam { get; set; }
        public string NrPaczki { get; set; }
        public byte Trybrejestracji { get; set; }
        public decimal? IdPozPrzych { get; set; }
        public byte PozWybDostawy { get; set; }
        public string NrSerii { get; set; }
        public Guid? GuidPozDostawyMm { get; set; }
        public Guid? GuidPozDokMag { get; set; }
        public int DataWaznosci { get; set; }
        public decimal? IdPozZlecenia { get; set; }
        public string KodVatUe { get; set; }
        public decimal? CenaNettoBezKosztu { get; set; }
        public decimal? CenaBruttoBezKosztu { get; set; }
        public byte? IPodlegaDeklaracji { get; set; }
        public string IKodCn { get; set; }
        public string IKodTransportu { get; set; }
        public string IKodTransakcji { get; set; }
        public string IKodDostawy { get; set; }
        public string IKodKrajuPrzezWys { get; set; }
        public string IKodKrajuPoch { get; set; }
        public decimal? IMasaNetto { get; set; }
        public decimal? IdPozObrotowej { get; set; }
        public decimal? IdPozZamZal { get; set; }
        public byte? AktCenPrzyDostawie { get; set; }
        public decimal? CnSprzPz { get; set; }
        public decimal? CbSprzPz { get; set; }
        public decimal? CenaNKgo { get; set; }
        public decimal? CenaBKgo { get; set; }
        public decimal? CzbMarza { get; set; }
        public byte? Akcyza { get; set; }
        public string Pole1 { get; set; }
        public string Pole2 { get; set; }
        public string Pole3 { get; set; }
        public string Pole4 { get; set; }
        public string Pole5 { get; set; }
        public string Pole6 { get; set; }
        public string Pole7 { get; set; }
        public string Pole8 { get; set; }
        public string Pole9 { get; set; }
        public string Pole10 { get; set; }
        public decimal? IdDokKorygujacegoZbiorczo { get; set; }
        public decimal? Rabat2Zb { get; set; }
        public decimal? IdPozycjiOferty { get; set; }
        public string IKodKontrahentaZagr { get; set; }
    }
}
