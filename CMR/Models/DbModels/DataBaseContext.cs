﻿using System;
using Schenker2.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Schenker2;

namespace Schenker2.DbModels
{
    public partial class DataBaseContext : DbContext
    {
        public DataBaseContext()
        {
        }

        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Waprodb> Waprodb { get; set; }
        public virtual DbSet<AdresyFirmy> AdresyFirmy { get; set; }
        public virtual DbSet<Artykul> Artykul { get; set; }
        public virtual DbSet<DokumentHandlowy> DokumentHandlowy { get; set; }
        public virtual DbSet<DokumentMagazynowy> DokumentMagazynowy { get; set; }
        public virtual DbSet<Dostawa> Dostawa { get; set; }
        public virtual DbSet<Firma> Firma { get; set; }
        public virtual DbSet<Kontrahent> Kontrahent { get; set; }
        public virtual DbSet<MagOperacjeDodatkowe> MagOperacjeDodatkowe { get; set; }
        public virtual DbSet<MiejsceDostawy> MiejsceDostawy { get; set; }
        public virtual DbSet<PozycjaDokumentuMagazynowego> PozycjaDokumentuMagazynowego { get; set; }
        public virtual DbSet<Zamowienie> Zamowienie { get; set; }
        public virtual DbSet<ImagSchenkerTypyOpakowan> ImagSchenkerTypyOpakowan { get; set; }
        public virtual DbSet<ImagSchenker> ImagSchenker { get; set; }

        // Unable to generate entity type for table 'dbo.WAPRODB'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var uwierzytelnienie = !DataBaseController.Instance.Auth.Contains("SQL") ? "Windows Authentication" : $"User Id={ DataBaseController.Instance.Login};Password={ DataBaseController.Instance.Password};";

                var auth = uwierzytelnienie == "Windows Authentication" ? "Integrated Security=true;" : $"User Id={ DataBaseController.Instance.Login};Password={ DataBaseController.Instance.Password};";


                var cs = $"Data Source={DataBaseController.Instance.DbServerName};Initial Catalog={DataBaseController.Instance.DbName};{auth}";
                //var cs = 
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(cs);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");
            modelBuilder.Entity<ImagSchenker>(entity =>
            {
                entity.ToTable("_IMAG_SCHENKER");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Adres)
                    .IsRequired()
                    .HasColumnName("adres")
                    .IsUnicode(false);

                entity.Property(e => e.DataListu)
                    .HasColumnName("data_listu")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdDok)
                    .IsRequired()
                    .HasColumnName("id_dok")
                    .IsUnicode(false);

                entity.Property(e => e.Import).HasColumnName("import");

                entity.Property(e => e.Kontrahent)
                    .IsRequired()
                    .HasColumnName("kontrahent")
                    .IsUnicode(false);

                entity.Property(e => e.Lp).HasColumnName("lp");

                entity.Property(e => e.LiczbaPaczek)
                    .IsRequired()
                    .HasColumnName("liczba_paczek")
                    .IsUnicode(false);

                entity.Property(e => e.NrProtokolu)
                    .HasColumnName("nr_protokolu")
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasColumnName("orderId")
                    .IsUnicode(false);

                entity.Property(e => e.Statusf)
                    .HasColumnName("statusf")
                    .IsUnicode(false);

                entity.Property(e => e.Telefon)
                    .IsRequired()
                    .HasColumnName("telefon")
                    .IsUnicode(false);

                entity.Property(e => e.Typ)
                    .IsRequired()
                    .HasColumnName("typ")
                    .IsUnicode(false);

                entity.Property(e => e.V2).HasColumnName("v2");
                entity.Property(e => e.Miejscowosc).HasColumnName("miejscowosc");
                entity.Property(e => e.ImieNazwisko).HasColumnName("imie_nazwisko");
            });
            modelBuilder.Entity<ImagSchenkerTypyOpakowan>(entity =>
            {
                entity.ToTable("_IMAG_SCHENKER_TYPY_OPAKOWAN");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Dlugosc).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SymbolPaczki)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");
                entity.Property(e => e.NazwaOpakowania)
                    .IsRequired()
                    .HasMaxLength(200)
                   ;

                entity.Property(e => e.Szerokosc).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Wysokosc).HasColumnType("decimal(18, 0)");
            });
            modelBuilder.Entity<Waprodb>(entity =>
            {
                entity.Property(e => e.WaprodbId).HasColumnName("WAPRODB_ID");
            });

            modelBuilder.Entity<AdresyFirmy>(entity =>
            {
                entity.HasKey(e => e.IdAdresyFirmy);

                entity.ToTable("ADRESY_FIRMY");

                entity.HasIndex(e => new { e.IdFirmy, e.TypAdresu, e.DataOd })
                    .HasName("ADRESY_FIRMY_IDF_TY_DATA");

                entity.Property(e => e.IdAdresyFirmy)
                    .HasColumnName("ID_ADRESY_FIRMY")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Aktywny).HasColumnName("AKTYWNY");

                entity.Property(e => e.DataOd).HasColumnName("DATA_OD");

                entity.Property(e => e.EMail)
                    .HasColumnName("E_MAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Faks)
                    .HasColumnName("FAKS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gmina)
                    .HasColumnName("GMINA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.IdFirmy)
                    .HasColumnName("ID_FIRMY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdUrzeduSkarbowego)
                    .HasColumnName("ID_URZEDU_SKARBOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.KodPocztowy)
                    .HasColumnName("KOD_POCZTOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KpDataOd)
                    .HasColumnName("KP_DATA_OD")
                    .HasColumnType("datetime");

                entity.Property(e => e.Kraj)
                    .HasColumnName("KRAJ")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Miejscowosc)
                    .HasColumnName("MIEJSCOWOSC")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.NrDomu)
                    .HasColumnName("NR_DOMU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NrLokalu)
                    .HasColumnName("NR_LOKALU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Poczta)
                    .HasColumnName("POCZTA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Powiat)
                    .HasColumnName("POWIAT")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Skrytka)
                    .HasColumnName("SKRYTKA")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SymKraju)
                    .HasColumnName("SYM_KRAJU")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Telefon)
                    .HasColumnName("TELEFON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telex)
                    .HasColumnName("TELEX")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TypAdresu)
                    .HasColumnName("TYP_ADRESU")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('S')");

                entity.Property(e => e.Ulica)
                    .HasColumnName("ULICA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Wojewodztwo)
                    .HasColumnName("WOJEWODZTWO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Zakonczenie).HasColumnName("ZAKONCZENIE");
            });

            modelBuilder.Entity<Artykul>(entity =>
            {
                entity.HasKey(e => e.IdArtykulu);

                entity.ToTable("ARTYKUL");

                entity.HasIndex(e => e.GuidArtykul)
                    .HasName("GUID_ARTYKUL_INDEX");

                entity.HasIndex(e => e.IdArtykuluProd)
                    .HasName("ARTYKUL_ID_ART_PROD");

                entity.HasIndex(e => e.IdCenyDom)
                    .HasName("ARTYKUL_IDCENDOM");

                entity.HasIndex(e => e.IdKategorii)
                    .HasName("ARTYKUL_IDKAT");

                entity.HasIndex(e => e.IdKategoriiTree)
                    .HasName("IDX_ARTYKUL_IDKATTREE");

                entity.HasIndex(e => e.IdOpakowaniaRef)
                    .HasName("IDX_ARTYKUL_IDOBJREF");

                entity.HasIndex(e => new { e.IdMagazynu, e.IdArtykuluProd })
                    .HasName("ARTYKUL_IDMAG_PROD");

                entity.HasIndex(e => new { e.IdMagazynu, e.IndKatTowarZast })
                    .HasName("ARTYKUL_IDMAG_IKATTOOWZAST");

                entity.HasIndex(e => new { e.IdMagazynu, e.IndeksKatalogowy })
                    .HasName("ARTYKUL_IDMAG_IKATAL");

                entity.HasIndex(e => new { e.IdMagazynu, e.KodKreskowy })
                    .HasName("ARTYKUL_IDMAG_KODKRESK");

                entity.HasIndex(e => new { e.IdMagazynu, e.Lokalizacja })
                    .HasName("ARTYKUL_IDMAG_LOKAL");

                entity.HasIndex(e => new { e.IdMagazynu, e.Nazwa })
                    .HasName("ARTYKUL_IDMAG_NAZWA");

                entity.HasIndex(e => new { e.IdMagazynu, e.NazwaCala })
                    .HasName("ARTYKUL_IDMAG_NAZWA_CALA");

                entity.HasIndex(e => new { e.IdMagazynu, e.Plu })
                    .HasName("ARTYKUL_IDMAG_PLU");

                entity.HasIndex(e => new { e.IdMagazynu, e.Stan })
                    .HasName("ARTYKUL_IDMAG_STAN");

                entity.HasIndex(e => new { e.IdMagazynu, e.Wyroznik })
                    .HasName("ARTYKUL_IDMAG_WYROZNIK");

                entity.HasIndex(e => new { e.IndeksKatalogowy, e.IdMagazynu })
                    .HasName("IDX_ARTYKUL_IKATAL");

                entity.HasIndex(e => new { e.NazwaOryg, e.IdMagazynu })
                    .HasName("ARTYKUL_NAZWAORG_IDM");

                entity.HasIndex(e => new { e.Semafor, e.IdArtykulu })
                    .HasName("AK_ARTYKUL_SEMAFOR_ARTYKUL")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdMagazynu, e.DoOdbiorcow, e.OdDostawcow })
                    .HasName("ARTYKUL_IDMAG_ZAM");

                entity.HasIndex(e => new { e.IdMagazynu, e.IdKategorii, e.Nazwa })
                    .HasName("ARTYKUL_IDMAG_IDKAT_NAZWA");

                entity.HasIndex(e => new { e.IdMagazynu, e.IndeksHandlowy, e.IndeksKatalogowy })
                    .HasName("ARTYKUL_IDMAG_IHANDL");

                entity.HasIndex(e => new { e.IdMagazynu, e.Nazwa, e.Nazwa2 })
                    .HasName("ARTYKUL_IDMAG_NAZWA2");

                entity.HasIndex(e => new { e.IdMagazynu, e.Rodzaj, e.Nazwa })
                    .HasName("ARTYKUL_IDMAG_RODZ_NAZWA");

                entity.HasIndex(e => new { e.IdMagazynu, e.StanMaksymalny, e.StanMinimalny })
                    .HasName("ARTYKUL_IDMAG_MINMAX");

                entity.HasIndex(e => new { e.IdMagazynu, e.Zamowiono, e.Rodzaj })
                    .HasName("ARTYKUL_IDMAGZAMRODZ");

                entity.HasIndex(e => new { e.IdArtykulu, e.IndeksKatalogowy, e.IdMagazynu, e.DostepnyWSklepieInter })
                    .HasName("ARTYKUL_DOSTEPNY_W_SKLEPIE_INTER");

                entity.Property(e => e.IdArtykulu)
                    .HasColumnName("ID_ARTYKULU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Akcyza)
                    .HasColumnName("AKCYZA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AkcyzaJm)
                    .HasColumnName("AKCYZA_JM")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.AkcyzaPrzelicznik)
                    .HasColumnName("AKCYZA_PRZELICZNIK")
                    .HasColumnType("decimal(14, 6)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.AkcyzaPrzelicznikJmJa)
                    .HasColumnName("AKCYZA_PRZELICZNIK_JM_JA")
                    .HasColumnType("decimal(14, 6)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.AkcyzaStawkaZaJm)
                    .HasColumnName("AKCYZA_STAWKA_ZA_JM")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AktCenPrzyDostawie)
                    .HasColumnName("AKT_CEN_PRZY_DOSTAWIE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.AktywnyDlaSysZew).HasColumnName("AKTYWNY_DLA_SYS_ZEW");

                entity.Property(e => e.CZakupuBruttoWal)
                    .HasColumnName("C_ZAKUPU_BRUTTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CZakupuNettoWal)
                    .HasColumnName("C_ZAKUPU_NETTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaBKgo)
                    .HasColumnName("CENA_B_KGO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CenaNKgo)
                    .HasColumnName("CENA_N_KGO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CenaPromocjiB)
                    .HasColumnName("CENA_PROMOCJI_B")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaPromocjiN)
                    .HasColumnName("CENA_PROMOCJI_N")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaZakupuBrutto)
                    .HasColumnName("CENA_ZAKUPU_BRUTTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaZakupuNetto)
                    .HasColumnName("CENA_ZAKUPU_NETTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Certyfikat).HasColumnName("CERTYFIKAT");

                entity.Property(e => e.DataCertyfikatu).HasColumnName("DATA_CERTYFIKATU");

                entity.Property(e => e.DataZmCz)
                    .HasColumnName("DATA_ZM_CZ")
                    .HasDefaultValueSql("((36163))");

                entity.Property(e => e.DoOdbiorcow)
                    .HasColumnName("DO_ODBIORCOW")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.DoRezerwacji)
                    .HasColumnName("DO_REZERWACJI")
                    .HasColumnType("decimal(16, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DomyslnyVatZaGranica)
                    .HasColumnName("DOMYSLNY_VAT_ZA_GRANICA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DostepnyWAukcjach)
                    .HasColumnName("DOSTEPNY_W_AUKCJACH")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DostepnyWSklepieInter)
                    .HasColumnName("DOSTEPNY_W_SKLEPIE_INTER")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.GuidArtykul)
                    .HasColumnName("GUID_ARTYKUL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdArtykuluProd)
                    .HasColumnName("ID_ARTYKULU_PROD")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdCenyDom)
                    .HasColumnName("ID_CENY_DOM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDostawcyPreferowanego)
                    .HasColumnName("ID_DOSTAWCY_PREFEROWANEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdEtykiety)
                    .HasColumnName("ID_ETYKIETY")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFprom)
                    .HasColumnName("ID_FPROM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdJednostki)
                    .HasColumnName("ID_JEDNOSTKI")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdJednostkiRef)
                    .HasColumnName("ID_JEDNOSTKI_REF")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdJednostkiSprz)
                    .HasColumnName("ID_JEDNOSTKI_SPRZ")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdJednostkiZak)
                    .HasColumnName("ID_JEDNOSTKI_ZAK")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKategorii)
                    .HasColumnName("ID_KATEGORII")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKategoriiTree)
                    .HasColumnName("ID_KATEGORII_TREE")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMagazynu)
                    .HasColumnName("ID_MAGAZYNU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdOpakowaniaRef)
                    .HasColumnName("ID_OPAKOWANIA_REF")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdProducenta)
                    .HasColumnName("ID_PRODUCENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IloscEdytowana)
                    .HasColumnName("ILOSC_EDYTOWANA")
                    .HasColumnType("decimal(16, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IloscProd)
                    .HasColumnName("ILOSC_PROD")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.IndKatTowarZast)
                    .HasColumnName("IND_KAT_TOWAR_ZAST")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IndeksHandlowy)
                    .IsRequired()
                    .HasColumnName("INDEKS_HANDLOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IndeksKatalogowy)
                    .IsRequired()
                    .HasColumnName("INDEKS_KATALOGOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.JedWagi)
                    .HasColumnName("JED_WAGI")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.JedWymiaru)
                    .HasColumnName("JED_WYMIARU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.JednostkaProd)
                    .HasColumnName("JEDNOSTKA_PROD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.JestZdjecie)
                    .HasColumnName("JEST_ZDJECIE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KodCn)
                    .HasColumnName("KOD_CN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KodKreskowy)
                    .HasColumnName("KOD_KRESKOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.KrajPochodzenia)
                    .HasColumnName("KRAJ_POCHODZENIA")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Lokalizacja)
                    .HasColumnName("LOKALIZACJA")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Marzowy)
                    .HasColumnName("MARZOWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MinNarzut)
                    .HasColumnName("MIN_NARZUT")
                    .HasColumnType("decimal(10, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Nazwa)
                    .IsRequired()
                    .HasColumnName("NAZWA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Nazwa2)
                    .HasColumnName("NAZWA2")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaCala)
                    .HasColumnName("NAZWA_CALA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaCertyfikatu)
                    .HasColumnName("NAZWA_CERTYFIKATU")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaIntrastat)
                    .HasColumnName("NAZWA_INTRASTAT")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NazwaOryg)
                    .HasColumnName("NAZWA_ORYG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OdDostawcow)
                    .HasColumnName("OD_DOSTAWCOW")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Odwrotny)
                    .HasColumnName("ODWROTNY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Opis)
                    .HasColumnName("OPIS")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Ostrzezenie)
                    .HasColumnName("OSTRZEZENIE")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Pkwiu)
                    .HasColumnName("PKWIU")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.Plu)
                    .HasColumnName("PLU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PodlegaRabatowaniu)
                    .HasColumnName("PODLEGA_RABATOWANIU")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PokazujOstrzezenie).HasColumnName("POKAZUJ_OSTRZEZENIE");

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrgIlosc)
                    .HasColumnName("PRG_ILOSC")
                    .HasColumnType("decimal(16, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrgLojal)
                    .HasColumnName("PRG_LOJAL")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Producent)
                    .HasColumnName("PRODUCENT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PromocjaDo).HasColumnName("PROMOCJA_DO");

                entity.Property(e => e.PromocjaOd).HasColumnName("PROMOCJA_OD");

                entity.Property(e => e.PromocjaProcent)
                    .HasColumnName("PROMOCJA_PROCENT")
                    .HasColumnType("decimal(8, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PromocjaRabat)
                    .HasColumnName("PROMOCJA_RABAT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrzelicznikProd)
                    .HasColumnName("PRZELICZNIK_PROD")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Rodzaj)
                    .HasColumnName("RODZAJ")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Semafor)
                    .HasColumnName("SEMAFOR")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Stan)
                    .HasColumnName("STAN")
                    .HasColumnType("decimal(16, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StanMaksymalny)
                    .HasColumnName("STAN_MAKSYMALNY")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.StanMinimalny)
                    .HasColumnName("STAN_MINIMALNY")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Swwku)
                    .HasColumnName("SWWKU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SymWal)
                    .HasColumnName("SYM_WAL")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.VatSprzedazy)
                    .HasColumnName("VAT_SPRZEDAZY")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.VatZakupu)
                    .HasColumnName("VAT_ZAKUPU")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Waga)
                    .HasColumnName("WAGA")
                    .HasColumnType("decimal(20, 3)");

                entity.Property(e => e.WylaczCenyInd)
                    .HasColumnName("WYLACZ_CENY_IND")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.WymiarG)
                    .HasColumnName("WYMIAR_G")
                    .HasColumnType("decimal(20, 3)");

                entity.Property(e => e.WymiarS)
                    .HasColumnName("WYMIAR_S")
                    .HasColumnType("decimal(20, 3)");

                entity.Property(e => e.WymiarW)
                    .HasColumnName("WYMIAR_W")
                    .HasColumnType("decimal(20, 3)");

                entity.Property(e => e.WymuszajDostawy)
                    .HasColumnName("WYMUSZAJ_DOSTAWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Wyroznik)
                    .HasColumnName("WYROZNIK")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Zablokowany)
                    .HasColumnName("ZABLOKOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Zamowiono)
                    .HasColumnName("ZAMOWIONO")
                    .HasColumnType("decimal(16, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Zarezerwowano)
                    .HasColumnName("ZAREZERWOWANO")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.ZasadaZdejmowaniaZeStanu)
                    .HasColumnName("ZASADA_ZDEJMOWANIA_ZE_STANU")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<DokumentHandlowy>(entity =>
            {
                entity.HasKey(e => e.IdDokumentuHandlowego);

                entity.ToTable("DOKUMENT_HANDLOWY");

                entity.HasIndex(e => e.IdDokKorygowanego)
                    .HasName("DOKHANDL_IDDOKKOR");

                entity.HasIndex(e => e.IdFzalOst)
                    .HasName("DOKHANDL_IDFZALOST");

                entity.HasIndex(e => e.IdKorektyZbiorczej)
                    .HasName("DOKHANDL_ID_KOREKTY_ZBIORCZEJ");

                entity.HasIndex(e => e.IdMagazynu)
                    .HasName("DOKHANDL_IDMAG");

                entity.HasIndex(e => e.IdOstKorekty)
                    .HasName("DOKHANDL_IDOSTKOR");

                entity.HasIndex(e => e.IdPracownika)
                    .HasName("DOKHANDL_IDPRAC");

                entity.HasIndex(e => e.KodKreskowy)
                    .HasName("DOKHANDL_KOD_KRES");

                entity.HasIndex(e => e.Numer)
                    .HasName("DOKHANDL_NUMERDESC");

                entity.HasIndex(e => e.Semafor)
                    .HasName("DOKHANDL_SEMAFOR");

                entity.HasIndex(e => new { e.DataWystawienia, e.IdDokumentuHandlowego })
                    .HasName("DOKHANDL_DWYSTDESC_IDDOKHANDL");

                entity.HasIndex(e => new { e.IdDokOryginalnego, e.DataWystawienia })
                    .HasName("DOKHANDL_IDDOKORG");

                entity.HasIndex(e => new { e.IdFirmy, e.DataOtrzymania })
                    .HasName("DOKHANDL_IDFIR_DATA_OTRZYMANIA");

                entity.HasIndex(e => new { e.IdFirmy, e.DataWystawienia })
                    .HasName("DOKHANDL_IDFIR_DATA");

                entity.HasIndex(e => new { e.IdFirmy, e.IdKontrahenta })
                    .HasName("DOKHANDL_IDFIR_IDKONTR");

                entity.HasIndex(e => new { e.IdFirmy, e.Numer })
                    .HasName("DOKHANDL_IDFIR_NUMER");

                entity.HasIndex(e => new { e.IdFirmy, e.Pozostalo })
                    .HasName("DOKHANDL_IDFIR_POZOSTALO");

                entity.HasIndex(e => new { e.IdFirmy, e.TerminPlat })
                    .HasName("DOKHANDL_IDFIR_TERMIN");

                entity.HasIndex(e => new { e.IdFzalOrg, e.DataWystawienia })
                    .HasName("DOKHANDL_IDFZALORG");

                entity.HasIndex(e => new { e.IdPlatnika, e.Pozostalo })
                    .HasName("DOKHANDL_IDPLATN_POZO");

                entity.HasIndex(e => new { e.KontrahentNazwa, e.IdDokumentuHandlowego })
                    .HasName("DOKHANDL_KONTRAHENT_NAZWA_DESC")
                    .IsUnique();

                entity.HasIndex(e => new { e.PlatnikNazwa, e.IdDokumentuHandlowego })
                    .HasName("DOKHANDL_PLATNIK_NAZWA_DESC")
                    .IsUnique();

                entity.HasIndex(e => new { e.TypDokumentu, e.DokKorekty })
                    .HasName("DOKHANDL_TYPDOKDK");

                entity.HasIndex(e => new { e.IdTypu, e.DokKorekty, e.Trybrejestracji })
                    .HasName("DOKHANDL_IDTYPUDKTR");

                entity.HasIndex(e => new { e.NumerParagonu, e.IdMagazynu, e.IdDokumentuHandlowego })
                    .HasName("DOKHANDL_IDFIR_NR_PARAGONU");

                entity.HasIndex(e => new { e.TypDokumentu, e.IdTypu, e.DataWystawienia })
                    .HasName("DOKHANDL_TYPDOKIDTYP");

                entity.Property(e => e.IdDokumentuHandlowego)
                    .HasColumnName("ID_DOKUMENTU_HANDLOWEGO")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Autonumer).HasColumnName("AUTONUMER");

                entity.Property(e => e.CzyZaliczkaDlaFin).HasColumnName("CZY_ZALICZKA_DLA_FIN");

                entity.Property(e => e.DataKursWal).HasColumnName("DATA_KURS_WAL");

                entity.Property(e => e.DataKursWalPz).HasColumnName("DATA_KURS_WAL_PZ");

                entity.Property(e => e.DataOstRozliczenia).HasColumnName("DATA_OST_ROZLICZENIA");

                entity.Property(e => e.DataOtrzymania).HasColumnName("DATA_OTRZYMANIA");

                entity.Property(e => e.DataSprzedazy).HasColumnName("DATA_SPRZEDAZY");

                entity.Property(e => e.DataWplywu).HasColumnName("DATA_WPLYWU");

                entity.Property(e => e.DataWystawienia).HasColumnName("DATA_WYSTAWIENIA");

                entity.Property(e => e.DoKsiegowania)
                    .HasColumnName("DO_KSIEGOWANIA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokHandlowy)
                    .HasColumnName("DOK_HANDLOWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokKorekty).HasColumnName("DOK_KOREKTY");

                entity.Property(e => e.DokWal).HasColumnName("DOK_WAL");

                entity.Property(e => e.DokWalKrajowy)
                    .HasColumnName("DOK_WAL_KRAJOWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokZablokowany)
                    .HasColumnName("DOK_ZABLOKOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokZaksiegowany)
                    .HasColumnName("DOK_ZAKSIEGOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FiskalnyOk).HasColumnName("FISKALNY_OK");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FormaPlatnosci)
                    .HasColumnName("FORMA_PLATNOSCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrupujPozycje)
                    .HasColumnName("GRUPUJ_POZYCJE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdBanku).HasColumnName("ID_BANKU");

                entity.Property(e => e.IdDokKorygowanego)
                    .HasColumnName("ID_DOK_KORYGOWANEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokOryginalnego)
                    .HasColumnName("ID_DOK_ORYGINALNEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdEtykiety)
                    .HasColumnName("ID_ETYKIETY")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFirmy)
                    .HasColumnName("ID_FIRMY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdFzalOrg)
                    .HasColumnName("ID_FZAL_ORG")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFzalOst)
                    .HasColumnName("ID_FZAL_OST")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdKontrahenta)
                    .HasColumnName("ID_KONTRAHENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahentaJst)
                    .HasColumnName("ID_KONTRAHENTA_JST")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdKorektyZbiorczej)
                    .HasColumnName("ID_KOREKTY_ZBIORCZEJ")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMagazynu)
                    .HasColumnName("ID_MAGAZYNU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdOstKorekty)
                    .HasColumnName("ID_OST_KOREKTY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPlatnika)
                    .HasColumnName("ID_PLATNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPracownika)
                    .HasColumnName("ID_PRACOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdRachunku)
                    .HasColumnName("ID_RACHUNKU")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdSzabdokser)
                    .HasColumnName("ID_SZABDOKSER")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdTypu)
                    .HasColumnName("ID_TYPU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdUzytkownika)
                    .HasColumnName("ID_UZYTKOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.KodKreskowy)
                    .HasColumnName("KOD_KRESKOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KontrahentNazwa)
                    .HasColumnName("KONTRAHENT_NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Marzowy)
                    .HasColumnName("MARZOWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MarzowyOpis)
                    .HasColumnName("MARZOWY_OPIS")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MetodaKasowa)
                    .HasColumnName("METODA_KASOWA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Miejsce)
                    .HasColumnName("MIEJSCE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mp)
                    .HasColumnName("MP")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NarzutRabat)
                    .HasColumnName("NARZUT_RABAT")
                    .HasColumnType("decimal(9, 2)");

                entity.Property(e => e.Numer)
                    .HasColumnName("NUMER")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NumerParagonu)
                    .HasColumnName("NUMER_PARAGONU")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ObliczanieWgCen)
                    .HasColumnName("OBLICZANIE_WG_CEN")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Odebral)
                    .HasColumnName("ODEBRAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Odwrotny)
                    .HasColumnName("ODWROTNY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PlatnikNazwa)
                    .HasColumnName("PLATNIK_NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PotwierdzonyUe)
                    .HasColumnName("POTWIERDZONY_UE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PozKosztoweBazowe)
                    .HasColumnName("POZ_KOSZTOWE_BAZOWE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Pozostalo)
                    .HasColumnName("POZOSTALO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.PozostaloWal)
                    .HasColumnName("POZOSTALO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.ProporcjaZalAutoNettoBrutto)
                    .HasColumnName("PROPORCJA_ZAL_AUTO_NETTO_BRUTTO")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrzelicznikWal)
                    .HasColumnName("PRZELICZNIK_WAL")
                    .HasColumnType("decimal(14, 8)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PrzelicznikWalPz)
                    .HasColumnName("PRZELICZNIK_WAL_PZ")
                    .HasColumnType("decimal(14, 8)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RazemZaplacono)
                    .HasColumnName("RAZEM_ZAPLACONO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.RazemZaplaconoWal)
                    .HasColumnName("RAZEM_ZAPLACONO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.RodzajPlat)
                    .HasColumnName("RODZAJ_PLAT")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Rozliczony)
                    .HasColumnName("ROZLICZONY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Semafor)
                    .HasColumnName("SEMAFOR")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SumaOdsetek)
                    .HasColumnName("SUMA_ODSETEK")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SymWal)
                    .HasColumnName("SYM_WAL")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TerminPlat).HasColumnName("TERMIN_PLAT");

                entity.Property(e => e.TrojstronnyUe)
                    .HasColumnName("TROJSTRONNY_UE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Trybrejestracji).HasColumnName("TRYBREJESTRACJI");

                entity.Property(e => e.TypDokumentu)
                    .HasColumnName("TYP_DOKUMENTU")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TypPlatnika)
                    .HasColumnName("TYP_PLATNIKA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uproszczony)
                    .HasColumnName("UPROSZCZONY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.WartoscBrutto)
                    .HasColumnName("WARTOSC_BRUTTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.WartoscBruttoWal)
                    .HasColumnName("WARTOSC_BRUTTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.WartoscNetto)
                    .HasColumnName("WARTOSC_NETTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.WartoscNettoWal)
                    .HasColumnName("WARTOSC_NETTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Wewnetrzny)
                    .HasColumnName("WEWNETRZNY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Wyroznik)
                    .IsRequired()
                    .HasColumnName("WYROZNIK")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Zaliczka)
                    .HasColumnName("ZALICZKA")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.ZaliczkaOdroczona)
                    .HasColumnName("ZALICZKA_ODROCZONA")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DokumentMagazynowy>(entity =>
            {
                entity.HasKey(e => e.IdDokMagazynowego);

                entity.ToTable("DOKUMENT_MAGAZYNOWY");

                entity.HasIndex(e => e.IdDokKorygowanego)
                    .HasName("DOKMAG_IDDOKKOR");

                entity.HasIndex(e => e.IdDokOryginalnego)
                    .HasName("DOKMAG_IDDOKORG");

                entity.HasIndex(e => e.IdFakturyRo)
                    .HasName("DOKMAG_ID_FAKTURY_RO");

                entity.HasIndex(e => e.IdKontrahenta)
                    .HasName("DOKMAG_IDKONTR");

                entity.HasIndex(e => e.IdMagazynuDocelowego)
                    .HasName("DOKMAG_ID_MAGDOC");

                entity.HasIndex(e => e.IdOstKorekty)
                    .HasName("DOKMAG_IDOSTKOR");

                entity.HasIndex(e => e.IdPracownika)
                    .HasName("DOKMAG_IDPRAC");

                entity.HasIndex(e => e.IdRo)
                    .HasName("DOKMAG_ID_RO");

                entity.HasIndex(e => e.IdSadu)
                    .HasName("DOKMAG_IDSADU");

                entity.HasIndex(e => e.IdZlecenia)
                    .HasName("DOKMAG_IDZLECENIA");

                entity.HasIndex(e => e.KodKreskowy)
                    .HasName("DOKMAG_KOD_KRES");

                entity.HasIndex(e => e.Numer)
                    .HasName("DOKMAG_NUMERDESC");

                entity.HasIndex(e => e.Semafor)
                    .HasName("DOKMAG_SEMAFOR");

                entity.HasIndex(e => new { e.IdDokMagazynowego, e.IdFzalOrg })
                    .HasName("DOKMAG_IDFZALORG");

                entity.HasIndex(e => new { e.IdDokumentuHandlowego, e.Data })
                    .HasName("DOKMAG_IDDOKHANDL_DATA");

                entity.HasIndex(e => new { e.IdKontrahenta, e.Data })
                    .HasName("DOKMAG_IDKONTR_DATA");

                entity.HasIndex(e => new { e.KontrahentNazwa, e.IdDokMagazynowego })
                    .HasName("DOKMAG_KONTRAHENT_NAZWA_DESC")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdMagazynu, e.Data, e.IdDokMagazynowego })
                    .HasName("DOKMAG_IDMAG_DATA");

                entity.HasIndex(e => new { e.IdMagazynu, e.DataVat, e.IdDokMagazynowego })
                    .HasName("DOKMAG_IDMAG_DATA_VAT");

                entity.HasIndex(e => new { e.IdMagazynu, e.Numer, e.IdDokMagazynowego })
                    .HasName("DOKMAG_IDMAG_NUMER");

                entity.HasIndex(e => new { e.IdMagazynu, e.Przychod, e.Data })
                    .HasName("DOKMAG_IDMAG_PRZYCH_DATA");

                entity.HasIndex(e => new { e.IdMagazynu, e.Rozchod, e.Data })
                    .HasName("DOKMAG_IDMAG_ROZCH_DATA");

                entity.HasIndex(e => new { e.IdTypu, e.IdMagazynu, e.Numer })
                    .HasName("DOKMAG_IDTYPUNUM");

                entity.HasIndex(e => new { e.IdTypu, e.IdMagazynu, e.Trybrejestracji })
                    .HasName("DOKMAG_IDTYPUTR");

                entity.Property(e => e.IdDokMagazynowego)
                    .HasColumnName("ID_DOK_MAGAZYNOWEGO")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Autonumer).HasColumnName("AUTONUMER");

                entity.Property(e => e.BruttoNetto)
                    .HasColumnName("BRUTTO_NETTO")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Data).HasColumnName("DATA");

                entity.Property(e => e.DataDhWymagana)
                    .HasColumnName("DATA_DH_WYMAGANA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DataKursWalDm).HasColumnName("DATA_KURS_WAL_DM");

                entity.Property(e => e.DataKursWalVat)
                    .HasColumnName("DATA_KURS_WAL_VAT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DataVat)
                    .HasColumnName("DATA_VAT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DoKsiegowania)
                    .HasColumnName("DO_KSIEGOWANIA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokKorekty).HasColumnName("DOK_KOREKTY");

                entity.Property(e => e.DokZablokowany)
                    .HasColumnName("DOK_ZABLOKOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokZaksiegowany)
                    .HasColumnName("DOK_ZAKSIEGOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Dokumentdalaczony).HasColumnName("DOKUMENTDALACZONY");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdDokKorygowanego)
                    .HasColumnName("ID_DOK_KORYGOWANEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokOryginalnego)
                    .HasColumnName("ID_DOK_ORYGINALNEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokumentuHandlowego)
                    .HasColumnName("ID_DOKUMENTU_HANDLOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdEtykiety)
                    .HasColumnName("ID_ETYKIETY")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFakturyRo)
                    .HasColumnName("ID_FAKTURY_RO")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFzalOrg)
                    .HasColumnName("ID_FZAL_ORG")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahenta)
                    .HasColumnName("ID_KONTRAHENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMagazynu)
                    .HasColumnName("ID_MAGAZYNU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMagazynuDocelowego)
                    .HasColumnName("ID_MAGAZYNU_DOCELOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMm)
                    .HasColumnName("ID_MM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdOstKorekty)
                    .HasColumnName("ID_OST_KOREKTY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPracownika)
                    .HasColumnName("ID_PRACOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdRo)
                    .HasColumnName("ID_RO")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdSadu)
                    .HasColumnName("ID_SADU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdTypu)
                    .HasColumnName("ID_TYPU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdUzytkownika)
                    .HasColumnName("ID_UZYTKOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdZlecenia)
                    .HasColumnName("ID_ZLECENIA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.KodKreskowy)
                    .HasColumnName("KOD_KRESKOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KontrahentNazwa)
                    .HasColumnName("KONTRAHENT_NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Marzowy)
                    .HasColumnName("MARZOWY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Numer)
                    .HasColumnName("NUMER")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Odchylka)
                    .HasColumnName("ODCHYLKA")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Odebral)
                    .HasColumnName("ODEBRAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PodstPrzyj)
                    .HasColumnName("PODST_PRZYJ")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrzelicznikWalDm)
                    .HasColumnName("PRZELICZNIK_WAL_DM")
                    .HasColumnType("decimal(14, 8)");

                entity.Property(e => e.PrzelicznikWalVat)
                    .HasColumnName("PRZELICZNIK_WAL_VAT")
                    .HasColumnType("decimal(14, 8)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Przychod).HasColumnName("PRZYCHOD");

                entity.Property(e => e.RodzajDokumentu)
                    .HasColumnName("RODZAJ_DOKUMENTU")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Rozchod).HasColumnName("ROZCHOD");

                entity.Property(e => e.Semafor).HasColumnName("SEMAFOR");

                entity.Property(e => e.SymWalDm)
                    .HasColumnName("SYM_WAL_DM")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SymWalVat)
                    .HasColumnName("SYM_WAL_VAT")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TrybRozchodu)
                    .HasColumnName("TRYB_ROZCHODU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Trybrejestracji).HasColumnName("TRYBREJESTRACJI");

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.WartoscBrutto)
                    .HasColumnName("WARTOSC_BRUTTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.WartoscNetto)
                    .HasColumnName("WARTOSC_NETTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Wycena)
                    .HasColumnName("WYCENA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Wyroznik)
                    .IsRequired()
                    .HasColumnName("WYROZNIK")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Dostawa>(entity =>
            {
                entity.HasKey(e => e.IdDostawy);

                entity.ToTable("DOSTAWA");

                entity.Property(e => e.IdDostawy)
                    .HasColumnName("ID_DOSTAWY")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DataOdbioru).HasColumnName("DATA_ODBIORU");

                entity.Property(e => e.DataPlanowana).HasColumnName("DATA_PLANOWANA");

                entity.Property(e => e.GodzinaOdbioru).HasColumnName("GODZINA_ODBIORU");

                entity.Property(e => e.IdDokMagazynowego)
                    .HasColumnName("ID_DOK_MAGAZYNOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdFormyDostawy)
                    .HasColumnName("ID_FORMY_DOSTAWY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMiejscaDostawy)
                    .HasColumnName("ID_MIEJSCA_DOSTAWY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdZamowienia)
                    .HasColumnName("ID_ZAMOWIENIA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Odebrano)
                    .HasColumnName("ODEBRANO")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.IdMiejscaDostawyNavigation)
                    .WithMany(p => p.Dostawa)
                    .HasForeignKey(d => d.IdMiejscaDostawy)
                    .HasConstraintName("FK_DOSTAWA_REFERENCE_MIEJSCE_");
            });

            modelBuilder.Entity<Firma>(entity =>
            {
                entity.HasKey(e => e.IdFirmy);

                entity.ToTable("FIRMA");

                entity.HasIndex(e => e.Nazwa)
                    .HasName("FIRMA_NAZWA")
                    .IsUnique();

                entity.HasIndex(e => new { e.Semafor, e.IdFirmy })
                    .HasName("AK_FIRMA_SEMAFOR_FIRMA")
                    .IsUnique();

                entity.Property(e => e.IdFirmy)
                    .HasColumnName("ID_FIRMY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Aktywna).HasColumnName("AKTYWNA");

                entity.Property(e => e.DataRej).HasColumnName("DATA_REJ");

                entity.Property(e => e.DataRozpDzial).HasColumnName("DATA_ROZP_DZIAL");

                entity.Property(e => e.Ekd)
                    .HasColumnName("EKD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Est2000).HasColumnName("EST2000");

                entity.Property(e => e.Fakir)
                    .HasColumnName("FAKIR")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Gang)
                    .HasColumnName("GANG")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdAdresuDomyslnego)
                    .HasColumnName("ID_ADRESU_DOMYSLNEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahentaJst)
                    .HasColumnName("ID_KONTRAHENTA_JST")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdRachunku)
                    .HasColumnName("ID_RACHUNKU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdUs)
                    .HasColumnName("ID_US")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdZus)
                    .HasColumnName("ID_ZUS")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Kaper)
                    .HasColumnName("KAPER")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Katalog)
                    .HasColumnName("KATALOG")
                    .HasMaxLength(260)
                    .IsUnicode(false);

                entity.Property(e => e.KpAktywna)
                    .HasColumnName("kp_aktywna")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.KpDataRej)
                    .HasColumnName("KP_DATA_REJ")
                    .HasColumnType("datetime");

                entity.Property(e => e.KpDataRozpDzial)
                    .HasColumnName("KP_DATA_ROZP_DZIAL")
                    .HasColumnType("datetime");

                entity.Property(e => e.KpIdRodzaju).HasColumnName("KP_ID_RODZAJU");

                entity.Property(e => e.KpKduPath)
                    .HasColumnName("KP_KDU_PATH")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.KpMiesiacAktualny)
                    .HasColumnName("KP_MIESIAC_AKTUALNY")
                    .HasColumnType("datetime");

                entity.Property(e => e.KpNumerStartowy).HasColumnName("KP_NUMER_STARTOWY");

                entity.Property(e => e.KpOdliczenieZusDw).HasColumnName("KP_Odliczenie_ZUS_DW");

                entity.Property(e => e.KpOsobaFizyczna)
                    .HasColumnName("KP_OSOBA_FIZYCZNA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KpRca)
                    .HasColumnName("KP_RCA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KpTrybNumeracji)
                    .HasColumnName("KP_TRYB_NUMERACJI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KpWlascicielPlatnikiemZus).HasColumnName("KP_WLASCICIEL_PLATNIKIEM_ZUS");

                entity.Property(e => e.KwartalnyVat)
                    .HasColumnName("KWARTALNY_VAT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LogoFirmy)
                    .HasColumnName("LOGO_FIRMY")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LogoFirmyBlob)
                    .HasColumnName("LOGO_FIRMY_BLOB")
                    .HasColumnType("image");

                entity.Property(e => e.Mag)
                    .HasColumnName("MAG")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MetodaKasowa)
                    .HasColumnName("METODA_KASOWA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Mp)
                    .HasColumnName("MP")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Naglowek)
                    .HasColumnName("NAGLOWEK")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Naglowek2)
                    .HasColumnName("NAGLOWEK2")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Nazwa)
                    .IsRequired()
                    .HasColumnName("NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaPelna)
                    .HasColumnName("NAZWA_PELNA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaRej)
                    .HasColumnName("NAZWA_REJ")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nip)
                    .HasColumnName("NIP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nkp)
                    .HasColumnName("NKP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NrFkPulaAktualny)
                    .HasColumnName("NR_FK_PULA_AKTUALNY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NrFkPulaDo)
                    .HasColumnName("NR_FK_PULA_DO")
                    .HasDefaultValueSql("((99999999))");

                entity.Property(e => e.NrFkPulaOd)
                    .HasColumnName("NR_FK_PULA_OD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NrWRej)
                    .HasColumnName("NR_W_REJ")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.OrganZal)
                    .HasColumnName("ORGAN_ZAL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pkd)
                    .HasColumnName("PKD")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PlatnikVat).HasColumnName("PLATNIK_VAT");

                entity.Property(e => e.Regon)
                    .HasColumnName("REGON")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RodzajDzialH).HasColumnName("RODZAJ_DZIAL_H");

                entity.Property(e => e.RodzajDzialP).HasColumnName("RODZAJ_DZIAL_P");

                entity.Property(e => e.RodzajDzialU).HasColumnName("RODZAJ_DZIAL_U");

                entity.Property(e => e.RodzajPlatnika)
                    .HasColumnName("RODZAJ_PLATNIKA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Semafor)
                    .HasColumnName("SEMAFOR")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.StruktWlFirmy)
                    .HasColumnName("STRUKT_WL_FIRMY")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Kontrahent>(entity =>
            {
                entity.HasKey(e => e.IdKontrahenta);

                entity.ToTable("KONTRAHENT");

                entity.HasIndex(e => e.Klucz)
                    .HasName("KONTRAHENT_IDFIR_KLUCZ");

                entity.HasIndex(e => new { e.IdFirmy, e.IdAllegro })
                    .HasName("KONTRAHENT_IDFIR_IDALLEGRO");

                entity.HasIndex(e => new { e.IdFirmy, e.KodPocztowy })
                    .HasName("KONTRAHENT_IDFIR_KOD");

                entity.HasIndex(e => new { e.IdFirmy, e.LoginAllegro })
                    .HasName("KONTRAHENT_IDFIR_LOGINALLEGRO");

                entity.HasIndex(e => new { e.IdFirmy, e.Miejscowosc })
                    .HasName("KONTRAHENT_IDFIR_MIEJSCOWOSC");

                entity.HasIndex(e => new { e.IdFirmy, e.Nazwa })
                    .HasName("KONTRAHENT_IDFIR_NAZWA");

                entity.HasIndex(e => new { e.IdFirmy, e.Nip })
                    .HasName("KONTRAHENT_IDFIR_NIP");

                entity.HasIndex(e => new { e.IdFirmy, e.Nipl })
                    .HasName("KONTRAHENT_IDFIR_NIPL");

                entity.HasIndex(e => new { e.IdFirmy, e.Pesel })
                    .HasName("KONTRAHENT_IDFIR_PESEL");

                entity.HasIndex(e => new { e.IdFirmy, e.Wojewodztwo })
                    .HasName("KONTRAHENT_IDFIR_WOJEW");

                entity.HasIndex(e => new { e.KodKontrahenta, e.IdFirmy })
                    .HasName("KONTRAHENT_KOD")
                    .IsUnique();

                entity.HasIndex(e => new { e.Semafor, e.IdKontrahenta })
                    .HasName("AK_KONTRAHENT_SEMAFOR_KONTRAHE")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdFirmy, e.Dostawca, e.KodPocztowy })
                    .HasName("KONTRAHENT_IDFIR_DOST_KOD");

                entity.HasIndex(e => new { e.IdFirmy, e.Dostawca, e.Miejscowosc })
                    .HasName("KONTRAHENT_IDFIR_DOST_MIEJSCOWOSC");

                entity.HasIndex(e => new { e.IdFirmy, e.Dostawca, e.Nazwa })
                    .HasName("KONTRAHENT_IDFIR_DOST_NAZWA");

                entity.HasIndex(e => new { e.IdFirmy, e.Dostawca, e.Nip })
                    .HasName("KONTRAHENT_IDFIR_DOST_NIP");

                entity.HasIndex(e => new { e.IdFirmy, e.Odbiorca, e.KodPocztowy })
                    .HasName("KONTRAHENT_IDFIR_ODB_KOD");

                entity.HasIndex(e => new { e.IdFirmy, e.Odbiorca, e.Miejscowosc })
                    .HasName("KONTRAHENT_IDFIR_ODB_MIEJSCOWOSC");

                entity.HasIndex(e => new { e.IdFirmy, e.Odbiorca, e.Nazwa })
                    .HasName("KONTRAHENT_IDFIR_ODB_NAZWA");

                entity.HasIndex(e => new { e.IdFirmy, e.Odbiorca, e.Nip })
                    .HasName("KONTRAHENT_IDFIR_ODB_NIP");

                entity.Property(e => e.IdKontrahenta)
                    .HasColumnName("ID_KONTRAHENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.AdresEmail)
                    .HasColumnName("ADRES_EMAIL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AdresWww)
                    .HasColumnName("ADRES_WWW")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AkcyzaZwolniony).HasColumnName("AKCYZA_ZWOLNIONY");

                entity.Property(e => e.Autofiskalizacja)
                    .HasColumnName("AUTOFISKALIZACJA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Blokowanie)
                    .HasColumnName("BLOKOWANIE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DniPoTerminie)
                    .HasColumnName("DNI_PO_TERMINIE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokumentDataWydania).HasColumnName("DOKUMENT_DATA_WYDANIA");

                entity.Property(e => e.DokumentTozsamosciNazwa)
                    .HasColumnName("DOKUMENT_TOZSAMOSCI_NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DokumentTozsamosciNumer)
                    .HasColumnName("DOKUMENT_TOZSAMOSCI_NUMER")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DokumentWydanyPrzez)
                    .HasColumnName("DOKUMENT_WYDANY_PRZEZ")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DomSymWal)
                    .HasColumnName("DOM_SYM_WAL")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DomyslnyRabat)
                    .HasColumnName("DOMYSLNY_RABAT")
                    .HasColumnType("numeric(6, 2)");

                entity.Property(e => e.DosKod)
                    .HasColumnName("DOS_KOD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Dostawca).HasColumnName("DOSTAWCA");

                entity.Property(e => e.DostepnyWB2b)
                    .HasColumnName("DOSTEPNY_W_B2B")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DrukujOstrzezenie).HasColumnName("DRUKUJ_OSTRZEZENIE");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FormaPlatnosci)
                    .HasColumnName("FORMA_PLATNOSCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdAllegro)
                    .HasColumnName("ID_ALLEGRO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.IdEtykiety)
                    .HasColumnName("ID_ETYKIETY")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFirmy)
                    .HasColumnName("ID_FIRMY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdFormyDostawyDom)
                    .HasColumnName("ID_FORMY_DOSTAWY_DOM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdGrupy)
                    .HasColumnName("ID_GRUPY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdHandlowca)
                    .HasColumnName("ID_HANDLOWCA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKlasyfikacji)
                    .HasColumnName("ID_KLASYFIKACJI")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahentaJst)
                    .HasColumnName("ID_KONTRAHENTA_JST")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdMiejscaDostawyDom)
                    .HasColumnName("ID_MIEJSCA_DOSTAWY_DOM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPlatnika)
                    .HasColumnName("ID_PLATNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdRachunku)
                    .HasColumnName("ID_RACHUNKU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdTabeliOdsetek)
                    .HasColumnName("ID_TABELI_ODSETEK")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Incydentalny).HasDefaultValueSql("((0))");

                entity.Property(e => e.Klucz)
                    .HasColumnName("KLUCZ")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.KodKontrahenta).HasColumnName("KOD_KONTRAHENTA");

                entity.Property(e => e.KodKreskowy)
                    .HasColumnName("KOD_KRESKOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KodPocztowy)
                    .HasColumnName("KOD_POCZTOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.KodPocztowyKor)
                    .HasColumnName("KOD_POCZTOWY_KOR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KontrahentUe)
                    .HasColumnName("KontrahentUE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KpIdZdarzenia)
                    .HasColumnName("KP_ID_Zdarzenia")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.KtoPoprawil)
                    .HasColumnName("KTO_POPRAWIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KtoWpisal)
                    .HasColumnName("KTO_WPISAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KtoryMailDoRaportow)
                    .HasColumnName("KTORY_MAIL_DO_RAPORTOW")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LimitKupiecki)
                    .HasColumnName("LIMIT_KUPIECKI")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LoginAllegro)
                    .HasColumnName("LOGIN_ALLEGRO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LojalPkt)
                    .HasColumnName("LOJAL_PKT")
                    .HasColumnType("decimal(16, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LojalWartosc)
                    .HasColumnName("LOJAL_WARTOSC")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Miejscowosc)
                    .HasColumnName("MIEJSCOWOSC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiejscowoscKor)
                    .HasColumnName("MIEJSCOWOSC_KOR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mp)
                    .HasColumnName("MP")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Nazwa)
                    .HasColumnName("NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NazwaPelna)
                    .HasColumnName("NAZWA_PELNA")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Nip)
                    .HasColumnName("NIP")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Nipl)
                    .HasColumnName("NIPL")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NrAkcyzowy)
                    .IsRequired()
                    .HasColumnName("NR_AKCYZOWY")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Odbiorca).HasColumnName("ODBIORCA");

                entity.Property(e => e.OperatorPlatnosci)
                    .HasColumnName("OPERATOR_PLATNOSCI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Ostrzezenie)
                    .HasColumnName("OSTRZEZENIE")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Pesel)
                    .HasColumnName("PESEL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PlatnikVat)
                    .HasColumnName("PLATNIK_VAT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PodlegaPromocji)
                    .HasColumnName("PODLEGA_PROMOCJI")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PokazujOstrzezenie).HasColumnName("POKAZUJ_OSTRZEZENIE");

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Powiat)
                    .HasColumnName("POWIAT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PowiatKor)
                    .HasColumnName("POWIAT_KOR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrgLojal)
                    .HasColumnName("PRG_LOJAL")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrgLojalKwota)
                    .HasColumnName("PRG_LOJAL_KWOTA")
                    .HasColumnType("decimal(16, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Priorytet)
                    .HasColumnName("PRIORYTET")
                    .HasDefaultValueSql("((2))");

                entity.Property(e => e.RaksKodKontrahenta)
                    .HasColumnName("RAKS_KOD_KONTRAHENTA")
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Regon)
                    .HasColumnName("REGON")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Semafor)
                    .HasColumnName("SEMAFOR")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SymKraju)
                    .HasColumnName("SYM_KRAJU")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SymKrajuKor)
                    .HasColumnName("SYM_KRAJU_KOR")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonFirmowy)
                    .HasColumnName("TELEFON_FIRMOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TerminNaleznosci)
                    .HasColumnName("TERMIN_NALEZNOSCI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TerminZobowiazan)
                    .HasColumnName("TERMIN_ZOBOWIAZAN")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UlicaLokal)
                    .HasColumnName("ULICA_LOKAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UlicaLokalKor)
                    .HasColumnName("ULICA_LOKAL_KOR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UtworzonyWSystemieZewnetrznym).HasColumnName("UTWORZONY_W_SYSTEMIE_ZEWNETRZNYM");

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.VatCzynny)
                    .HasColumnName("VAT_CZYNNY")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Wewnetrzny)
                    .HasColumnName("WEWNETRZNY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Wojewodztwo)
                    .HasColumnName("WOJEWODZTWO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.WojewodztwoKor)
                    .HasColumnName("WOJEWODZTWO_KOR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Wyroznik)
                    .HasColumnName("WYROZNIK")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zablokowany)
                    .HasColumnName("ZABLOKOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ZbiorczePlatnosci)
                    .HasColumnName("ZBIORCZE_PLATNOSCI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ZgodaNaFakturyElektroniczne)
                    .HasColumnName("ZGODA_NA_FAKTURY_ELEKTRONICZNE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ZgodaNaPrzetwarzanie).HasColumnName("ZGODA_NA_PRZETWARZANIE");
            });

            modelBuilder.Entity<MagOperacjeDodatkowe>(entity =>
            {
                entity.HasKey(e => new { e.KodGrupy, e.Nazwa });

                entity.ToTable("MAG_OPERACJE_DODATKOWE");

                entity.HasIndex(e => new { e.KodGrupy, e.KodOperacji })
                    .HasName("AK_KOD_OPERACJI_KOD_GRUPY")
                    .IsUnique();

                entity.HasIndex(e => new { e.KodGrupy, e.Grupowanie, e.Nazwa })
                    .HasName("AK_PK_GRUPOWANIE_MAG_OPER")
                    .IsUnique();

                entity.Property(e => e.KodGrupy)
                    .HasColumnName("KOD_GRUPY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nazwa)
                    .HasColumnName("NAZWA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FunkcjaZewnetrzna)
                    .HasColumnName("FUNKCJA_ZEWNETRZNA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Grupowanie).HasColumnName("GRUPOWANIE");

                entity.Property(e => e.IdOperacji)
                    .HasColumnName("ID_OPERACJI")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Info)
                    .HasColumnName("INFO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.KasujZaznaczenie)
                    .HasColumnName("KASUJ_ZAZNACZENIE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.KodOperacji)
                    .IsRequired()
                    .HasColumnName("KOD_OPERACJI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OdswiezListe)
                    .HasColumnName("ODSWIEZ_LISTE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Parametry)
                    .HasColumnName("PARAMETRY")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Sciezka)
                    .HasColumnName("SCIEZKA")
                    .HasMaxLength(260)
                    .IsUnicode(false);

                entity.Property(e => e.SkrotKlawiszowy)
                    .HasColumnName("SKROT_KLAWISZOWY")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SkrotKlawiszowyKod).HasColumnName("SKROT_KLAWISZOWY_KOD");
            });

            modelBuilder.Entity<MiejsceDostawy>(entity =>
            {
                entity.HasKey(e => e.IdMiejscaDostawy);

                entity.ToTable("MIEJSCE_DOSTAWY");

                entity.Property(e => e.IdMiejscaDostawy)
                    .HasColumnName("ID_MIEJSCA_DOSTAWY")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Firma)
                    .HasColumnName("FIRMA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdFirmyMiejsca)
                    .HasColumnName("ID_FIRMY_MIEJSCA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahenta)
                    .HasColumnName("ID_KONTRAHENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.KodPocztowy)
                    .HasColumnName("KOD_POCZTOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Miejscowosc)
                    .HasColumnName("MIEJSCOWOSC")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Odbiorca)
                    .IsRequired()
                    .HasColumnName("ODBIORCA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .HasColumnName("OPIS")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SymKraju)
                    .HasColumnName("SYM_KRAJU")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Tel)
                    .HasColumnName("TEL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TypAdresu).HasColumnName("TYP_ADRESU");

                entity.Property(e => e.UlicaLokal)
                    .HasColumnName("ULICA_LOKAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PozycjaDokumentuMagazynowego>(entity =>
            {
                entity.HasKey(e => e.IdPozDokMag)
                    .HasName("PK_POZYCJA_DOKUMENTU_MAGAZYNOW");

                entity.ToTable("POZYCJA_DOKUMENTU_MAGAZYNOWEGO");

                entity.HasIndex(e => e.GuidPozDokMag)
                    .HasName("GUID_POZ_DOK_MAG_INDEX")
                    .IsUnique();

                entity.HasIndex(e => e.GuidPozDostawyMm)
                    .HasName("GUID_MM_INDEX");

                entity.HasIndex(e => e.IdDokKorygujacegoZbiorczo)
                    .HasName("IDX_PDM_ID_DOK_KORYGUJACEGO_ZBIORCZO");

                entity.HasIndex(e => e.IdPowKorekty)
                    .HasName("POZDOKMAG_IDPOWKOR");

                entity.HasIndex(e => e.IdPozOryginalnej)
                    .HasName("POZDOKMAG_IDPOZORG");

                entity.HasIndex(e => e.IdPozPrzych)
                    .HasName("POZDOKMAG_IDPOZPRZYCH");

                entity.HasIndex(e => e.IdPozZam)
                    .HasName("POZDOKMAG_IDPOZZAM_1");

                entity.HasIndex(e => e.IdPozZamZal)
                    .HasName("POZDOKMAG_IDPOZZAMZAL_1");

                entity.HasIndex(e => e.IdPozycjiOferty)
                    .HasName("PDM_IDPOZOF");

                entity.HasIndex(e => e.KodVat)
                    .HasName("POZDOKMAG_KOD_VAT");

                entity.HasIndex(e => e.NrSerii)
                    .HasName("POZDOKMAG_NRSERII");

                entity.HasIndex(e => new { e.Data, e.IdDokHandlowego })
                    .HasName("IDX_PDM_DATA_ID_DOK_HANDL1");

                entity.HasIndex(e => new { e.Data, e.IdDokMagazynowego })
                    .HasName("IDX_PDM_DATA_ID_DOK_MAG1");

                entity.HasIndex(e => new { e.IdDokHandlowego, e.Data })
                    .HasName("POZDOKMAG_IDDH_DATA");

                entity.HasIndex(e => new { e.IdDokHandlowego, e.IdPozDokMag })
                    .HasName("POZDOKMAG_IDDOKH_IDPOZ");

                entity.HasIndex(e => new { e.IdDokHandlowego, e.IdPozZam })
                    .HasName("POZDOKMAG_IDPOZZAM");

                entity.HasIndex(e => new { e.IdDokHandlowego, e.IdPozZamZal })
                    .HasName("POZDOKMAG_IDPOZZAMZAL");

                entity.HasIndex(e => new { e.IdDokMagazynowego, e.IdPozDokMag })
                    .HasName("POZDOKMAG_IDDOK_IDPOZ");

                entity.HasIndex(e => new { e.IdDokMagazynowego, e.IdPozZam })
                    .HasName("POZDOKMAG_IDPOZZAM_2");

                entity.HasIndex(e => new { e.IdDokMagazynowego, e.IdPozZamZal })
                    .HasName("POZDOKMAG_IDPOZZAMZAL_2");

                entity.HasIndex(e => new { e.IdOstKorekty, e.IdDokHandlowego })
                    .HasName("POZDOKMAG_IDOSTKOR");

                entity.HasIndex(e => new { e.IdPozKorygowanej, e.Trybrejestracji })
                    .HasName("POZDOKMAG_IDPOZKOR");

                entity.HasIndex(e => new { e.IdDokHandlowego, e.IdDokMagazynowego, e.RodzajArtykulu })
                    .HasName("POZDOKMAG_IDDM_IDDH_RA");

                entity.HasIndex(e => new { e.PozKorekty, e.PozWybDostawy, e.IdDokHandlowego })
                    .HasName("POZDOKMAG_POZDH");

                entity.HasIndex(e => new { e.RodzajPozycji, e.RodzajArtykulu, e.IdDokMagazynowego })
                    .HasName("POZDOKMAG_RODZAJ");

                entity.HasIndex(e => new { e.IdArtykulu, e.PozPrzychoduOk, e.Data, e.IdDokMagazynowego, e.IdPozDokMag, e.Ilosc, e.Wydano, e.Zarezerwowano })
                    .HasName("POZDOKMAG_IDART_POZOK_DATA");

                entity.HasIndex(e => new { e.IdPozDokMag, e.IdDokMagazynowego, e.IdDokHandlowego, e.IdArtykulu, e.IdPozKorygowanej, e.IdOstKorekty, e.Ilosc, e.RodzajPozycji, e.Data, e.CenaNetto, e.Rabat, e.Rabat2, e.Trybrejestracji, e.PozKorekty })
                    .HasName("IDX_PDM_TRYBREJESTRACJI_POZ_KOREKTY");

                entity.HasIndex(e => new { e.IdPozDokMag, e.IdDokMagazynowego, e.IdPozKorygowanej, e.IdOstKorekty, e.PozKorekty, e.Ilosc, e.CenaNetto, e.Rabat, e.Rabat2, e.IdArtykulu, e.RodzajPozycji, e.Trybrejestracji, e.IdDokHandlowego, e.Data })
                    .HasName("IDX_PDM_IDART_RPOZ_TRYB_IDDOK_DATA");

                entity.Property(e => e.IdPozDokMag)
                    .HasColumnName("ID_POZ_DOK_MAG")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Akcyza)
                    .HasColumnName("AKCYZA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AktCenPrzyDostawie)
                    .HasColumnName("AKT_CEN_PRZY_DOSTAWIE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CbSprzPz)
                    .HasColumnName("CB_SPRZ_PZ")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CenaBKgo)
                    .HasColumnName("CENA_B_KGO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CenaBrutto)
                    .HasColumnName("CENA_BRUTTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaBruttoBezKosztu)
                    .HasColumnName("CENA_BRUTTO_BEZ_KOSZTU")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaBruttoWal)
                    .HasColumnName("CENA_BRUTTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaNKgo)
                    .HasColumnName("CENA_N_KGO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CenaNetto)
                    .HasColumnName("CENA_NETTO")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaNettoBezKosztu)
                    .HasColumnName("CENA_NETTO_BEZ_KOSZTU")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CenaNettoWal)
                    .HasColumnName("CENA_NETTO_WAL")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CnSprzPz)
                    .HasColumnName("CN_SPRZ_PZ")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CzbMarza)
                    .HasColumnName("CZB_MARZA")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Data).HasColumnName("DATA");

                entity.Property(e => e.DataWaznosci).HasColumnName("DATA_WAZNOSCI");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.GuidPozDokMag)
                    .HasColumnName("GUID_POZ_DOK_MAG")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.GuidPozDostawyMm).HasColumnName("GUID_POZ_DOSTAWY_MM");

                entity.Property(e => e.IKodCn)
                    .HasColumnName("I_KOD_CN")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IKodDostawy)
                    .HasColumnName("I_KOD_DOSTAWY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IKodKontrahentaZagr)
                    .HasColumnName("I_KOD_KONTRAHENTA_ZAGR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IKodKrajuPoch)
                    .HasColumnName("I_KOD_KRAJU_POCH")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IKodKrajuPrzezWys)
                    .HasColumnName("I_KOD_KRAJU_PRZEZ_WYS")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IKodTransakcji)
                    .HasColumnName("I_KOD_TRANSAKCJI")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IKodTransportu)
                    .HasColumnName("I_KOD_TRANSPORTU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IMasaNetto)
                    .HasColumnName("I_MASA_NETTO")
                    .HasColumnType("decimal(16, 3)");

                entity.Property(e => e.IPodlegaDeklaracji)
                    .HasColumnName("I_PODLEGA_DEKLARACJI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdArtykulu)
                    .HasColumnName("ID_ARTYKULU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokHandlowego)
                    .HasColumnName("ID_DOK_HANDLOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokKorygujacegoZbiorczo)
                    .HasColumnName("ID_DOK_KORYGUJACEGO_ZBIORCZO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdDokMagazynowego)
                    .HasColumnName("ID_DOK_MAGAZYNOWEGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdOstKorekty)
                    .HasColumnName("ID_OST_KOREKTY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPowKorekty)
                    .HasColumnName("ID_POW_KOREKTY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozKorygowanej)
                    .HasColumnName("ID_POZ_KORYGOWANEJ")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozObrotowej)
                    .HasColumnName("ID_POZ_OBROTOWEJ")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozOryginalnej)
                    .HasColumnName("ID_POZ_ORYGINALNEJ")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozPrzych)
                    .HasColumnName("ID_POZ_PRZYCH")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozZam)
                    .HasColumnName("ID_POZ_ZAM")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozZamZal)
                    .HasColumnName("ID_POZ_ZAM_ZAL")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozZlecenia)
                    .HasColumnName("ID_POZ_ZLECENIA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdPozycjiOferty)
                    .HasColumnName("ID_POZYCJI_OFERTY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Ilosc)
                    .HasColumnName("ILOSC")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Jednostka)
                    .HasColumnName("JEDNOSTKA")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.KodVat)
                    .HasColumnName("KOD_VAT")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.KodVatUe)
                    .HasColumnName("KOD_VAT_UE")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NrPaczki)
                    .HasColumnName("NR_PACZKI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NrSerii)
                    .IsRequired()
                    .HasColumnName("NR_SERII")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.OpakowaniaPrzyjeto)
                    .HasColumnName("OPAKOWANIA_PRZYJETO")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.OpakowaniaWydano)
                    .HasColumnName("OPAKOWANIA_WYDANO")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Opis)
                    .HasColumnName("OPIS")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OpisCeny)
                    .HasColumnName("OPIS_CENY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PozKorekty).HasColumnName("POZ_KOREKTY");

                entity.Property(e => e.PozPrzychoduOk).HasColumnName("POZ_PRZYCHODU_OK");

                entity.Property(e => e.PozWybDostawy).HasColumnName("POZ_WYB_DOSTAWY");

                entity.Property(e => e.Przelicznik)
                    .HasColumnName("PRZELICZNIK")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Rabat)
                    .HasColumnName("RABAT")
                    .HasColumnType("decimal(8, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Rabat2)
                    .HasColumnName("RABAT2")
                    .HasColumnType("decimal(8, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Rabat2Zb)
                    .HasColumnName("RABAT2_ZB")
                    .HasColumnType("decimal(8, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.RodzajArtykulu)
                    .HasColumnName("RODZAJ_ARTYKULU")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RodzajPozycji)
                    .HasColumnName("RODZAJ_POZYCJI")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Trybrejestracji).HasColumnName("TRYBREJESTRACJI");

                entity.Property(e => e.Wydano)
                    .HasColumnName("WYDANO")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.Zarezerwowano)
                    .HasColumnName("ZAREZERWOWANO")
                    .HasColumnType("decimal(16, 6)");

                entity.Property(e => e.ZnacznikCeny)
                    .HasColumnName("ZNACZNIK_CENY")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('k')");
            });

            modelBuilder.Entity<Zamowienie>(entity =>
            {
                entity.HasKey(e => e.IdZamowienia);

                entity.ToTable("ZAMOWIENIE");

                entity.HasIndex(e => e.IdOperatoraPrzesylki)
                    .HasName("ZAMOWIENIE_ID_OPERATORA_PRZESYLKI");

                entity.HasIndex(e => e.IdPracownika)
                    .HasName("ZAMOWIENIE_IDPRAC");

                entity.HasIndex(e => e.KodKreskowy)
                    .HasName("ZAMOWIENIE_KOD_KRES");

                entity.HasIndex(e => e.NumerPrzesylki)
                    .HasName("ZAMOWIENIE_NUMER_PRZESYLKI");

                entity.HasIndex(e => e.OsobaZamawiajaca)
                    .HasName("ZAMOWIENIE_OSOBA_ZAMAWIAJACA");

                entity.HasIndex(e => e.Semafor)
                    .HasName("ZAMOWIENIE_SEMAFOR");

                entity.HasIndex(e => new { e.IdMagazynu, e.Data })
                    .HasName("ZAMOWIENIE_DATA_WYSTAWIENIA");

                entity.HasIndex(e => new { e.IdMagazynu, e.Numer })
                    .HasName("ZAMOWIENIE_NUMER");

                entity.HasIndex(e => new { e.KontrahentNazwa, e.IdZamowienia })
                    .HasName("ZAMOWIENIE_KONTRAHENT_NAZWA_DESC")
                    .IsUnique();

                entity.HasIndex(e => new { e.NrZamowieniaKlienta, e.IdMagazynu })
                    .HasName("ZAMOWIENIE_NRZAMKLI");

                entity.HasIndex(e => new { e.IdMagazynu, e.DokWal, e.SymWal })
                    .HasName("ZAMOWIENIE_IDMAGDW");

                entity.HasIndex(e => new { e.IdMagazynu, e.IdKontrahenta, e.DataRealizacji })
                    .HasName("ZAMOWIENIE_ID_KONTRAHENTA_DATA_REALIZACJI");

                entity.HasIndex(e => new { e.IdMagazynu, e.Priorytet, e.DataRealizacji })
                    .HasName("ZAMOWIENIE_PRIORYTET_DATA_REALIZACJI");

                entity.HasIndex(e => new { e.IdMagazynu, e.StanRealiz, e.StatusZam })
                    .HasName("ZAMOWIENIE_IDM_SZ");

                entity.Property(e => e.IdZamowienia)
                    .HasColumnName("ID_ZAMOWIENIA")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AutoRezerwacja)
                    .HasColumnName("AUTO_REZERWACJA")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Autonumer)
                    .HasColumnName("AUTONUMER")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Data).HasColumnName("DATA");

                entity.Property(e => e.DataKursWal).HasColumnName("DATA_KURS_WAL");

                entity.Property(e => e.DataRealizacji).HasColumnName("DATA_REALIZACJI");

                entity.Property(e => e.DniPlatnosci)
                    .HasColumnName("DNI_PLATNOSCI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokWal)
                    .HasColumnName("DOK_WAL")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DokZablokowany)
                    .HasColumnName("DOK_ZABLOKOWANY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FakturaDoZamowienia)
                    .HasColumnName("FAKTURA_DO_ZAMOWIENIA")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FlagaStanu)
                    .HasColumnName("FLAGA_STANU")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FormaPlatnosci)
                    .HasColumnName("FORMA_PLATNOSCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdEtykiety)
                    .HasColumnName("ID_ETYKIETY")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdFirmy)
                    .HasColumnName("ID_FIRMY")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontaktu)
                    .HasColumnName("ID_KONTAKTU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdKontrahenta)
                    .HasColumnName("ID_KONTRAHENTA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdMagazynu)
                    .HasColumnName("ID_MAGAZYNU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdOperatoraPrzesylki)
                    .HasColumnName("ID_OPERATORA_PRZESYLKI")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdPracownika)
                    .HasColumnName("ID_PRACOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdRachunku)
                    .HasColumnName("ID_RACHUNKU")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.IdUzytkownika)
                    .HasColumnName("ID_UZYTKOWNIKA")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.InformacjeDodatkowe)
                    .HasColumnName("INFORMACJE_DODATKOWE")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.KodKreskowy)
                    .HasColumnName("KOD_KRESKOWY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KontrahentNazwa)
                    .HasColumnName("KONTRAHENT_NAZWA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NrZamowieniaKlienta)
                    .HasColumnName("NR_ZAMOWIENIA_KLIENTA")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Numer)
                    .HasColumnName("NUMER")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NumerPrzesylki)
                    .HasColumnName("NUMER_PRZESYLKI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ObliczanieWgCen)
                    .HasColumnName("OBLICZANIE_WG_CEN")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Netto')");

                entity.Property(e => e.OsobaZamawiajaca)
                    .HasColumnName("OSOBA_ZAMAWIAJACA")
                    .HasMaxLength(101)
                    .IsUnicode(false);

                entity.Property(e => e.Pole1)
                    .HasColumnName("POLE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole10)
                    .HasColumnName("POLE10")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole2)
                    .HasColumnName("POLE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole3)
                    .HasColumnName("POLE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole4)
                    .HasColumnName("POLE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole5)
                    .HasColumnName("POLE5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole6)
                    .HasColumnName("POLE6")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole7)
                    .HasColumnName("POLE7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole8)
                    .HasColumnName("POLE8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pole9)
                    .HasColumnName("POLE9")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Priorytet)
                    .HasColumnName("PRIORYTET")
                    .HasDefaultValueSql("((2))");

                entity.Property(e => e.PrzelicznikWal)
                    .HasColumnName("PRZELICZNIK_WAL")
                    .HasColumnType("decimal(14, 8)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RabatNarzut)
                    .HasColumnName("RABAT_NARZUT")
                    .HasColumnType("decimal(6, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Semafor)
                    .HasColumnName("SEMAFOR")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.StanRealiz)
                    .HasColumnName("STAN_REALIZ")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.StatusZam)
                    .HasColumnName("STATUS_ZAM")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SymWal)
                    .HasColumnName("SYM_WAL")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Trybrejestracji)
                    .HasColumnName("TRYBREJESTRACJI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Typ)
                    .HasColumnName("TYP")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Uwagi)
                    .HasColumnName("UWAGI")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.WartoscBrutto)
                    .HasColumnName("WARTOSC_BRUTTO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.WartoscBruttoWal)
                    .HasColumnName("WARTOSC_BRUTTO_WAL")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.WartoscNetto)
                    .HasColumnName("WARTOSC_NETTO")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.WartoscNettoWal)
                    .HasColumnName("WARTOSC_NETTO_WAL")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Zaliczka)
                    .HasColumnName("ZALICZKA")
                    .HasColumnType("decimal(14, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ZamowienieInternetowe)
                    .HasColumnName("ZAMOWIENIE_INTERNETOWE")
                    .HasDefaultValueSql("((0))");
            });
        }
    }
}
