﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class Zamowienie
    {
        public decimal IdZamowienia { get; set; }
        public decimal? IdKontrahenta { get; set; }
        public decimal IdFirmy { get; set; }
        public decimal IdMagazynu { get; set; }
        public string Numer { get; set; }
        public int? Data { get; set; }
        public int? DataRealizacji { get; set; }
        public decimal? Zaliczka { get; set; }
        public byte? Priorytet { get; set; }
        public byte? AutoRezerwacja { get; set; }
        public string NrZamowieniaKlienta { get; set; }
        public byte? Typ { get; set; }
        public decimal Autonumer { get; set; }
        public decimal? WartoscBrutto { get; set; }
        public decimal? WartoscNetto { get; set; }
        public decimal? WartoscNettoWal { get; set; }
        public decimal? WartoscBruttoWal { get; set; }
        public decimal? PrzelicznikWal { get; set; }
        public string SymWal { get; set; }
        public byte? DokWal { get; set; }
        public int? DataKursWal { get; set; }
        public decimal? Semafor { get; set; }
        public string StanRealiz { get; set; }
        public string StatusZam { get; set; }
        public byte? FlagaStanu { get; set; }
        public string Uwagi { get; set; }
        public decimal? IdPracownika { get; set; }
        public decimal? IdUzytkownika { get; set; }
        public string Pole1 { get; set; }
        public string Pole2 { get; set; }
        public string Pole3 { get; set; }
        public string Pole4 { get; set; }
        public string Pole5 { get; set; }
        public string Pole6 { get; set; }
        public string Pole7 { get; set; }
        public string Pole8 { get; set; }
        public string Pole9 { get; set; }
        public string Pole10 { get; set; }
        public byte? DokZablokowany { get; set; }
        public byte? Trybrejestracji { get; set; }
        public decimal? RabatNarzut { get; set; }
        public string ObliczanieWgCen { get; set; }
        public decimal IdRachunku { get; set; }
        public decimal? IdEtykiety { get; set; }
        public string InformacjeDodatkowe { get; set; }
        public byte? ZamowienieInternetowe { get; set; }
        public string KodKreskowy { get; set; }
        public string KontrahentNazwa { get; set; }
        public decimal? IdKontaktu { get; set; }
        public string OsobaZamawiajaca { get; set; }
        public string FormaPlatnosci { get; set; }
        public int? DniPlatnosci { get; set; }
        public byte? FakturaDoZamowienia { get; set; }
        public string NumerPrzesylki { get; set; }
        public decimal? IdOperatoraPrzesylki { get; set; }
    }
}
