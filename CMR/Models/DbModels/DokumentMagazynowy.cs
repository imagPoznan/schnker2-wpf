﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class DokumentMagazynowy
    {
        public decimal IdDokMagazynowego { get; set; }
        public decimal? IdKontrahenta { get; set; }
        public decimal? IdMagazynu { get; set; }
        public decimal? IdDokumentuHandlowego { get; set; }
        public decimal? IdOstKorekty { get; set; }
        public decimal? IdDokOryginalnego { get; set; }
        public decimal? IdDokKorygowanego { get; set; }
        public byte? DokKorekty { get; set; }
        public byte? Przychod { get; set; }
        public decimal? IdTypu { get; set; }
        public byte? FlagaStanu { get; set; }
        public decimal? IdMagazynuDocelowego { get; set; }
        public decimal? IdUzytkownika { get; set; }
        public decimal? IdZlecenia { get; set; }
        public byte? Rozchod { get; set; }
        public string Numer { get; set; }
        public int? Data { get; set; }
        public decimal? WartoscBrutto { get; set; }
        public decimal? WartoscNetto { get; set; }
        public string BruttoNetto { get; set; }
        public string TrybRozchodu { get; set; }
        public int? Semafor { get; set; }
        public string PodstPrzyj { get; set; }
        public string RodzajDokumentu { get; set; }
        public int? Autonumer { get; set; }
        public string Wycena { get; set; }
        public string Uwagi { get; set; }
        public decimal? IdPracownika { get; set; }
        public byte Trybrejestracji { get; set; }
        public byte? Dokumentdalaczony { get; set; }
        public decimal? IdSadu { get; set; }
        public byte? DokZaksiegowany { get; set; }
        public byte? DokZablokowany { get; set; }
        public string Pole1 { get; set; }
        public string Pole2 { get; set; }
        public string Pole3 { get; set; }
        public string Pole4 { get; set; }
        public string Pole5 { get; set; }
        public string Pole6 { get; set; }
        public string Pole7 { get; set; }
        public string Pole8 { get; set; }
        public string Pole9 { get; set; }
        public string Pole10 { get; set; }
        public decimal? IdMm { get; set; }
        public decimal? IdRo { get; set; }
        public decimal? IdFakturyRo { get; set; }
        public decimal Odchylka { get; set; }
        public string Wyroznik { get; set; }
        public decimal? IdFzalOrg { get; set; }
        public string SymWalDm { get; set; }
        public int? DataKursWalDm { get; set; }
        public decimal? PrzelicznikWalDm { get; set; }
        public int? DataDhWymagana { get; set; }
        public byte? Marzowy { get; set; }
        public decimal? IdEtykiety { get; set; }
        public string Odebral { get; set; }
        public string KodKreskowy { get; set; }
        public byte? DoKsiegowania { get; set; }
        public int? DataVat { get; set; }
        public int? DataKursWalVat { get; set; }
        public decimal? PrzelicznikWalVat { get; set; }
        public string SymWalVat { get; set; }
        public string KontrahentNazwa { get; set; }
    }
}
