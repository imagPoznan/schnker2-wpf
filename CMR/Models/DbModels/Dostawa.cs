﻿using System;
using System.Collections.Generic;

namespace Schenker2.DbModels
{
    public partial class Dostawa
    {
        public decimal IdDostawy { get; set; }
        public decimal? IdMiejscaDostawy { get; set; }
        public decimal? IdDokMagazynowego { get; set; }
        public decimal? IdZamowienia { get; set; }
        public decimal? IdFormyDostawy { get; set; }
        public int? DataPlanowana { get; set; }
        public int? DataOdbioru { get; set; }
        public int? GodzinaOdbioru { get; set; }
        public byte? Odebrano { get; set; }

        public virtual MiejsceDostawy IdMiejscaDostawyNavigation { get; set; }
    }
}
